<?php echo $this->element('breadcrumb'); ?>
<div class = 'col-xs-12 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4 generic-margin-top-15px generic-padding-breadcrumb'>
	<div class="panel panel-default">
    	<div class="panel-heading generic-panel-heading">
        	<h3 class="panel-title">Vista Menu Administrativo</h3>
    	</div>
	    <div class="panel-body">
	    	<label>Nombre</label>
	        <div class="form-group">
	        	<div class = 'generic-disabled'>
	        		<?php echo $registro[$model_name]['name']; ?>
	        	</div>
	        </div>
	        <label>Posición</label>
	        <div class="form-group">
	            <div class = 'generic-disabled'>
	            	<?php echo $registro[$model_name]['position']; ?>
	        	</div>
	        </div>
			<?php echo $this->Html->link('Regresar', array('controller' => $controller_name, 'action' => 'index', 'admin' => true), array('class' => 'btn btn-sm btn-default generic-btn-default')); ?>
	        <!-- <button id = 'Regresar' class="btn btn-default" input type='button'>Regresar</button>-->
	        
	    </div>
	</div>
</div>