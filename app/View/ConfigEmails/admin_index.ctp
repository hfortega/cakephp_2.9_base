<?php
    // organizamos los errores de los campos en el formulario
    if(isset($fields_validation)){
        $this->Form->validationErrors[$model_name] = $fields_validation;
    }
?>
<?php
echo $this->Form->create($model_name, array('type' => 'file', 'id' => 'Emails'));
echo $this->Form->input('id', array('type' => 'hidden', 'readonly' => true));
?>
<!--
<script type="text/javascript">
    $(document).ready(function(){
            $('#Cancelar').click(function(){
                self.location.href = "<?php echo(Router::url(array('controller'=>'Mains','action'=>'index','admin' => true))); ?>";
                }
            )
        })
</script>
-->
<?php echo $this->element('breadcrumb'); ?>
<div class = 'col-xs-12 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4 generic-margin-top-15px generic-padding-breadcrumb'>
    <div class="panel panel-default">
        <div class="panel-heading generic-panel-heading">
            <h3 class="panel-title">Configuracion de Correo (Smtp)</h3>
        </div>
        <div class="panel-body">
            <div class="form-group">
                <?php echo $this->Form->input('host', array('label' => 'Host *', 'type' => 'text', 'placeholder' => '', 'class' => 'form-control input-sm')); ?>
            </div>
            <div class="form-group">
                <?php echo $this->Form->input('port', array('label' => 'Puerto *', 'type' => 'text', 'placeholder' => '', 'class' => 'form-control input-sm')); ?>
            </div>
            <div class="form-group">
                <?php echo $this->Form->input('username', array('label' => 'Email *', 'type' => 'text', 'placeholder' => '', 'class' => 'form-control input-sm')); ?>
            </div>
            <div class="form-group">
                <?php echo $this->Form->input('password1', array('label' => 'Contraseña', 'type' => 'password', 'placeholder' => '', 'class' => 'form-control input-sm')); ?></
            </div>
            <div class="form-group">
                <?php echo $this->Form->input('transport', array('label' => 'Transporte *', 'type' => 'text', 'placeholder' => '', 'value' => 'Smtp', 'class' => 'form-control input-sm')); ?>
            </div>
            <div class="form-group">
                <?php echo $this->Form->input('tls', array('options' => array('0' => 'No', '1' => 'Si'), 'label' => 'TLS *', 'class' => 'form-control input-sm')); ?>
            </div>
        <button id = 'Enviar' type="submit" class="btn btn-sm btn-primary generic-btn-default">Guardar</button>
            <!-- <button id = 'Cancelar' class="btn btn-default" input type='button'>Cancelar</button>-->
            <?php echo $this->Form->end(); ?>
        </div>
    </div>
</div>

    <!-- Este modal es para mostrar los errores en caso de que no se pueda actualizar la información o haya ocurrido un error en la base de datos -->
    <!-- Los errores de campos son mostrados debajo de cada campo para saber donde se equivoco y cual debe ser el formato correspondiente y  demás -->
<?php echo $this->element('modalFlash'); ?>