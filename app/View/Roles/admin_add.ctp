<?php
	// organizamos los errores de los campos en el formulario
	if(isset($fields_validation)){
		$this->Form->validationErrors[$model_name] = $fields_validation;
	}
	$message = $this->Flash->render();
	if(empty($message)){
		$show_modal = 0;
	}
	else{
		$show_modal = 1;
	}
?>
<script type="text/javascript">
    $(document).ready(function(){
		$('#Cancelar').click(function(){
            self.location.href = "<?php echo(Router::url(array('controller'=>$controller_name,'action'=>'index','admin' => true))); ?>";
        });
		// aqui vamos a validar si mostramos el modal para mostrar el error o aceptación o no
		if("<?php echo $show_modal ?>" == 1){
			$("#modal-flash-body").html('<?php echo $message; ?>');
			$("#Modal-Flash").modal('show');
		}
    })
</script>
<?php echo $this->element('breadcrumb'); ?>
<div class = 'col-md-12 generic-margin-top-15px generic-padding-breadcrumb'>
	<div class="panel panel-default">
    	<div class="panel-heading generic-panel-heading">
        	<h3 class="panel-title"><?php __('Nuevo Rol'); ?></h3>
    	</div>
	    <div class="panel-body">
	        <?php
	            echo $this->Form->create($model_name, array('type' => 'file','id' => $model_name));
	        ?>
	        <div class="form-group">
	            <?php echo $this->Form->input('name', array('label' => 'Nombre *', 'class' => 'form-control input-sm')); ?>
	        </div>
	        <div class = "table-responsive">
	            <table class = "table table-striped generic-table">
	                <thead>
                       	<th><?php echo __('Modulo'); ?></th>
                        <th><?php echo __('Menu'); ?></th>
                        <th><?php echo __('Activo'); ?></th>
                        <th><?php echo __('Agregar'); ?></th>
                        <th><?php echo __('Editar'); ?></th>
                        <th><?php echo __('Ver'); ?></th>
                        <th><?php echo __('Borrar'); ?></th>
                        <th><?php echo __('Ver Revisiones'); ?></th>
                        <th><?php echo __('Activar / Inactivar'); ?></th>
                        <th><?php echo __('Exportar'); ?></th>
                        <th><?php echo __('Importar'); ?></th>
	                    
	                </thead>
	                <?php
	                    $count = 1;
	                    foreach ($modulos as $modulo) {
	                ?>
	                            <tr>
	                                <td style="display:none;"><?php echo $this->Form->input('module_id_'.$count, array('type' => 'hidden', 'value' => $modulo['Module']['id'])) ?></td>
	                                <td><?php echo $modulo['Module']['name']; ?></td>
	                                <td><?php echo $modulo['Categorie']['name']; ?></td>
	                                <td><?php echo $this->Form->checkbox('active_'.$count, array('hiddenField' => true)); ?></td>
	                                <td><?php echo $this->Form->checkbox('add_'.$count, array('hiddenField' => true)); ?></td>
	                                <td><?php echo $this->Form->checkbox('edit_'.$count, array('hiddenField' => true)); ?></td>
	                                <td><?php echo $this->Form->checkbox('view_'.$count, array('hiddenField' => true)); ?></td>
	                                <td><?php echo $this->Form->checkbox('delete_'.$count, array('hiddenField' => true)); ?></td>
	                                <td><?php echo $this->Form->checkbox('view_revision_'.$count, array('hiddenField' => true)); ?></td>
	                                <td><?php echo $this->Form->checkbox('change_state_'.$count, array('hiddenField' => true)); ?></td>
	                                <td><?php echo $this->Form->checkbox('export_'.$count, array('hiddenField' => true)); ?></td>
	                                <td><?php echo $this->Form->checkbox('import_'.$count, array('hiddenField' => true)); ?></td>
	                            </tr>
	                <?php
	                        $count++;
	                    }
	                ?>
	                <tr>
	                    <td style="display:none"><?php echo $this->Form->input('count', array('type' => 'hidden', 'value' => $count)); ?></td>
	                </tr>
	            </table>
	        </div>
	        <button id = 'Enviar' type="submit" class="btn btn-sm btn-primary generic-btn-default">Guardar</button>
			<button id = 'Cancelar' class="btn btn-sm btn-default" input type='button'>Cancelar</button>
	        <?php echo $this->Form->end(); ?>
	    </div>
	</div>
</div>

	<!-- Este modal es para mostrar los errores en caso de que no se pueda actualizar la información o haya ocurrido un error en la base de datos -->
	<!-- Los errores de campos son mostrados debajo de cada campo para saber donde se equivoco y cual debe ser el formato correspondiente y  demás -->
<?php echo $this->element('modalFlash'); ?>