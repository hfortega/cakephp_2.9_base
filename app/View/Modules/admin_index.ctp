<?php
$message = $this->Flash->render();
if(empty($message)){
	$show_modal = 0;
}
else{
	$show_modal = 1;
}

?>
<script>
	$(document).ready(function(){
		var div_ids = ['#role_id', '#state'];
		initSelect(div_ids);
		$('[data-toggle="popover"]').popover();
		// aqui vamos a validar si mostramos el modal para mostrar el error o aceptación o no
		if("<?php echo $show_modal ?>" == 1){
			$("#modal-flash-body").html('<?php echo $message; ?>');
			$("#Modal-Flash").modal('show');
		}
	})
</script>
<?php echo $this->element('breadcrumb'); ?>
<div class = 'col-md-12 generic-margin-top-15px generic-background-white'>
	<div class = 'generic-row-buttons'>
		<div class="btn-group">
			<button type="button" class="btn btn-default dropdown-toggle generic-btn-default" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				<?php echo __('Acciones') ?> <span class="caret"></span>
			</button>
			<ul class="dropdown-menu generic-btn-dropdown-menu">
				<?php
				if($access_operation['RoleModule']['add']){
					echo "<li>". $this->Form->postLink(__('Nuevo'), array('controller' => $controller_name, 'action' => 'add', 'admin' => true), array('div' => false, 'escape' => false))."</li>";
				}
				if($access_operation['RoleModule']['import']){
					echo "<li>". $this->Form->postLink(__('Importar'), array('controller' => $controller_name, 'action' => 'import_index', 'admin' => true), array('div' => false, 'escape' => false))."</li>";
				}
				if($access_operation['RoleModule']['export']){
					echo "<li>".$this->Form->postLink(__('Exportar'), array('controller' => $controller_name, 'action' => 'export', 'admin' => true), array('div' => false, 'escape' => false))."</li>";
				}
				echo "<li>". $this->Html->link(__('Buscar'), '#', array('escape' => false, 'id' => 'buscar', 'data-toggle' => 'modal', 'data-target' => '#myModal'))."</li>";
				?>
			</ul>
		</div>
	</div>
	<div class = 'generic-title-table'> <?php echo $title_index ?></div>
	<div class = 'table-responsive'>
		<table class = 'table table-striped generic-table'>
			<thead>
				<th><?php echo __('Cód.') ?></th>
				<th><?php echo __('Nombre Completo') ?></th>
				<th><?php echo __('Menu') ?></th>
				<th><?php echo __('Estado') ?></th>
				<th></th>
			</thead>
			<tbody>
				<?php
					foreach ($registros as $registro) {
				?>
						<tr>
							<td><?php echo $registro[$model_name]['id']; ?></td>
							<td><?php echo $registro[$model_name]['name']; ?></td>
							<td><?php echo $registro['Categorie']['name']; ?></td>
							<td><?php echo $states[$registro[$model_name]['state']]; ?></td>
							<td>
								<?php
								// variable
								$action_html = '<div class="list-group generic-list-group">';
								for ($i=0; $i < count($action_rows); $i++) {
									if($access_operation['RoleModule'][array_values($action_rows)[$i]]){

										$action_html .= $this->Form->postLink(__(array_keys($action_rows)[$i]), array('controller' => $controller_name, 'action' => array_values($action_rows)[$i], $registro[$model_name]['id'], 'admin' => true), array('escape' => false, 'class' => 'list-group-item generic-group-item'));
									}
								}
								$action_html .= '</div>';
								?>
								<button type="button" id = 'popover_test' class="btn btn-sm btn-default generic-btn-default"  data-trigger = 'focus' data-html = 'true' data-container="body" data-toggle="popover" data-placement="left" data-content="<?php echo htmlspecialchars($action_html); ?>">
									Acciones
								</button>
							</td>
						</tr>
				<?php
					}
				?>
			</tbody>
		</table>
	</div>
	<div class = 'text-center generic-margin-top-25px'>
		<ul class="generic-pagination">
            <?php echo $this->Paginator->first('<< ', array('tag' => 'li')); ?>
            <?php
            	if($this->Paginator->hasPrev()){ 
            		echo $this->Paginator->prev('<', array('tag' => 'li'), null, array('class' => 'prev disabled'));
            	}
             ?>
            <?php echo $this->Paginator->numbers(array('separator' => '', 'class' => 'inactiveNumber', 'tag' => 'li')); ?>
            <?php
            	if($this->Paginator->hasNext()){
            		echo $this->Paginator->next('>', array('tag' => 'li'), null, array('class' => 'next disabled'));	
            	} 
            ?>
            <?php echo $this->Paginator->last(" >>", array('tag' => 'li')); ?>
    	</ul>
	</div>
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
	<div class="modal-dialog generic-modal-dialog">
	  	<!-- Modal content-->
	  	<div class="modal-content">
	    	<div class="modal-header"></div>
		    <div class="modal-body" id = "msj_popup">
		        <div class="panel panel-default">
		            <div class="panel-heading generic-panel-heading">
		                <h3 class="panel-title"><?php echo __('Búsqueda Avanzada') ?></h3>
		            </div>
		            <div class="panel-body">
		                <!-- <p>Some text in the modal.</p> -->
		                <?php
		                    echo $this->Form->create($model_name, array('type' => 'file'));
		                ?>
		                <div class="form-group">
		                    <?php echo $this->Form->input('name', array('label' => false, 'placeholder' => 'Nombre(s)' ,'class' => 'form-control input-sm')); ?>
		                </div>
		                <div class="form-group">
		                    <?php echo $this->Form->input('state', array('label' => false, 'options' => $states, 'empty' => 'Seleccione Opción' ,'class' => 'form-control input-sm', 'id' => 'state', 'style' => 'width:100%')); ?>
		                </div>
		            </div>
		        </div>
		    </div>
		    <div class="modal-footer">
				<button id = 'Enviar' type="submit" class="btn btn-sm btn-primary generic-btn-default"><?php echo __('Buscar'); ?></button>
				<?php echo $this->Form->end(); ?>
				<button type="button" class="btn btn-sm btn-default" data-dismiss="modal" id = 'cancel_button'><?php echo __('Cerrar'); ?></button>
		    </div>
	  	</div>
	</div>
</div>


<?php echo $this->element('modalFlash'); ?>