<?php
	$state = array('Inactivo', 'Activo');
?>
<div class = 'col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4'>
	<div class="panel panel-primary">
    	<div class="panel-heading">
        	<h3 class="panel-title">Vista Módulo de Sistema</h3>
    	</div>
	    <div class="panel-body">
	    	<label>Nombre</label>
	        <div class="form-group">
	        	<div class = 'generic-disabled'>
	        		<?php echo $registro[$model_name]['name']; ?>
	        	</div>
	        </div>
	        <label>Nombre Máquina</label>
	        <div class="form-group">
	            <div class = 'generic-disabled'>
	            	<?php echo $registro[$model_name]['name_machine']; ?>
	        	</div>
	        </div>
	        <label>Menu</label>
	        <div class="form-group">
            	<div class = 'generic-disabled'>
            		<?php echo $registro['Categorie']['name']; ?>
        		</div>
        	</div>
        	<label>Estado</label>
        	<div class="form-group">
            	<div class = 'generic-disabled'>
            		<?php echo $state[$registro[$model_name]['state']]; ?>
        		</div>
        	</div>
        	<?php echo $this->Html->link('Regresar', array('controller' => $controller_name, 'action' => 'index', 'admin' => true), array('class' => 'btn btn-default')); ?>
	    </div>
	</div>
</div>