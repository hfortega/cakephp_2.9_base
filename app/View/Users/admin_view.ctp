<?php echo $this->element('breadcrumb'); ?>
<div class = 'col-xs-12 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4 generic-margin-top-15px generic-padding-breadcrumb'>
	<div class="panel panel-default">
    	<div class="panel-heading generic-panel-heading">
        	<h3 class="panel-title">Vista del Usuario [<?php echo $registro[$model_name]['name']; ?>]</h3>
    	</div>
	    <div class="panel-body">
	    	<label>Nro Documento</label>
	        <div class="form-group">
	        	<div class = 'generic-disabled'>
	        		<?php echo $registro[$model_name]['nit']; ?>
	        	</div>
	        </div>
	        <label>Nombre Completo</label>
	        <div class="form-group">
	            <div class = 'generic-disabled'>
	            	<?php echo $registro[$model_name]['name']; ?>
	        	</div>
	        </div>
	        <label>Nro Telefono</label>
	        <div class="form-group">
            	<div class = 'generic-disabled'>
            		<?php echo $registro[$model_name]['phone']; ?>
        		</div>
        	</div>
        	<label>Nro Celular</label>
        	<div class="form-group">
            	<div class = 'generic-disabled'>
            		<?php echo $registro[$model_name]['cellphone']; ?>
        		</div>
        	</div>
        	<label>Correo Electrónico</label>
	        <div class="form-group">
            	<div class = 'generic-disabled'>
            		<?php echo $registro[$model_name]['email']; ?>
        		</div>
        	</div>
        	<label>Role</label>
        	<div class="form-group">
	            <div class = 'generic-disabled'>
	            	<?php echo $roles[$registro[$model_name]['role_id']]; ?>
	        	</div>
	        </div>
	        <label>Usuario</label>
	        <div class="form-group">
	            <div class = 'generic-disabled'>
	            	<?php echo $registro[$model_name]['username']; ?>
	        	</div>
	        </div>
	        <?php echo $this->Html->link('Regresar', array('controller' => $controller_name, 'action' => 'index', 'admin' => true), array('class' => 'btn btn-sm btn-default generic-btn-default')); ?>
		</div>
	</div>
</div>