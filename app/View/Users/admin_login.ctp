<?php

$message = $this->Flash->render();
if(empty($message)){
	$show_modal = 0;
}
else{
	$show_modal = 1;
}

?>
<script type="text/javascript">
    $(document).ready(function(){
        /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
        $("#recovery").click(function(){
            if($("#username").val() == ''){
                $("#modal-flash-body").html('Para recuperar el acceso por favor ingrese el usuario, este será enviado por correo electrónico, en caso de no recordar el Usuario debe comunicarse con el administrador de la plataforma');
            }
            else{
                $("#modal-flash-body").html('<p style="text-align:center"><?php echo $this->Html->image("flat/loader.gif"); ?></p>');
                $("#close").css('display', 'none');
                var url = "<?php echo Router::Url(array('controller' => 'Users', 'action' => 'recovery', 'admin' => false)); ?>"+"/"+$("#username").val();
                $.ajax({
                    type: 'POST',
                    url: url,
                    async: true,
                    success: function(response){
                        $("#modal-flash-body").html(response);
                        $("#close").css('display', '');
                    }
                })
            }
        })

		if("<?php echo $show_modal ?>" == 1){
			$("#modal-flash-body").html('<?php echo $message; ?>');
			$("#Modal-Flash").modal('show');
		}
	})
</script>
<div class = 'generic-form-login-box generic-form-login-center'>
	<div class="panel panel-default">
		<div class="panel-heading generic-panel-heading">
			<h3 class="panel-title"><?php echo __('LOGIN'); ?></h3>
		</div>
		<div class="panel-body">
			<?php
	            echo $this->Form->create(NULL, array('class' => 'form col-md-12 center-block'));
	        ?>
	            <div class="form-group">
	                <?php
	                    echo $this->Form->input('username', array('label' => false, 'placeholder' => 'Usuario', 'class' => 'form-control', 'required' => true, 'id' => 'username'));
	                ?>
	            </div>
	            <div class="form-group">
	                <?php
	                    echo $this->Form->input('password', array('label' => false, 'placeholder' => 'Contraseña', 'class' => 'form-control', 'type' => 'password', 'required' => true));
	                ?>    
	            </div>
	            <div class="form-group">
	            <?php 
	                echo $this->Form->input('Entrar', array('type' => 'submit', 'label' => false, 'div' => false, 'class' => 'btn btn-primary btn-block generic-btn-default'));
	            ?>
	            </div>
	        <?php echo $this->Form->end(); ?>
	    	<div class = 'text-center'>
				<?php
					echo $this->Html->link('Recuperar Accesos', '#', array('div' => false, 'label' => false, 'id' => 'recovery', 'data-toggle' => 'modal', 'data-target' => '#Modal-Flash'));
				?>
			</div>
		</div>
      </div>
	</div>
</div>

<!-- Modal para mostrar los mensajes de error o aceptación -->
<div class="modal fade" id="Modal-Flash" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" id = "close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<div id = 'modal-flash-body'></div>
			</div>
		</div>
	</div>
</div>