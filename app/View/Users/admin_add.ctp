<?php
	// organizamos los errores de los campos en el formulario
	if(isset($fields_validation)){
		$this->Form->validationErrors[$model_name] = $fields_validation;
	}
	$message = $this->Flash->render();
	if(empty($message)){
		$show_modal = 0;
	}
	else{
		$show_modal = 1;
	}
?>
<script type="text/javascript">
    $(document).ready(function(){
		var div_ids = ['#role_id'];
		initSelect(div_ids);
        $('#Cancelar').click(function(){
            self.location.href = "<?php echo(Router::url(array('controller'=>$controller_name,'action'=>'index','admin' => true))); ?>";
        });
		// aqui vamos a validar si mostramos el modal para mostrar el error o aceptación o no
		if("<?php echo $show_modal ?>" == 1){
			$("#modal-flash-body").html('<?php echo $message; ?>');
			$("#Modal-Flash").modal('show');
		}
    })
</script>
<?php echo $this->element('breadcrumb'); ?>
<div class = 'col-xs-12 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4 generic-margin-top-15px generic-padding-breadcrumb'>
	<div class="panel panel-default">
    	<div class="panel-heading generic-panel-heading">
        	<h3 class="panel-title">Nuevo Usuario</h3>
    	</div>
	    <div class="panel-body">
	        <?php
	            echo $this->Form->create($model_name, array('type' => 'file','id' => $model_name));
	        ?>
	        <div class="form-group">
	            <?php echo $this->Form->input('nit', array('label' => 'Nro Documento *', 'class' => 'form-control input-sm')); ?>
	        </div>
	        <div class="form-group">
	            <?php echo $this->Form->input('name', array('label' => 'Nombre Completo *', 'class' => 'form-control input-sm')); ?>
	        </div>
	        <div class="form-group">
            	<?php echo $this->Form->input('phone', array('label' => 'Nro Telefono', 'class' => 'form-control input-sm')); ?>
        	</div>
        	<div class="form-group">
            	<?php echo $this->Form->input('cellphone', array('label' => 'Nro Celular *', 'class' => 'form-control input-sm')); ?>
        	</div>
	        <div class="form-group">
            	<?php echo $this->Form->input('email', array('label' => 'Correo Electronico *', 'class' => 'form-control input-sm')); ?>
        	</div>
	        <div class="form-group">
				<label>Rol *</label>
	            <?php echo $this->Form->input('role_id', array('label' => false, 'options' => $roles, 'class' => 'form-control input-sm', 'id' => 'role_id', 'style' => 'width:100%')); ?>
	        </div>
	        <div class="form-group">
	            <?php echo $this->Form->input('username', array('label' => 'Nombre Usuario *', 'class' => 'form-control input-sm')); ?>
	        </div>
	        <div class="form-group">
	            <?php echo $this->Form->input('password', array('label' => 'Contraseña *', 'type' => 'password', 'class' => 'form-control input-sm')); ?>
	        </div>
			<button id = 'Enviar' type="submit" class="btn btn-sm btn-primary generic-btn-default">Guardar</button>
			<button id = 'Cancelar' class="btn btn-sm btn-default" input type='button'>Cancelar</button>
	        <?php echo $this->Form->end(); ?>
	    </div>
	</div>
</div>


	<!-- Este modal es para mostrar los errores en caso de que no se pueda actualizar la información o haya ocurrido un error en la base de datos -->
	<!-- Los errores de campos son mostrados debajo de cada campo para saber donde se equivoco y cual debe ser el formato correspondiente y  demás -->
<?php echo $this->element('modalFlash'); ?>