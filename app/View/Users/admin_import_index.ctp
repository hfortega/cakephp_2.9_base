<?php
    $message = $this->Flash->render();
    if(empty($message)){
        $show_modal = 0;
    }
    else{
        $show_modal = 1;
    }
    $type_import = array('1' => 'Crear Registros', '2' => 'Actualizar Registros');
?>
<script>
    $(document).ready(function(){
        var div_ids = ['#user_id', '#type'];
        initSelect(div_ids);
        if("<?php echo $show_modal ?>" == 1){
            $("#modal-flash-body").html('<?php echo $message; ?>');
            $("#Modal-Flash").modal('show');
        }
    })
</script>
<?php echo $this->element('breadcrumb'); ?>
<div class = 'col-md-12 generic-margin-top-15px generic-background-white'>
    <div class = 'generic-row-buttons'>
        <div class="btn-group">
            <button type="button" class="btn btn-default dropdown-toggle generic-btn-default" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <?php echo __('Acciones') ?> <span class="caret"></span>
            </button>
            <ul class="dropdown-menu generic-btn-dropdown-menu">
                <?php
                    echo "<li>". $this->Html->Link(__('Importar'), array('controller' => $controller_name, 'action' => 'add_import', 'admin' => true), array('div' => false, 'escape' => false))."</li>";
                    echo "<li>". $this->Html->Link(__('Actualizar'), array('controller' => $controller_name, 'action' => 'upd_import', 'admin' => true), array('div' => false, 'escape' => false))."</li>";
                    echo "<li>".$this->Html->link(__('Regresar'), array('controller' => $controller_name, 'action' => 'index'), array('div' => false, 'escape' => false))."</li>";
                    echo "<li>". $this->Html->link(__('Buscar'), '#', array('escape' => false, 'id' => 'buscar', 'data-toggle' => 'modal', 'data-target' => '#myModal'))."</li>";
                ?>
            </ul>
        </div>
    </div>
    <div class = 'generic-title-table'><?php echo __($title_index); ?></div>
    <div class = 'col-md-12'>
    <?php
        foreach ($registros as $registro) {
    ?>    
        <div class = "col-md-6">
            <div class="panel panel-default">
              <div class="panel-body">
                <p>Cod [<?php echo $registro['FileImport']['id']; ?>] </p>
                <p>Fecha [<?php echo $registro['FileImport']['date']; ?>] </p>
                <p>Nombre Archivo</p> 
                <p>[<?php
                    echo $this->Html->link($registro['FileImport']['name'], array('controller' => $controller_name, 'action' => 'download_file', $registro['FileImport']['id']));
                ?>] </p>
                <p>Tipo Importación [<?php echo $registro['FileImport']['type']; ?>] </p>
                <p>Registros Correctos [<?php echo $registro['FileImport']['correct']; ?>] </p>
                <p>Registros Errores [<?php echo $registro['FileImport']['error']; ?>] </p>
                <p>Registros Fallidos [<?php echo $registro['FileImport']['failed']; ?>] </p>
                <p>Archivo Errores </p>
                <p>[<?php 
                    echo $this->Html->link($registro['FileImport']['file_error'], array('controller' => $controller_name, 'action' => 'download_file', $registro['FileImport']['id'], 'error')); 
                ?>] </p>
                <p>Usuario [<?php echo $registro['User']['name']; ?>] </p>
              </div>
            </div>
        </div>
    <?php
        }
    ?>
    </div>

    <div class = 'text-center generic-margin-top-25px'>
        <ul class="generic-pagination">
            <?php echo $this->Paginator->first('<< ', array('tag' => 'li')); ?>
            <?php
                if($this->Paginator->hasPrev()){ 
                    echo $this->Paginator->prev('<', array('tag' => 'li'), null, array('class' => 'prev disabled'));
                }
             ?>
            <?php echo $this->Paginator->numbers(array('separator' => '', 'class' => 'inactiveNumber', 'tag' => 'li')); ?>
            <?php
                if($this->Paginator->hasNext()){
                    echo $this->Paginator->next('>', array('tag' => 'li'), null, array('class' => 'next disabled'));    
                } 
            ?>
            <?php echo $this->Paginator->last(" >>", array('tag' => 'li')); ?>
        </ul>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog  generic-modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header"></div>
        <div class="modal-body" id = "msj_popup">
            <div class="panel panel-default">
                <div class="panel-heading  generic-panel-heading">
                    <h3 class="panel-title">Busqueda Avanzada</h3>
                </div>
                <div class="panel-body">
                    <!-- <p>Some text in the modal.</p> -->
                    <?php
                        echo $this->Form->create($model_name, array('type' => 'file'));
                    ?>
                    <div class="form-group">
                        <?php echo $this->Form->input('name', array('label' => false, 'placeholder' => 'Nombre' ,'class' => 'form-control imput-sm')); ?>
                    </div>
                    <div class="form-group">
                        <?php echo $this->Form->input('type', array('label' => false, 'options' => $type_import, 'empty' => __('Seleccione Opción'), 'class' => 'form-control imput-sm', 'id' => 'type', 'style' => 'width:100%')); ?>
                    </div>
                    <div class="form-group">
                        <?php echo $this->Form->input('user_id', array('label' => false, 'options' => $users, 'empty' => __('Seleccione Opción'),'class' => 'form-control imput-sm', 'id' => 'user_id', 'style' => 'width:100%')); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button id = 'Enviar' type="submit" class="btn btn-sm btn-primary generic-btn-default"><?php echo __('Buscar'); ?></button>
            <?php echo $this->Form->end(); ?>
            <button type="button" class="btn btn-sm btn-default" data-dismiss="modal" id = 'cancel_button'><?php echo __('Cerrar'); ?></button>
        </div>
      </div>
    </div>
</div>

<?php echo $this->element('modalFlash'); ?>