<?php
    $options_import = array_keys($header_import);
    $options_update = array_keys($header_update);
?>  
<script type="text/javascript">
    $(document).ready(function(){
            $("#Enviar").click(function(){
                var retorno = validar_formulario("FileImport", "error");
                if(!retorno){
                    return false;
                }   
            })
            $('#Cancelar').click(function(){
                self.location.href = "<?php echo(Router::url(array('controller'=>$controller,'action'=>'import_index','admin' => true))); ?>";
                }
            )
        })
</script>
<div class = 'col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4'>
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title"><?php echo "Nueva Importación [".$nombre_module."]"; ?></h3>
        </div>
        <div class="panel-body">
            <?php
                echo $this->Form->create('FileImport', array('type' => 'file','id' => 'FileImport'));
            ?>
                <div class="form-group">
                    <p><strong>IMPORTACIÓN</strong></p>
                    <p>Para importar la información de <strong><?php echo $nombre_module; ?></strong> se debe crear un archivo <strong>csv (Separado por comas)</strong> con el siguiente encabezado: <strong><?php echo implode(', ', $options_import); ?></strong>.</p>
                    <p>
                        <strong>Nota:</strong> se debe tener en cuenta lo siguiente:
                        <ul>
                            <li>Todos los encabezados deben estar presenten en el archivo.</li>
                            <li>Si vas a crear registros nuevos se debe dejar vacia la columna codigo, pero el encabezado debe estar y debes seleccionar Tipo Operación [Crear Registros].</li>
                        </ul>
                    </p>
                    <p><strong>ACTUALIZACIÓN</strong></p>
                    <p>Para actualizar la información de <strong><?php echo $nombre_module ?></strong> se debe crear el archivo <strong>csv (Seprado por comas)</strong> con cualquiera de estas columnas : <strong><?php echo implode(', ', $options_update) ?></strong>.</p>
                    <p>
                        <strong>Nota:</strong> se debe tener en cuenta lo siguiente
                        <ul>
                            <li>La primera columna del archivo csv debe ser el codigo</li>
                            <li>Las siguientes columnas pueden ser cualquiera de las mencionadas anteriormente sin importar el orden</li>
                            <li>Se debe seleccionar [Actualizar Registros]</li>
                        </ul>
                    </p>
                </div>
                <div class="form-group">
                    <?php echo $this->Form->input('name', array('label' => 'Archivo *', 'type' => 'file' , 'rel' => 'Requerido', 'data-error' => 'Campo Requerido', 'class' => 'form-control', 'data-type' => 'Archivo_CSV', 'data-type-error' => 'Solo se permiten archivo con extensión csv (Separados por coma).')); ?>
                </div>
                <div class="form-group">
                    <?php echo $this->Form->input('type', array('label' => 'Tipo Operación *', 'options' => array('1' => 'Crear Registros', '2' => 'Actualizar Registros') ,'class' => 'form-control')); ?>
                </div>
                <button id = 'Enviar' type="submit" class="btn btn-primary">Guardar</button>
                <button id = 'Cancelar' class="btn btn-default" input type='button'>Cancelar</button>
            <?php echo $this->Form->end(); ?>
        </div>
    </div>
</div>
