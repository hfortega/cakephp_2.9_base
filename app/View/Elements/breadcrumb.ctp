<div class = 'col-md-12 generic-padding-breadcrumb'>
    <ol class = 'breadcrumb text-right generic-breadcrumb generic-margin-top-15px'>
        <?php
        foreach ($breadCrumb as $key => $value) {
            ?>
            <li><?php echo __($value); ?></li>
            <?php
        }
        ?>
    </ol>
</div>
