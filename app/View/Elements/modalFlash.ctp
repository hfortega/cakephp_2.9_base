<!-- Modal para mostrar los mensajes de error o aceptación -->
<div class="modal fade" id="Modal-Flash" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <div id = 'modal-flash-body'></div>
            </div>
        </div>
    </div>
</div>
