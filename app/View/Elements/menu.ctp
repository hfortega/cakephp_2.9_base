<?php
$user = $this->Session->read('Auth.User');
$menus = $this->Session->read('menu');
if (!empty($user)) {
?>
  <nav class="navbar navbar-default generic-navbar-primary">
    <div class="container-fluid">
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header generic-navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand generic-navbar-brand" href="#">Panel Control</a>
      </div>

      <!-- Collect the nav links, forms, and other content for toggling -->
      <div class="collapse navbar-collapse generic-navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav navbar-right">
          <?php
                foreach ($menus as $menu) {
                  $menu_name = array_keys($menu)[0];
                  if(!empty($menu[$menu_name])){
              ?>
                    <li class="dropdown generic-dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                        <?php echo $menu_name; ?> 
                        <span class="caret"></span>
                      </a>
                      <ul class="dropdown-menu generic-dropdown-menu">
                      <?php
                        foreach ($menu as $submenu) {
                          foreach ($submenu as $key_submenu) {
                      ?>
                          <li>
                            <?php
                              echo $this->Html->Link($key_submenu['Module']['name'], array('controller' => $key_submenu['Module']['name_machine'], 'action' => 'index', 'admin' => true));
                            ?>
                          </li>
                    <?php
                          }
                        }
                    ?>
                      </ul>
                    </li>
              <?php
                  }
                }
              ?>
          <li class = "generic-dropdown">
            <?php 
              echo $this->Form->postLink('Mi cuenta', array('controller' => 'Users', 'action' => 'account', 'admin' => true), array('escape' => false, 'div' => false));  
            ?>  
          </li>
          <li class = "generic-dropdown">
          	<?php 
          		echo $this->Form->postLink('Salir', array('controller' => 'Users', 'action' => 'logout', 'admin' => true), array('escape' => false, 'div' => false));  
          	?>	
          </li>
         </ul>
      </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
  </nav>
  <div class = 'clearfix'></div>
<?php
}
?>