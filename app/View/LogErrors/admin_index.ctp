<div class = 'col-md-12 generic-margin-top-50px'>
	<?php 
		if($access_operation['RoleModule']['import']){
			echo $this->Form->postLink('Importar', array('controller' => $controller_name, 'action' => 'import_index', 'admin' => true), array('div' => false, 'escape' => false, 'class' => 'btn btn-warning')); 
		}
	?>
	<?php 
		if($access_operation['RoleModule']['export']){
			echo $this->Form->postLink('Exportar', array('controller' => $controller_name, 'action' => 'export', 'admin' => true), array('div' => false, 'escape' => false, 'class' => 'btn btn-info')); 
		}
	?>
	<?php
        echo $this->Html->link('Buscar', '#', array('escape' => false, 'id' => 'buscar', 'data-toggle' => 'modal', 'data-target' => '#myModal', 'class' => 'btn btn-default'));
    ?>
</div>
<div class = 'col-md-10 col-md-offset-1 generic-margin-top-50px'>
	<?php 
		if($access_operation['RoleModule']['add']){
			echo $this->Form->postLink('Nuevo', array('controller' => $controller_name, 'action' => 'add', 'admin' => true), array('div' => false, 'escape' => false, 'class' => 'btn btn-primary pull-right')); 
		}
	?>
</div>
<div class = 'col-md-12 generic-margin-top-50px'>
	<?php
		echo $this->Flash->render();
	?>
	<div class = 'generic-title-table'>
		Listado de Usuarios
	</div>
	<div class = 'table-responsive'>
		<table class = 'table table-striped generic-table'>
			<thead>
				<th>Fecha</th>
				<th>Controlador</th>
				<th>Usuario</th>
				<th>Operación del error</th>

				<th>Operaciones</th>
			</thead>
			<tbody>
				<?php
					foreach ($registros as $registro) {
				?>
						<tr>
							<td><?php echo date('Y-m-d H:i:s',$registro[$model_name]['event_timestamp']); ?></td>
							<td><?php echo $registro[$model_name]['controller']; ?></td>
							<td><?php echo $registro['User']['name']; ?></td>
							<td><?php echo $registro[$model_name]['operation']; ?></td>
							<td>
								<?php
								for ($i=0; $i < count($action_rows); $i++) { 
									if($access_operation['RoleModule'][array_values($action_rows)[$i]]){
										echo $this->Form->postLink($this->Html->image('flat/'.$action_icons[array_keys($action_rows)[$i]], array('title' => array_keys($action_rows)[$i], 'class' => 'generic-btn')), array('controller' => $controller_name, 'action' => array_values($action_rows)[$i], $registro[$model_name]['id'], 'admin' => true), array('escape' => false)); 
									}
								}
								?>
							</td>
						</tr>
				<?php
					}
				?>
			</tbody>
		</table>
	</div>
	<div class = 'text-center generic-margin-top-25px'>
		<ul class="generic-pagination">
            <?php echo $this->Paginator->first('<< ', array('tag' => 'li')); ?>
            <?php
            	if($this->Paginator->hasPrev()){ 
            		echo $this->Paginator->prev('<', array('tag' => 'li'), null, array('class' => 'prev disabled'));
            	}
             ?>
            <?php echo $this->Paginator->numbers(array('separator' => '', 'class' => 'inactiveNumber', 'tag' => 'li')); ?>
            <?php
            	if($this->Paginator->hasNext()){
            		echo $this->Paginator->next('>', array('tag' => 'li'), null, array('class' => 'next disabled'));	
            	} 
            ?>
            <?php echo $this->Paginator->last(" >>", array('tag' => 'li')); ?>
    	</ul>
	</div>
</div>



<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
	<div class="modal-dialog">
	  	<!-- Modal content-->
	  	<div class="modal-content">
	    	<div class="modal-header"></div>
		    <div class="modal-body" id = "msj_popup">
		        <div class="panel panel-info">
		            <div class="panel-heading">
		                <h3 class="panel-title">Búsqueda Avanzada</h3>
		            </div>
		            <div class="panel-body">
		                <!-- <p>Some text in the modal.</p> -->
		                <?php
		                    echo $this->Form->create($model_name, array('type' => 'file'));
		                ?>
		                <!-- Aqui organizamos una tabla de busqueda con un formulario para realizar las busqueda -->
		                <div class="form-group">
		                    <?php echo $this->Form->input('user', array('label' => false, 'placeholder' => 'Usuario' ,'class' => 'form-control')); ?>
		                </div>
		            </div>
		        </div>
		    </div>
		    <div class="modal-footer">
		        <button id = 'Enviar' type="submit" class="btn btn-info">Buscar</button>
		        <?php echo $this->Form->end(); ?>
		        <button type="button" class="btn btn-default" data-dismiss="modal" id = 'cancel_button'>Close</button>
		    </div>
	  	</div>
	</div>
</div>