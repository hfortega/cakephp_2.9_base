<div class = 'col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4'>
	<div class="panel panel-primary">
    	<div class="panel-heading">
        	<h3 class="panel-title">Vista Log Error</h3>
    	</div>
	    <div class="panel-body">
	    	<label>Fecha</label>
	        <div class="form-group">
	        	<div class = 'generic-disabled'>
	        		<?php echo date('Y-m-d H:i:s', $registro[$model_name]['event_timestamp']); ?>
	        	</div>
	        </div>
	        <label>Usuario</label>
	        <div class="form-group">
	        	<div class = 'generic-disabled'>
	        		<?php echo $registro[$model_name]['user']; ?>
	        	</div>
	        </div>
	        <label>Controlador</label>
	        <div class="form-group">
	        	<div class = 'generic-disabled'>
	        		<?php echo $registro[$model_name]['controller']; ?>
	        	</div>
	        </div>
	        <label>operación del error</label>
	        <div class="form-group">
	        	<div class = 'generic-disabled'>
	        		<?php echo $registro[$model_name]['operation']; ?>
	        	</div>
	        </div>
	        <label>Descripción Error</label>
	        <div class="form-group">
	        	<div class = 'generic-disabled'>
	        		<?php echo $registro[$model_name]['description']; ?>
	        	</div>
	        </div>
	        <?php echo $this->Html->link('Regresar', array('controller' => $controller_name, 'action' => 'index', 'admin' => true), array('class' => 'btn btn-default')); ?>
	        <!-- <button id = 'Regresar' class="btn btn-default" input type='button'>Regresar</button>-->
	        
	    </div>
	</div>
</div>