<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<?php echo $this->Html->charset('utf-8'); ?>
	<title>
		<?php echo $this->fetch('title'); ?>
	</title>
	<?php
		echo $this->Html->meta('icon');
	echo $this->Html->css(array('Bootstrap/bootstrap','generic', 'datetimepicker/jquery.datetimepicker.min','Bootstrap/tabs','Bootstrap/tabs.sideways','jquery-ui.min', 'Select2/select2'));
	echo $this->Html->script(array('jQuery/jquery-1.11.2.min','Bootstrap/bootstrap','FunctionsCustom/functionsGeneric','jQuery/jquery-ui.min', 'Select2/select2', 'Moment/moment.min', 'Moment/moment-with-locales', 'Datetimepicker/jquery.datetimepicker.full.min'));
		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
	?>
</head>
<body>
	<div class = 'container-fluid'>
			<?php echo $this->element('menu'); ?>
			<?php echo $this->fetch('content'); ?>
	</div>
</body>
</html>