<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<?php echo $this->Html->charset('utf-8'); ?>
	<title>
		<?php echo $this->fetch('title'); ?>
	</title>
	<?php
		echo $this->Html->meta('icon');
		echo $this->Html->css(array('Bootstrap/bootstrap','generic', 'generic_menu'));
		echo $this->Html->script(array('jQuery/jquery-1.11.2.min','Bootstrap/bootstrap','FunctionsCustom/ValidacionesForm'));
		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
	?>
	<script src='https://www.google.com/recaptcha/api.js'></script>
	<link href="https://fonts.googleapis.com/css?family=Oswald:400,700|Roboto:400,500,700" rel="stylesheet">
</head>
<body>
	<div id="container-fluid">
			<?php echo $this->fetch('content'); ?>
	</div>
</body>
</html>