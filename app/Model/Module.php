<?php

class Module extends AppModel {

    public $name = 'Module';
    private $fields_import_map = array('nombre' => 'name' , 'nombre_maquina' => 'name_machine', 'estado' => 'state');
    private $fields_update_map = array('codigo' => 'id', 'nombre' => 'name' , 'nombre_maquina' => 'name_machine', 'estado' => 'state');
    private $fields_unique = array('name_machine' => 'nombre_maquina');
    private $rules_validation = array(
            'name' => array(
                'rules' => array(
                    'allowEmpty' => false,
                    'regExpresion' => '/^[\w\sáéíóúÁÉÍÓÚ]+$/',
                    'betweenLength' => '1,250',
                ),
                'messages' => array(
                    'regExpresion' => 'Campo Alfanumérico, solo se permiten Letras, Números y el caracter _',
                    'betweenLength' => 'El campo debe tener mínimo 1 caracteres y un máximo de 250 caracteres.',
                    'allowEmpty' => 'Campo Obligatorio',
                )
            ),
            'name_machine' => array(
                'rules' => array(
                    'allowEmpty' => false,
                    'regExpresion' => '/^[a-zA-Z]+$/',
                    'betweenLength' => '1,250',
                ),
                'messages' => array(
                    'regExpresion' => 'Campo Alfabético, solo se permiten Letras',
                    'betweenLength' => 'El campo debe tener mínimo 1 caracteres y un máximo de 250 caracteres.',
                    'allowEmpty' => 'Campo Obligatorio',
                )
            ),
            'categorie_id' => array(
                'rules' => array(
                    'allowEmpty' => false,
                    'regExpresion' => '/^[1-9]+[0-9]*$/',
                ),
                'messages' => array(
                    'allowEmpty' => 'Campo Obligatorio',
                    'regExpresion' => 'Campo númerico donde el primer número no puede ser cero (0)',
                )
            ),
        );

    // Función para extraer los modulos y mostrarlos en el index

    public function get_allModule($search = array(), $page = 1, $limit = 0){
        if(!empty($search[$this->name])){
            $fields = array_keys($search[$this->name]);
        }
        else{
            $fields = $search;
        }
        $conditions = array();
        for ($i=0; $i < count($fields); $i++) { 
            if($search[$this->name][$fields[$i]] != ""){
                $type_col = $this->query("SELECT DATA_TYPE AS TYPE FROM INFORMATION_SCHEMA.COLUMNS AS ISC WHERE table_name = 'modules' AND COLUMN_NAME = '".$fields[$i]."' LIMIT 1");
                if($type_col[0]['ISC']['TYPE'] == 'int'){
                    $conditions[$this->name.'.'.$fields[$i]] = $search[$this->name][$fields[$i]];
                }
                elseif($type_col[0]['ISC']['TYPE'] == 'varchar'){
                    $conditions[$this->name.'.'.$fields[$i].' LIKE'] = '%'.$search[$this->name][$fields[$i]].'%';
                }
            }
        }
        $modules = array(
                'fields' => array('Module.*','Categorie.*'),
                'joins' => array(
                        array(
                            'table' => 'categories',
                            'alias' => 'Categorie',
                            'type' => 'LEFT',
                            'conditions' => array('Module.categorie_id = Categorie.id'),
                        ),
                    ),
                'conditions' => $conditions,
                'order' => array('Module.id' => 'ASC')
            );
        if(!is_null($limit)){
            $offset = ($page - 1) * $limit;
            $registros['limit'] = $limit;
            $registros['offset'] = $offset;
            $registros['page'] = $page;

        }
        return $modules;
    }

    public function get_ModulesActive(){
        $modules = $this->find(
            'all',
            array(
                'fields' => array('Module.*'),
                'conditions' => array('state' => 1),
            )
        );
        return $modules;
    }

    public function get_ModuleCategorie($id){
        $module = $this->find(
            'first',
            array(
                'fields' => array('Module.*','Categorie.*'),
                'joins' => array(
                    array(
                        'table' => 'categories',
                        'alias' => 'Categorie',
                        'type' => 'LEFT',
                        'conditions' => array('Module.categorie_id = Categorie.id'),
                    )
                ),
                'conditions' => array('Module.id' => $id)
            )
        );
        return $module;
    }

    public function get_allModuleCategorie(){
        $modules = $this->find(
            'all',
            array(
                'fields' => array('Module.id','Module.name', 'Categorie.name'),
                'joins' => array(
                    array(
                        'table' => 'categories',
                        'alias' => 'Categorie',
                        'type' => 'LEFT',
                        'conditions' => array('Module.categorie_id = Categorie.id')
                    )
                ),
                'order' => array('Categorie.name ASC'),
            )
        );

        return $modules;
    }


    public function get_allModuleCategorieOperation($id){
        $modules = $this->find(
                'all',
                array(
                    'fields' => array('RoleModule.*', 'Module.*', 'Categorie.name'),
                    'joins' => array(
                        array(
                            'table' => 'role_modules',
                            'alias' => 'RoleModule',
                            'type' => 'LEFT',
                            'conditions' => array('RoleModule.module_id = Module.id'),
                        ),
                        array(
                            'table' => 'categories',
                            'alias' => 'Categorie',
                            'type' => 'LEFT',
                            'conditions' => array('Module.categorie_id = Categorie.id'),
                        ),
                    ),
                    'conditions' => array('RoleModule.role_id' => $id),
                    'order' => array('Categorie.name ASC'),
                )
            );
        return $modules;
    }

    public function is_access_module($id, $modulo){
        $module = $this->find(
                        'all',
                        array(
                            'fields' => array('Module.name', 'RoleModule.active'),
                            'joins' => array(
                                array(
                                    'table' => 'role_modules',
                                    'alias' => 'RoleModule',
                                    'type' => 'INNER',
                                    'conditions' => array('RoleModule.module_id = Module.id'),
                                ),
                                 array(
                                    'table' => 'roles',
                                    'alias' => 'Role',
                                    'type' => 'INNER',
                                    'conditions' => array('Role.id = RoleModule.role_id'),
                                ),
                                array(
                                    'table' => 'users',
                                    'alias' => 'User',
                                    'type' => 'INNER',
                                    'conditions' => array('User.role_id = Role.id'),
                                ),
                            ),
                            'conditions' => array(
                                'User.id' => $id,
                                'Module.name_machine' => $modulo,
                            ),
                        )
                    );
        return $module[0]['RoleModule']['active'];
    }

    public function is_access_operation($id, $modulo){
        $module = $this->find(
                        'all',
                        array(
                            'fields' => array('RoleModule.*'),
                            'joins' => array(
                                array(
                                    'table' => 'role_modules',
                                    'alias' => 'RoleModule',
                                    'type' => 'INNER',
                                    'conditions' => array('RoleModule.module_id = Module.id'),
                                ),
                                 array(
                                    'table' => 'roles',
                                    'alias' => 'Role',
                                    'type' => 'INNER',
                                    'conditions' => array('Role.id = RoleModule.role_id'),
                                ),
                                array(
                                    'table' => 'users',
                                    'alias' => 'User',
                                    'type' => 'INNER',
                                    'conditions' => array('User.role_id = Role.id'),
                                ),
                            ),
                            'conditions' => array(
                                'User.id' => $id,
                                'Module.name_machine' => $modulo,
                            ),
                        )
                    );
        return $module;
    }

    public function validateFieldsUnique($fields){
        // variables
        $validate = false; // falso es que no hay datos repetidos que son considerados unicos
    }

    public function getRulesValidation(){
        return $this->rules_validation;
    }

    /**
     * Nombre: setRuleValidation
     * Descripcion: Función que me permite modificar una regla
     * Param Entrada:
     *   - $rules_set = array(), un arreglo que contiene los campos, las reglas y los valores que serán modificados
     * Param Salida:
     *    - $rules = array(), un arreglo con las reglas que se evaluaran
     */

    public function setRulesValidation($rules_set, $rules_validations = array()){
        if(empty($rules_validations)){
            $rules_validations = $this->rules_validation;
        }
        foreach ($rules_set as $key => $value) {
            $rules = $value['rules'];
            $values = $value['values'];
            for ($i=0; $i < count($rules); $i++) {
                $rules_validations[$key]['rules'][$rules[$i]] = $values[$i];
            }
        }
        return $rules_validations;
    }

    /**
     * Nombre: delRuleValidation
     * Descripcion: Función que me permite eliminar las reglas que no serán evaluadas
     * Param Entrada:
     *   - $rules = array(), un arreglo con las reglas que se deben eliminar
     * Param Salida:
     *    - $rules = array(), un arreglo con las reglas que se evaluaran
     */

    public function delRulesValidation($rules_del, $rules_validations = null){
        if(is_null($rules_validations)){
            $rules = $this->rules_validation;
        }
        else{
            $rules = $rules_validations;
        }
        for ($i=0; $i < count($rules_del); $i++) {
            unset($rules[$rules_del[$i]]);
        }
        return $rules;
    }

    public function getFieldImportMap(){
        return $this->fields_import_map;
    }

    public function getFieldUpdateMap(){
        return $this->fields_update_map;
    }


    public function getFieldsUnique(){
        return $this->fields_unique;
    }

    public function getBreadCrumb($name_machine){
        $register = $this->find(
            'first',
            array(
                'fields' => array('Categorie.name', $this->name.'.name'),
                'joins' => array(
                    array(
                        'table' => 'categories',
                        'alias' => 'Categorie',
                        'type' => 'INNER',
                        'conditions' => array($this->name.'.categorie_id = Categorie.id'),
                    ),
                ),
                'conditions' => array($this->name.'.name_machine' => $name_machine),
            )
        );

        $breadCrumb = array();
        foreach ($register as $key => $value) {
            $breadCrumb[] = $value['name'];
        }

        return $breadCrumb;
    }
}
      
?>