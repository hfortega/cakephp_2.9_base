<?php

class LogError extends AppModel {

    public $name = 'LogError';
    public function get_allLogError($search = array(), $page = 1, $limit = 0){
        if(!empty($search[$this->name])){
            $fields = array_keys($search[$this->name]);
        }
        else{
            $fields = $search;
        }
        $conditions = array();
        for ($i=0; $i < count($fields); $i++) { 
            if($search[$this->name][$fields[$i]] != ""){
                $type_col = $this->query("SELECT DATA_TYPE AS TYPE FROM INFORMATION_SCHEMA.COLUMNS AS ISC WHERE table_name = 'log_errors' AND COLUMN_NAME = '".$fields[$i]."' LIMIT 1");
                if($type_col[0]['ISC']['TYPE'] == 'int'){
                    $conditions[$this->name.'.'.$fields[$i]] = $search[$this->name][$fields[$i]];
                }
                elseif($type_col[0]['ISC']['TYPE'] == 'varchar'){
                    $conditions[$this->name.'.'.$fields[$i].' LIKE'] = '%'.$search[$this->name][$fields[$i]].'%';
                }
            }
        }
        $registros = array(
                'conditions' => $conditions,
                'order' => array('event_timestamp DESC'),
            );
        if(!is_null($limit)){
            $offset = ($page - 1) * $limit;
            $registros['limit'] = $limit;
            $registros['offset'] = $offset;
            $registros['page'] = $page;

        }
        return $registros;
    }

    public function saveData($controller, $user, $operation, $description){
        // Aqui guardamos el error
        $error = array(
            'LogError' => array(
                'event_timestamp' => time(),
                'module' => $controller,
                'user' => $user,
                'operation' => $operation,
                'description' => $description,
            )
        );
        $this->save($error);
    }

}
      
?>