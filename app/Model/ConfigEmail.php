<?php

class ConfigEmail extends AppModel {

    public $name = 'ConfigEmail';

    private $types_validation = array(
        'host' => array(
            'rules' => array(
                'allowEmpty' => false,
                'regExpresion' => '/^[a-zA-Z]*[:]?[\/\/]*[a-zA-Z\.]+$/',
                'betweenLength' => '1,250',
            ),
            'messages' => array(
                'regExpresion' => 'Campo Alfanumérico, solo se permiten Letras y los caracteres ( /, :, .)',
                'betweenLength' => 'El campo debe tener mínimo 1 caracteres y un máximo de 250 caracteres.',
                'allowEmpty' => 'Campo Obligatorio',
            )
        ),
        'port' => array(
            'rules' => array(
                'allowEmpty' => false,
                'regExpresion' => '/^[1-9]+[0-9]*$/',
            ),
            'messages' => array(
                'allowEmpty' => 'Campo Obligatorio',
                'regExpresion' => 'Campo númerico donde el primer número no puede ser cero (0)',
            )
        ),
        'username' => array(
            'rules' => array(
                'allowEmpty' => false,
                'regExpresion' => '/[\w-\.]{3,}@([\w-]{2,}\.)*([\w-]{2,}\.)[\w-]{2,4}$/',
                'maxLength' => '250',
            ),
            'messages' => array(
                'regExpresion' => 'Correo Electronico Invalido ej: test@ejemplo.com / test@ejemplo.com.co',
                'maxLength' => 'El campo debe tener un máximo de 250 caracteres.',   
                'allowEmpty' => 'Campo Obligatorio',
            )
        ),
        'tls' => array(
            'rules' => array(
                'allowEmpty' => false,
                'regExpresion' => '/^[1-9]+[0-9]*$/',
            ),
            'messages' => array(
                'allowEmpty' => 'Campo Obligatorio',
                'regExpresion' => 'Campo númerico donde el primer número no puede ser cero (0)',
            )
        ),
    );

    // =============================================================================================

    public function getRulesValidation(){
        return $this->types_validation;
    }
}
      
?>