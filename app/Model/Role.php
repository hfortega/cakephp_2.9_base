<?php

class Role extends AppModel {

    public $name = 'Role';
    private $fields_unique = array('nit' => 'El Nro Documento se encuentra registrado', 'email' => 'El Email se encuentra registrado', 'username' => 'El Nombre Usuario se encuentra registrado');
        private $rules_validation = array(
            'id' => array(
                'rules' => array(
                    'allowEmpty' => false,
                    'regExpresion' => '/^[0-9]+$/',
                ),
                'messages' => array(
                    'regExpresion' => 'Campo Númerico, Números > 0',
                    'allowEmpty' => 'Campo Obligatorio',
                )
            ),
            'name' => array(
                'rules' => array(
                    'allowEmpty' => false,
                    'regExpresion' => '/^[\w\sóÓáÁéÉíÍúÚ]+$/',
                    'betweenLength' => '1,250',
                ),
                'messages' => array(
                    'regExpresion' => 'Campo Alfanumérico, solo se permiten Letras, Números y el caracter _',
                    'betweenLength' => 'El campo debe tener mínimo 1 caracteres y un máximo de 250 caracteres.',
                    'allowEmpty' => 'Campo Obligatorio',
                )
            ),
        );

    public function get_allRole($search = array(), $page = 1, $limit = 0){
        if(!empty($search[$this->name])){
            $fields = array_keys($search[$this->name]);
        }
        else{
            $fields = $search;
        }
        $conditions = array();
        for ($i=0; $i < count($fields); $i++) { 
            if($search[$this->name][$fields[$i]] != ""){
                $type_col = $this->query("SELECT DATA_TYPE AS TYPE FROM INFORMATION_SCHEMA.COLUMNS AS ISC WHERE table_name = 'roles' AND COLUMN_NAME = '".$fields[$i]."' LIMIT 1");
                if($type_col[0]['ISC']['TYPE'] == 'int'){
                    $conditions[$this->name.'.'.$fields[$i]] = $search[$this->name][$fields[$i]];
                }
                elseif($type_col[0]['ISC']['TYPE'] == 'varchar'){
                    $conditions[$this->name.'.'.$fields[$i].' LIKE'] = '%'.$search[$this->name][$fields[$i]].'%';
                }
            }
        }
        $registros = array(
                    'fields' => array('Role.*'),
                    'conditions' => $conditions,
                    

                );
        if(!is_null($limit)){
            $offset = ($page - 1) * $limit;
            $registros['limit'] = $limit;
            $registros['offset'] = $offset;
            $registros['page'] = $page;

        }
        return $registros;
    }

    public function get_allRole_export($search = array()){
        if(!empty($search[$this->name])){
            $fields = array_keys($search[$this->name]);
        }
        else{
            $fields = $search;
        }
        $conditions = array();
        for ($i=0; $i < count($fields); $i++) { 
            if($search[$this->name][$fields[$i]] != ""){
                $type_col = $this->query("SELECT DATA_TYPE AS TYPE FROM INFORMATION_SCHEMA.COLUMNS AS ISC WHERE table_name = 'roles' AND COLUMN_NAME = '".$fields[$i]."' LIMIT 1");
                if($type_col[0]['ISC']['TYPE'] == 'int'){
                    $conditions[$this->name.'.'.$fields[$i]] = $search[$this->name][$fields[$i]];
                }
                elseif($type_col[0]['ISC']['TYPE'] == 'varchar'){
                    $conditions[$this->name.'.'.$fields[$i].' LIKE'] = '%'.$search[$this->name][$fields[$i]].'%';
                }
            }
        }
        $roles = array(
                    'fields' => array('Role.*', 'RoleModule.*','Module.*'),
                    'joins' => array(
                            array(
                                'table' => 'role_modules',
                                'alias' => 'RoleModule',
                                'type' => 'INNER',
                                'conditions' => array('RoleModule.role_id = Role.id'),
                            ),
                            array(
                                'table' => 'modules',
                                'alias' => 'Module',
                                'type' => 'INNER',
                                'conditions' => array('RoleModule.module_id = Module.id'),
                            )
                        ),
                    'conditions' => $conditions,
                );
        return $roles;
    }

    public function user_stateRole($user_id){
    	$state = $this->find(
    		'all',
    		array(
    			'fields' => array('Role.state'),
    			'joins' => array(
    				array(
    					'table' => 'users',
    					'alias' => 'User',
    					'type' => 'LEFT',
    					'conditions' => array('User.role_id=Role.id')
    				)
    			),
    			'conditions' => array('User.id' => $user_id)
    		)
    	);

    	return $state[0]['Role']['state'];
    }

    public function getListRoles(){
        // Sacamos todos los roles
        $roles = $this->find('list', array('fields' => array('id', 'name')));
        return $roles;
    }

        public function existRegisterById($id){
        $register = $this->find('first', array('conditions' => array('id' => $id)));
        if(!empty($register)){
            return true;
        }
        else{
            return false;
        }
    }

    public function validateFieldsUnique($fields){
        // variables
        $validate = false; // falso es que no hay datos repetidos que son considerados unicos
    }

    public function getRulesValidation(){
        return $this->rules_validation;
    }

    /**
    * Nombre: setRuleValidation
    * Descripcion: Función que me permite modificar una regla
    * Param Entrada: 
    *   - $rules_set = array(), un arreglo que contiene los campos, las reglas y los valores que serán modificados
    * Param Salida:
    *    - $rules = array(), un arreglo con las reglas que se evaluaran
    */

    public function setRulesValidation($rules_set, $rules_validations = array()){
        if(empty($rules_validations)){
            $rules_validations = $this->rules_validation;
        }
        foreach ($rules_set as $key => $value) {
            $rules = $value['rules'];
            $values = $value['values'];
            for ($i=0; $i < count($rules); $i++) { 
                $rules_validations[$key]['rules'][$rules[$i]] = $values[$i];
            }
        }
        return $rules_validations;
    }

    /**
    * Nombre: delRuleValidation
    * Descripcion: Función que me permite eliminar las reglas que no serán evaluadas
    * Param Entrada: 
    *   - $rules = array(), un arreglo con las reglas que se deben eliminar
    * Param Salida:
    *    - $rules = array(), un arreglo con las reglas que se evaluaran
    */

    public function delRulesValidation($rules_del, $rules_validations = null){
        if(is_null($rules_validations)){
            $rules = $this->rules_validation;
        }
        else{
            $rules = $rules_validations;
        }
        for ($i=0; $i < count($rules_del); $i++) {
            unset($rules[$rules_del[$i]]);
        }
        return $rules; 
    }

    public function getFieldImportMap(){
        return $this->fields_import_map;
    }

    public function getFieldUpdateMap(){
        return $this->fields_update_map;
    }


    public function getFieldsUnique(){
        return $this->fields_unique;
    }
}
      
?>