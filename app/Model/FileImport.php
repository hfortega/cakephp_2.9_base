<?php

class FileImport extends AppModel {

    public $name = 'FileImport';

    public function get_allFileImport($search = array(), $page = 1, $limit = 0){
        if(!empty($search[$this->name])){
            $fields = array_keys($search[$this->name]);
        }
        else{
            $fields = $search;
        }
        $conditions = array();
        for ($i=0; $i < count($fields); $i++) { 
            if($search[$this->name][$fields[$i]] != ""){
                $type_col = $this->query("SELECT DATA_TYPE AS TYPE FROM INFORMATION_SCHEMA.COLUMNS AS ISC WHERE table_name = 'file_imports' AND COLUMN_NAME = '".$fields[$i]."' LIMIT 1");
                if($type_col[0]['ISC']['TYPE'] == 'int'){
                    $conditions[$this->name.'.'.$fields[$i]] = $search[$this->name][$fields[$i]];
                }
                elseif($type_col[0]['ISC']['TYPE'] == 'varchar'){
                    $conditions[$this->name.'.'.$fields[$i]] = $search[$this->name][$fields[$i]];
                }
            }
        }
        $registros = array(
                'fields' => array('FileImport.*', 'User.*'),
                'joins' => array(
                            array(
                                'table' => 'users',
                                'alias' => 'User',
                                'type' => 'INNER',
                                'conditions' => array('FileImport.user_id = User.id'),
                            ),
                        ),
                'conditions' => $conditions,
                'order' => array('date' => 'DESC'),
            );
        if(!is_null($limit)){
            $offset = ($page - 1) * $limit;
            $registros['limit'] = $limit;
            $registros['offset'] = $offset;
            $registros['page'] = $page;

        }
        return $registros;
    }
}
      
?>