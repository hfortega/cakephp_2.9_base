<?php

class User extends AppModel {

    public $name = 'User';
    private $fields_import_map = array('codigo' => 'id', 'documento_identidad' => 'nit' , 'nombre_completo' => 'name', 'email' => 'email', 'celular' => 'cellphone', 'nombre_usuario' => 'username', 'password' => 'password', 'rol' => 'role_id', 'estado' => 'state');
    private $fields_update_map = array('codigo' => 'id', 'documento_identidad' => 'nit' , 'nombre_completo' => 'name', 'email' => 'email', 'celular' => 'cellphone', 'nombre_usuario' => 'username', 'password' => 'password', 'rol' => 'role_id', 'estado' => 'state');
    private $fields_unique = array('nit' => 'El Nro Documento se encuentra registrado', 'email' => 'El Email se encuentra registrado', 'username' => 'El Nombre Usuario se encuentra registrado');
    private $rules_validation = array(
            'id' => array(
                'rules' => array(
                    'allowEmpty' => false,
                    'regExpresion' => '/^[0-9]+$/',
                ),
                'messages' => array(
                    'regExpresion' => 'Campo Númerico, Números > 0',
                    'allowEmpty' => 'Campo Obligatorio',
                )
            ),
            'nit' => array(
                'rules' => array(
                    'allowEmpty' => false,
                    'regExpresion' => '/^[a-zA-Z0-9]+$/',
                    'betweenLength' => '1,250',
                ),
                'messages' => array(
                    'allowEmpty' => 'Campo Obligatorio',
                    'regExpresion' => 'Campo Alfanumérico, solo se permiten Letras y Números',
                    'betweenLength' => 'El campo debe tener mínimo 5 caracteres y un máximo de 10 caracteres.',
                    
                )
            ),
            'name' => array(
                'rules' => array(
                    'allowEmpty' => false,
                    'regExpresion' => '/^[\w\sáéíóúÁÉÍÓÚ]+$/',
                    'betweenLength' => '1,250',
                ),
                'messages' => array(
                    'regExpresion' => 'Campo Alfanumérico, solo se permiten Letras, Números y el caracter _',
                    'betweenLength' => 'El campo debe tener mínimo 1 caracteres y un máximo de 250 caracteres.',
                    'allowEmpty' => 'Campo Obligatorio',
                )
            ),
            'phone' => array(
                'rules' => array(
                    'allowEmpty' => true,
                    'regExpresion' => '/^(\+?[0-9]+\s?-?\s?)*$/',
                    'betweenLength' => '0,250',
                ),
                'messages' => array(
                    'allowEmpty' => '',
                    'regExpresion' => 'Se permiten Nro(s) de Telefono(s) separados por un guion (-) y el codigo de area del pais si lo necesita ej: +Codigo AreaNro1-Nro2 / Nro1-Nro2 / Nro1.',
                    'betweenLength' => 'El campo debe tener mínimo 0 caracteres y un máximo de 250 caracteres.',
                )
            ),
            'cellphone' => array(
                'rules' => array(
                    'allowEmpty' => false,
                    'regExpresion' => '/^(\+?[0-9]+\s?-?\s?)+$/',
                    'betweenLength' => '10,250',
                ),
                'messages' => array(
                    'regExpresion' => 'Se permiten Nro(s) de Telefono(s) separados por un guion (-) y el codigo de area del pais si lo necesita ej: +Codigo AreaNro1-Nro2 / Nro1-Nro2 / Nro1.',
                    'betweenLength' => 'El campo debe tener mínimo 10 caracteres y un máximo de 250 caracteres.',
                    'allowEmpty' => 'Campo Obligatorio',
                )
            ),
            'email' => array(
                'rules' => array(
                    'allowEmpty' => false,
                    'regExpresion' => '/[\w-\.]{3,}@([\w-]{2,}\.)*([\w-]{2,}\.)[\w-]{2,4}$/',
                    'maxLength' => '250',
                ),
                'messages' => array(
                    'allowEmpty' => 'Campo Obligatorio',
                    'regExpresion' => 'Correo Electronico Invalido ej: test@ejemplo.com / test@ejemplo.com.co',
                    'maxLength' => 'El campo debe tener un máximo de 250 caracteres.',   
                    
                )
            ),
            'role_id' => array(
                'rules' => array(
                    'allowEmpty' => false,
                    'regExpresion' => '/^[1-9]+[0-9]*$/',
                ),
                'messages' => array(
                    'allowEmpty' => 'Campo Obligatorio',
                    'regExpresion' => 'Por favor seleccione un Rol de la lista desplegable para el usuario que esta registrando',
                )
            ),
            'username' => array(
                'rules' => array(
                    'allowEmpty' => false,
                    'regExpresion' => '/^[\$\%\&\.\-\_a-zA-Z0-9]+$/',
                    'betweenLength' => '5, 250',
                ),
                'messages' => array(
                    'allowEmpty' => 'Campo Obligatorio',
                    'betweenLength' => 'El campo debe tener mínimo 5 caracteres y un máximo de 250 caracteres.',
                    'regExpresion' => 'Campo alfanúmerico y puede contener los siguientes caracteres especiales ( $, %, &,. ,- ,_ )',
                )
            ),
            'password' => array(
                'rules' => array(
                    'allowEmpty' => false,
                    'regExpresion' => '/^[\$\%\&\.\-\_a-zA-Z0-9]+$/',
                    'betweenLength' => '8, 250',
                ),
                'messages' => array(
                    'betweenLength' => 'El campo debe tener mínimo 8 caracteres y un máximo de 250 caracteres.',
                    'allowEmpty' => 'Campo Obligatorio',
                    'regExpresion' => 'Campo alfanúmerico y puede contener los siguientes caracteres especiales ( $, %, &,. ,- ,_ )',
                )
            ),
            'state' => array(
                'rules' => array(
                    'allowEmpty' => false,
                    'regExpresion' => '/^(0|1)+$/',
                ),
                'messages' => array(
                    'allowEmpty' => 'Campo Obligatorio',
                    'regExpresion' => 'El estado solamente puede ser Activo(1) / Inactivo(0)',
                )
            ),
        );

    /* Esta funcion me permite devolver todos los usuarios sean que se haya realizado una busqueda o no */
    public function get_allUser($search = array(), $user_id, $page = 1, $limit = 0){
        if(!empty($search[$this->name])){
            $fields = array_keys($search[$this->name]);
        }
        else{
            $fields = $search;
        }
        $conditions = array();
        for ($i=0; $i < count($fields); $i++) { 
            if($search[$this->name][$fields[$i]] != ""){
                $type_col = $this->query("SELECT DATA_TYPE AS TYPE FROM INFORMATION_SCHEMA.COLUMNS AS ISC WHERE table_name = 'users' AND COLUMN_NAME = '".$fields[$i]."' LIMIT 1");
                if($type_col[0]['ISC']['TYPE'] == 'int'){
                    $conditions[$this->name.'.'.$fields[$i]] = $search[$this->name][$fields[$i]];
                }
                elseif($type_col[0]['ISC']['TYPE'] == 'varchar'){
                    $conditions[$this->name.'.'.$fields[$i].' LIKE'] = '%'.$search[$this->name][$fields[$i]].'%';
                }
            }
        }
        if($user_id == 1){
            $conditions['User.id >'] = 0;
        }
        else{
            $conditions['User.id >'] = 1;   
        }
        $conditions['User.state != '] = 2;
        $registros = array(
                    'fields' => array('User.*', 'Role.*'),
                    'joins' => array(
                            array(
                                'table' => 'roles',
                                'alias' => 'Role',
                                'type' => 'INNER',
                                'conditions' => array('User.role_id = Role.id'),
                            ),
                        ),
                    'conditions' => $conditions,
                    
            );
        if(!is_null($limit)){
            $offset = ($page - 1) * $limit;
            $registros['limit'] = $limit;
            $registros['offset'] = $offset;
            $registros['page'] = $page;
        }
        return $registros;
    }

    /* Esta funcion me permite devolver todos los usuarios sean que se haya realizado una busqueda o no */
    public function get_allUser_Export($search = array(),$user_id){
        if(!empty($search[$this->name])){
            $fields = array_keys($search[$this->name]);
        }
        else{
            $fields = $search;
        }
        $conditions = array();
        for ($i=0; $i < count($fields); $i++) { 
            if($search[$this->name][$fields[$i]] != ""){
                $type_col = $this->query("SELECT DATA_TYPE AS TYPE FROM INFORMATION_SCHEMA.COLUMNS AS ISC WHERE table_name = 'users' AND COLUMN_NAME = '".$fields[$i]."' LIMIT 1");
                if($type_col[0]['ISC']['TYPE'] == 'int'){
                    $conditions[$this->name.'.'.$fields[$i]] = $search[$this->name][$fields[$i]];
                }
                elseif($type_col[0]['ISC']['TYPE'] == 'varchar'){
                    $conditions[$this->name.'.'.$fields[$i].' LIKE'] = '%'.$search[$this->name][$fields[$i]].'%';
                }
            }
        }
        if($user_id == 1){
            $conditions['User.id >'] = 0;
        }
        else{
            $conditions['User.id >'] = 1;   
        }
        $registros = array(
                    'fields' => array('User.*','Role.*'),
                    'joins' => array(
                            array(
                                'table' => 'roles',
                                'alias' => 'Role',
                                'type' => 'INNER',
                                'conditions' => array('User.role_id = Role.id'),
                            ),
                        ),
                    'conditions' => $conditions,
            );
        return $registros;
    }

    public function get_allmodules($id, $categorie_id){
        $registros = $this->find(
                        'all',
                        array(
                            'fields' => array('Module.name', 'Module.name_machine'),
                            'joins' => array(
                                array(
                                    'table' => 'roles',
                                    'alias' => 'Role',
                                    'type' => 'LEFT',
                                    'conditions' => array('User.role_id = Role.id'),
                                ),
                                array(
                                    'table' => 'role_modules',
                                    'alias' => 'RoleModule',
                                    'type' => 'LEFT',
                                    'conditions' => array('RoleModule.role_id = Role.id'),
                                ),
                                
                                array(
                                    'table' => 'modules',
                                    'alias' => 'Module',
                                    'type' => 'LEFT',
                                    'conditions' => array('RoleModule.module_id = Module.id'),
                                ),
                            ),
                            'conditions' => array(
                                'User.id' => $id,
                                'RoleModule.active' => 1,
                                'Module.categorie_id' => $categorie_id,
                                'Module.state' => 1,
                            ),
                            'order' => array('Module.name ASC')
                        )
                    );
        return $registros;
    }

    public function user_forceLogout($ids = array(), $forceLogout = 0){
      $this->updateAll(
            array(
                'forceLogout' => $forceLogout
            ),
            array(
                'id' => $ids
            )
        );  
    }

    public function user_getForceLogout($id){
         $registro = $this->find(
            'first', 
            array(
                'fields' => array('User.forceLogout'), 
                'conditions' => array('User.id' => $id)
            )
        );
        return $registro['User']['forceLogout'];
    }

    /**
    * Funcion que me permite obtener todos los id de los usuarios de acuerdo al rol
    */

    public function getUserId_rol($id_rol){
        $registros = $this->find('all',
            array(
                'fields' => array('User.id'),
                'conditions' => array('User.role_id' => $id_rol)
            )
        );
        return $registros;
    }

    public function getUser_id($id){
        $registro = $this->find(
            'first',
            array(
                'fields' => array('User.*', 'Role.*','Departament.*'),
                'joins' => array(
                    array(
                        'table' => 'roles',
                        'alias' => 'Role',
                        'type' => 'LEFT',
                        'conditions' => array('User.role_id=Role.id'),
                    ),
                    array(
                        'table' => 'departaments',
                        'alias' => 'Departament',
                        'type' => 'LEFT',
                        'conditions' => array('User.departament_id=Departament.id'),  
                    )
                ),
                'conditions' => array('User.id' => $id)
            )
        );
        return $registro;
    }

    public function get_UserByUsernamePassword($username, $password){
        $user = $this->find(
            'first', 
            array(
                'fields' => array('User.id', 'User.name', 'User.state'),
                'conditions' => array(
                    'username' => $username, 
                    'password' => $password, 
                    'role_id' => 2)
                )
            );
        return $user;
    }

    public function get_userAccessLvl($user_id){
        $registro = $this->find('first', array('conditions' => array('id' => $user_id)));
        return $registro['User']['access_level'];
    }

    public function exists_userByUsername($username, $rol_id = null){
        if(is_null($rol_id)){
            $conditions = array('username' => $username);
        }
        else{
            $conditions = array('username' => $username, 'role_id' => $rol_id);
        }
        $registro = $this->find('first', array('conditions' => $conditions));
        if(!empty($registro)){
            return 1; // existe el usuario
        }
        else{
            return 0; // no existe el usuario
        }
    }

    public function existRegisterById($id){
        $register = $this->find('first', array('conditions' => array('id' => $id)));
        if(!empty($register)){
            return true;
        }
        else{
            return false;
        }
    }

    public function validateFieldsUnique($fields){
        // variables
        $validate = false; // falso es que no hay datos repetidos que son considerados unicos
    }

    public function getRulesValidation(){
        return $this->rules_validation;
    }

    /**
    * Nombre: setRuleValidation
    * Descripcion: Función que me permite modificar una regla
    * Param Entrada: 
    *   - $rules_set = array(), un arreglo que contiene los campos, las reglas y los valores que serán modificados
    * Param Salida:
    *    - $rules = array(), un arreglo con las reglas que se evaluaran
    */

    public function setRulesValidation($rules_set, $rules_validations = array()){
        if(empty($rules_validations)){
            $rules_validations = $this->rules_validation;
        }
        foreach ($rules_set as $key => $value) {
            $rules = $value['rules'];
            $values = $value['values'];
            for ($i=0; $i < count($rules); $i++) { 
                $rules_validations[$key]['rules'][$rules[$i]] = $values[$i];
            }
        }
        return $rules_validations;
    }

    /**
    * Nombre: delRuleValidation
    * Descripcion: Función que me permite eliminar las reglas que no serán evaluadas
    * Param Entrada: 
    *   - $rules = array(), un arreglo con las reglas que se deben eliminar
    * Param Salida:
    *    - $rules = array(), un arreglo con las reglas que se evaluaran
    */

    public function delRulesValidation($rules_del, $rules_validations = null){
        if(is_null($rules_validations)){
            $rules = $this->rules_validation;
        }
        else{
            $rules = $rules_validations;
        }
        for ($i=0; $i < count($rules_del); $i++) {
            unset($rules[$rules_del[$i]]);
        }
        return $rules; 
    }

    public function getFieldImportMap(){
        return $this->fields_import_map;
    }

    public function getFieldUpdateMap(){
        return $this->fields_update_map;
    }


    public function getFieldsUnique(){
        return $this->fields_unique;
    }
}
?>