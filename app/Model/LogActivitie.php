<?php

class LogActivitie extends AppModel {

    public $name = 'LogActivitie';
    public function saveLog($description, $user, $operation, $object_id, $controller){
    	$log = array(
    		'LogActivitie' => array(
    			'date' => time(),
    			'description' => $description,
    			'user' => $user,
    			'operation' => $operation,
    			'object_id' => $object_id,
    			'controller' => $controller,
    		)
    	);
    	$this->create();
    	$this->save($log);
    }

    public function getLogActivity($object_id, $controller){
        
				$registros = array(
            'fields' => array($this->name.'.*'),
            'conditions' => array('object_id' => $object_id, 'controller' => $controller),
            'order' => array($this->name.'.date' => 'DESC')
        );
        return $registros;
    }

}
      
?>