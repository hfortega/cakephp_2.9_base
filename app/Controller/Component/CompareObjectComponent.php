<?php
	App::uses('Component', 'Controller');
	
	class CompareObjectComponent extends Component{
		
		public function CompareObject($old_object, $current_object){
			$difference = array();
			$keys_current_object = array_keys($current_object);
			for ($i=0; $i < count($keys_current_object); $i++) { 
				if($current_object[$keys_current_object[$i]] != $old_object[$keys_current_object[$i]]){
					$difference[$keys_current_object[$i]] = array(
							'old_value' => $old_object[$keys_current_object[$i]],
							'current_value' => $current_object[$keys_current_object[$i]],
						);
				}
			}
			return $difference;
		}
	}
?>