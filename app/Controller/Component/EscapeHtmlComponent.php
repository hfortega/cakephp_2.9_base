<?php
	App::uses('Component', 'Controller');
	
	class EscapeHtmlComponent extends Component{
		
		public function escapeHtml($data, $tridimensional=1){
			if($tridimensional == 0){
				$registers = array();
				$registers[] = $data;
			}
			else{
				$registers = $data;
			}

			for ($i = 0; $i < count($registers); $i++) {
				$value = $registers[$i];
				$keys_values = array_keys($value);
				for($j = 0; $j < count($keys_values); $j++){
					$key = $keys_values[$j];
					$keys_model = array_keys($value[$key]);
					for ($k = 0; $k < count($keys_model); $k++){
						$key_field = $keys_model[$k];
						$registers[$i][$key][$key_field] = preg_replace('/(<script(\s|\S)*?<\/script>)|(<style(\s|\S)*?<\/style>)|(<!--(\s|\S)*?-->)|(<\/?(\s|\S)*?>)/', '', $registers[$i][$key][$key_field]);
					}
				}
			}
			return $registers;
		}
	}
?>
