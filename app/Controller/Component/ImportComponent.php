<?php
	App::uses('Component', 'Controller');
	App::import('Controller', 'PaymentComponents');
	App::import('Controller', 'Deliveries');
	class ImportComponent extends Component{
		// para cargar el componente Flash
		public $components = array('Flash','CustomValidationField','Auth','ClientProgramRest');

		// BLOQUE DE VARIABLES PRIVADAS
		private $path_dir = WWW_ROOT.'files'.DS.'import'.DS;
		private $path_file = '';
		private $path_file_error = null;
		private $type_import = array('1' => 'Importar Registros', '2' => 'Actualizar Registros');
		private $rules_validations = array();
		private $Model;
		private $num_import_correct = 0;
		private $num_import_failed = 0;
		private $num_import_error = 0;
		
		

		/* Función para obtener el directorio de los archivo que se importan o de los archivos con errores */
		public function getPathDir($model){
        	return $this->path_dir.$model.DS;
        }


        /* Función para obtener el nombre y la extión de un archivo pasando la dirección completa */
        public function getFileName($path_file){
        	$file = $this->getFile($path_file).'.'.$this->getFileExt($path_file);
        	return $file;
        }

		// esta función crea el directorio de acuerdo al modulo en el cual se va a realizar la importación
		private function createDirectory($model_name){
		// verificamos que exista la carpeta de importación
            if(!file_exists($this->path_dir.$model_name.DS)){
                mkdir($this->path_dir.$model_name.DS);
            }
		}


		// Esta función guarda el archivo en el servidor
		private function saveFileImport($name_file_tmp){
			if (!move_uploaded_file($name_file_tmp,$this->path_file)) {
            	return false;        
            }
            else{
            	return true;
            }
		}


		/* Esta función me permite obtener el nombre de un archivo pasandole la ruta del archivo */
		/* Solo regresa el nombre del archivo sin la extensión */
		private function getFile($path_file){
			$file_array = explode('/', $path_file);
			$fileName = explode('.', $file_array[count($file_array)-1])[0];
			return $fileName;
		}


		/* Esta función me permite obtener la extensión de un archivo pasandole la ruta del archivo */
		/* Solo regresa la extensión del archivo */
		private function getFileExt($path_file){
			$file_array = explode('/', $path_file);
			$fileExt = explode('.', $file_array[count($file_array)-1])[1];
			return $fileExt;

			
		}


		/* Esta función me permite registrar en el archivo de errores el erros que haya ocurrido según la fila en la cual haya sucedido el error, esta función se llama por cada iteración de validación, ya sea validando el header o validando cada una de las filas de los archivos de importación o de los archivos de actualizacón */
		private function saveFileError($headers, $rows){
			if(is_null($this->path_file_error)){
				$this->path_file_error = $this->path_dir.$this->Model->name.DS.'Errores_Importacion_Pedidos_'.date('dmY_His').'.csv';
						
			}
			if(file_exists($this->path_file_error)){
				$file = fopen($this->path_file_error, 'a+');
				fputcsv($file, $rows, ',', '"');
			}
			else{
				$file = fopen($this->path_file_error, 'a+');
				fputcsv($file, $headers, ',', '"');	
				fputcsv($file, $rows, ',', '"');	
			}
			fclose($file);
		}


		/* Con esta función se inicia todo el proceso de importación validando el tipo de operación ya que existen en el momento dos (2) tipos de operación [1. Para importar datos, 2. Para actualizar datos] */
		public function import($model_name, $data, $user_id, $username){
			// revisamos si existe un directorio para guardar los archivos de importación
			$this->createDirectory($model_name);
			// cargamos el modelo de la clase que nos llega
			$this->Model = ClassRegistry::init($model_name); // se carga el nombre del modelo ya que de este se extraeran, borrarán o modificarán las validaciones de la información
			$FileImport = ClassRegistry::init('FileImport'); // se carga el modelo FileImport donde se guardará el tipo de importación para mostrar un lod de actividades de las importaciones
			// aqui guardamos el log de importación
			$FileImport->create();
            $file_name = $data['FileImport']['name']['name'];
            $file_name_tmp = $data['FileImport']['name']['tmp_name'];


            $file_name_array = explode('.', $file_name);
            $file_name = $file_name_array[0].'_'.date('dmY_His').'.'.$file_name_array[1];
            

            $import = array('FileImport' => array('date' => date('Y-m-d H:i:s'), 'name' => $file_name, 'model' => $model_name,'user_id' => $user_id, 'type' => $this->type_import[$data['FileImport']['type']]));
            if ($FileImport->save($import)){
                $destino = $this->path_dir.$model_name.DS;
                $this->path_file = $destino.$file_name;
                if(!$this->saveFileImport($file_name_tmp)){
                	// si no se puede guardar el archivo en la carpeta entonces debe borrarlo de la base de datos
                    $FileImport->delete($FileImport->id);
                    // mensaje al usuario
                    $this->Flash->error('Error al subir el archivo, Intentelo nuevamente.');
                }
                else{
                	/**
                	* Extraemos el encabezado y la información independientemente del tipo de Operación
                	*/
                	$rows = $this->extractHeadersInfo();
                	/**
                	* Ahora validamos que no haya ocurrido ningún problema con la lectura del archivo
                	*/
                	if($rows['error']){
                		$this->Flash->error($rows['msg']);
                		return array('error' => true, 'index' => false);
                	}
                	else{
                		$info = $rows['data'];
                	}
                	/**
                	* Ahora separamos la información en encabezado y filas
                	*/
                	$headers = $info[0];
                	/**
                	* Ahora validamos esos encabezados dependiendo del tipo de Operación
                	*/

					// hasta este punto el proceso va bien


                	$headers_errors = $this->validateHeaders($data['FileImport']['type'], $headers);
                	if($headers_errors['error']){
                		/**
                		* Si hay errores en el encabezado entonces los mostramos y reventamos el proceso
                		*/
                		$msg_error = '<p>Presentaron Errores en los encabezados del archivo.</p> <p>'.$headers_errors['msg'].'</p>';
                		return array('error' => true, 'index' => false, 'msg' => $msg_error);
                		// hasta este punto funcionó perfectamente
                	}
                	else{
                		/**
                		* Si no hay errores entonces validamos la información
                		*/
                		$rows_errors = $this->validateRows($data['FileImport']['type'], $headers, $rows['data'], 1, $username);
						$FileImport->updateAll(
							array(
								'file_error' => "'".$this->getFile($this->path_file_error).'.'.$this->getFileExt($this->path_file_error)."'",
								'correct' => $this->num_import_correct,
								'failed' => $this->num_import_failed,
								'error' => $this->num_import_error,
							),
							array(
								'id' => $FileImport->id
							)

						);
						if($rows_errors['error']){
                			/**
                			* Si hay errores en la información que se va a importar o actualizar mostramos un mensaje para que puedan dirigirse a ver el archivo de importación
                			*/
                			// 1. guardamos el nombre del archivo en la base de datos para poderlo ver
                			$this->Flash->error($rows_errors['msg']);
                			return array('error' => true, 'msg' => $rows_errors['msg'] ,'index' => true);

                		}
                		else{
                			return array('error' => false, 'msg' =>$this->num_import_correct.' Correctos / '.$this->num_import_error.' con Errores / '.$this->num_import_failed.' fallaron al guardarse en la base de datos' , 'index' => true);
                		}
                	}
                }
            }
        	else{
        		$this->Flash->error('Error al intentar crear el registro. Por favor, intente nuevamente.');
        		return array('error' => true, 'index' => false);
        	}
        }

		/**
		* Nombre: extractHeadersInfo
		*	Entradas : 
				- $errors: Arreglo donde se almacenarán los errores)
				- $field_validate: El arreglo de los campos mapeados del modelos.
				- $type_import: el tipo de importación (1 - Crear nuesvos registros, 2 - Actualización de registros).
			Salida(s) :
				- $error: Variable que me permitirá saber si hay errores o no, True cuando hay errores, false para cuando no hay errores
		*/
		private function extractHeadersInfo(){
			// bloque de variables
			$error = array();
			$rows = array();
			$is_header = 0;
			$headers = array();
			// abrimos el archivo en modo lectura
			if(($gestor = fopen($this->path_file, "r")) !== FALSE) {
				while ( ($datos = fgetcsv($gestor, 0, ",", '"')) !== FALSE ) {
					// sacamos el encabezado
					if($is_header == 0) {
						for ($i=0; $i < count($datos); $i++) { 
							$headers[] = trim(strtolower($datos[$i]));
						}
						$is_header += 1;
						$rows[] = $headers;
					}
					else {
						$info = array();
						for ($i=0; $i < count($datos); $i++) { 
							$info[$headers[$i]] = $datos[$i];
						}
						$rows[] = $info;
					}
				}
				fclose($gestor);
				$error['error'] = false;
				$error['msg'] = '';
				$error['data'] = $rows;
			}
			else{
				$error['error'] = true;
				$error['msg'] = 'No es posible abrir el archivo para importación';
			}
			return $error;
		}


		private function validateHeaders($type_operation, $headers){
			// Bloque de variables
			$errors = array('error' => false, 'msg' => '');
			$error_header_array = array();
			if($type_operation == 1){
				$fields_headers_import = $this->Model->getFieldImportMap();
				// Si el tipo de importación es CREAR REGISTROS (1) entonces se debe validar que todos los encabezados existan, OJO solo los encabezados.
				for ($i=0; $i < count($fields_headers_import); $i++) {
		            if(!in_array( strtolower( array_keys( $fields_headers_import )[$i] ), $headers ) ) {
		                $error_header_array[] = strtoupper(array_keys($fields_headers_import)[$i]);
						$errors['error'] = true;
					}
		        }
			}
			
			if($type_operation == 2){
				// Si el tipo de importación es ACTUALIZAR REGISTROS (2) entonces debemos validar que los encabezados existan en el modelo, aqui se hace del encabezado al modelo ya que estamos organizando la posibilidad de poder actualizar todos los registros o el que nosotros deseamos
				/**
				* 1. Vamos a extraer cuales son los campos llaves (es decir, los campos que tienen que exsitir por oblogatoriedad en toda actualización)
				* 2. Luego vamos a verificar que esos campos existan en el archivo de actualización
				*/

				$fields_key = $this->Model->getFieldsKeysUpdate();
				for ($i=0; $i < count($fields_key); $i++) { 
					if(!in_array( strtolower( array_keys($fields_key)[$i] ), $headers ) ) {
						$error_header_array[] = "El campo ".strtoupper( array_keys($fields_key)[$i])." es obligatorio que este en el archivo de actualización";
						$errors['error'] = true;	
					}
				}

				/**
				* 3. Ahora lo que vamos a validar es que los campos que tiene el archivo sean los campos permitidos para realizar la actualización, en caso de que haya un campo en el archivo que no este registrado en el mapeo de campos permitidos para actualizar se genera un error
				*/

				$fields_map_update = array_merge($fields_key, $this->Model->getFieldUpdateMap());
				for ($i=0; $i < count($headers); $i++) { 
					if(!in_array( $headers[$i], array_map('strtolower', array_keys($fields_map_update) ) ) ) {
						$error_header_array[] = "El campo ".strtoupper($headers[$i])." no existe o no esta permitido para ser actualizado";
						$errors['error'] = true;	
					}
				}
			}
			
			$errors['msg'] = '['.implode('. <br/>', $error_header_array ).']';
			return $errors;
		}

		private function validateRows($type_operation, $headers, $rows, $index, $username){
			// Bloque de variables
			$errors = array('error' => false, 'msg' => '');
			$header_have_error = 0;
			if($type_operation == 1){
				$fields_maps = $this->Model->getFieldImportMap();
				$rules_validations = $this->Model->getRulesValidation($fields_maps);
				for ($i=1; $i < count($rows); $i++) {
					$row_have_error = 0;
					$error_rows_array = array();
					$fields = $this->mapData($fields_maps, $rows[$i]);
					$errors_validations = $this->CustomValidationField->Validates($this->Model->name, $fields, $rules_validations);
					if(!empty($errors_validations)){
						// si el arreglo de validaciones no esta vacio quiere decir que hay errores entonces creamos un archivo con los errores
						foreach ($errors_validations as $key => $array){
							$error_rows_array[] = $array[0];
						}
						$row_have_error = 1;
					}
					
					/**
					* Bueno en este punto ha pasado que las validaciones de formato y de campos
					* obligatorios han pasado pero ahora lo que debemos de preguntar es lo
					* siguiente, si el model es un deliverie entonces deberemos de realizar otras
					* validaciones más puntuales para los deliveries.
					* 1. Validar : La Suma del precio sin iva + flete debe dar el precio total
					* 2. Validar : El programa exista en la central de programas
					* 3. Validar : Precio Fortunas = ceil(Precio Total / Valor Punto), el valor del punto es extraido del programa.
					*/
					
					if($this->Model->name == 'Deliverie'){
						$errors_other_validations = $this->otherValidationDelivery($fields, $type_operation);
						if($errors_other_validations['error']){
							$error_rows_array[] = $errors_other_validations['msg'];
						}
					}
					
					if(!empty($error_rows_array)){
						if($header_have_error == 0){
							$headers[] = 'errores';
							$header_have_error = 1;
						}
						$row_have_error = 1;
						$fields['errors'] = implode('. ', $error_rows_array);
						$this->saveFileError($headers, array_values($fields));
						$errors['error'] = true;
					}
					if($row_have_error){
						$this->num_import_error += 1;
					}
					else{
						$error_save = $this->saveData($type_operation, $fields, $username);
						if($error_save['error']){
							if($header_have_error == 0){
								$headers[] = 'errores';
								$header_have_error = 1;
							}
							$fields['errors'] = $error_save['msg'];
							$this->saveFileError($headers, array_values($fields));
							$errors['error'] = true;
						}
					}
						
				}
			}
			if($type_operation == 2){
				// aqui realizaremos ciertas validaciones, vamos a traer solo las validaciones que debemos de realizar pero para esto deberemos de juntar los campos obligatorios con los campos adicionales
				$fields_keys_update = $this->Model->getFieldsKeysUpdate();
				$fields_maps = array_merge($fields_keys_update, $this->Model->getFieldUpdateMap());
				// aqui obtenemos solo las validaciones necesarias que se deben de realizar
				$rules_validations = $this->Model->getRulesValidation($fields_maps);
				// aqui vamos a deshabilitar las reglar unicas y allowEmpty de los campo obligatorios
				$rules_sets = array();
				foreach ($fields_keys_update as $key => $value) {
					$rules_sets[$value] = array('rules' => array('allowEmpty', 'unique'), 'values' => array(false, false));
				}
				$rules_validations = $this->Model->setRulesValidation($rules_sets, $rules_validations);
				for ($i=1; $i < count($rows); $i++) {
					$row_have_error = 0;
					$error_rows_array = array();
					$fields = $this->mapData($fields_maps, $rows[$i]);
					$errors_validations = $this->CustomValidationField->Validates($this->Model->name, $fields, $rules_validations);
					if(!empty($errors_validations)){
						// si el arreglo de validaciones no esta vacio quiere decir que hay errores entonces creamos un archivo con los errores
						foreach ($errors_validations as $key => $array){
							$error_rows_array[] = $array[0];
						}
						$row_have_error = 1;
					}

					if($this->Model->name == 'Deliverie'){
						$errors_other_validations = $this->otherValidationDelivery($fields, $type_operation);
						if($errors_other_validations['error']){
							$error_rows_array[] = $errors_other_validations['msg'];
						}
					}
					
					if(!empty($error_rows_array)){
						if($header_have_error == 0){
							$headers[] = 'errores';
							$header_have_error = 1;
						}
						$row_have_error = 1;
						$fields['errors'] = implode('. ', $error_rows_array);
						$this->saveFileError($headers, array_values($fields));
						$errors['error'] = true;
					}
					if($row_have_error){
						$this->num_import_error += 1;
					}
					else{
						$error_save = $this->saveData($type_operation, $fields, $username);
						if($error_save['error']){
							if($header_have_error == 0){
								$headers[] = 'errores';
								$header_have_error = 1;
							}
							$fields['errors'] = $error_save['msg'];
							$this->saveFileError($headers, array_values($fields));
							$errors['error'] = true;
						}
					}
				}
				
			}
			if($errors['error']){
				$errors['msg'] = 'Han ocurrido errores en la importación, por favor revisar el documento de errores para poder obtener más información';
				
			}
			return $errors;
		}

		private function mapData($map, $data){
			$mapped_fields = array();
			foreach ($data as $key => $value) {
				$mapped_fields[$map[strtolower($key)]] = $value;
			}

			return $mapped_fields;
		}

		private function otherValidationDelivery($fields, $type_operation){
			$errors = array('error' => false, 'msg' => '');
			
			if($type_operation == 1){
				if( $fields['price'] + $fields['shipping_cost'] != $fields['total_unit_price'] ){
					$errors['error'] = true;
					$errors['msg'] = 'La suma de: PRECIO SIN IVA + FLETE es diferente del TOTAL. ';
				}
				// Validamos que el componente este creado eso se lo preguntamos al REST de cake_program
				$response = $this->ClientProgramRest->ReadProgram(trim($fields['program_code']));
				if($response->Code == 0){
					if(!$response->Data->exists_program){
						$errors['error'] = true;
						$errors['msg'] .= $response->Data->Program->msg.'. ';
					}
					else{
						// ahora lo que debemos de realizar es lo siguiente
						/**
						* 1. Validar que el nit que se esta introduciendo concuerde con el nit que tiene registrado el programa
						* 2. Validar que el valor del punto que tiene el programa concuerde con el resultado del ceil(total / Precio Puntos)
						*/
						if($fields['customer_id'] != $response->Data->Program->customer_nit){
							$errors['error'] = true;
							$errors['msg'] .= 'El nit del cliente Gluky introducido en el archivo de importación no coincide conn el nit que se tiene registrado en el programa. ';		
						}
						if( ceil($fields['total_unit_price'] / $fields['points_price']) != (int)$response->Data->Program->point_value){
							$errors['error'] = true;
							$errors['msg'] .= 'El valor del punto (Total / Precio Fortunas) no es igual al valor del punto registrado en el programa.';	
						}
					}
				}
				else{
					$errors['error'] = true;
					$errors['msg'] = $response->Msg;
				}
			}
			if($type_operation == 2){
				// aqui hago todo el asunto de buscar el codigo de la transportadora si es que el campo existe y si no esta vacio
				if( isset($fields['courier_name']) && ( !is_null($fields['courier_name']) || $fields['courier_name'] !== ' ' ) ){
					// si esta definido ese campo y no es vacio entonces realizamos una busqueda de la transportadora solo para verificar que la transportadora exista
					// 1. cargamos el modelo courier
					$CourierModel = ClassRegistry::init('Courier');
					$courier = $CourierModel->find('first', array('conditions' => array('name' => $fields['courier_name'])));
					if(empty($courier)){
						$errors['error'] = true;
						$errors['msg'] .= 'La transportadora ('.$fields['courier_name'].') no se encuentra registrada';
					}
				}
			}

			return $errors;

		}



		/**
		* Nombre: importData
		* Parametros:
		*	- Entrada:
		*		* &$errors:	array(), la referencia de un arreglo donde se van organizando todos los errores	
		*		+ $fields: array(), un arreglo con la información que se desea importar
		*		+ $type_import: Int, Un campo número que me dirá que tipo de importación se realizará
		*						(1) para crear nuesvos registros
		*						(2) para actualizar registros
		*/

		private function saveData($type_operation, $fields, $username){
			$errors = array('error' => false, 'msg' => '');
			if($type_operation == 1){
				if($this->Model->name == 'Deliverie'){
					$delivery = array($this->Model->name => $fields);
					$DeliveryController = new DeliveriesController();
					$delivery[$this->Model->name]['instance_user_names'] = trim(preg_replace('/\s{2,}/', ' ', $delivery[$this->Model->name]['instance_user_names']));
					$names = $DeliveryController->splitName($delivery[$this->Model->name]['instance_user_names']);
					$delivery[$this->Model->name]['instance_user_names'] = $names['names'];
					$delivery[$this->Model->name]['instance_user_lastnames'] = $names['lastnames'];

					// aqui llamamos nuevamente la información del programa
					$program = $this->ClientProgramRest->ReadProgram(trim($delivery[$this->Model->name]['program_code']));

					$delivery[$this->Model->name]['customer_name'] = $program->Data->Program->customer_name;
					$delivery[$this->Model->name]['total_price'] = $delivery[$this->Model->name]['total_unit_price'];
					$delivery[$this->Model->name]['central_delivery_state'] = 'new';
					$delivery[$this->Model->name]['event_timestamp'] = strtotime( str_replace('/', '-', $delivery[$this->Model->name]['event_timestamp']).' 01:00:00');
					$delivery[$this->Model->name]['is_third_managed'] = 0;
					// por el momento llenaremos este valor con la división entre el total y el precio en puntos, pero en realidad esta información viene de la consulta del programa
					$delivery[$this->Model->name]['point_value'] = $delivery[$this->Model->name]['total_unit_price'] / $delivery[$this->Model->name]['points_price'];
					$delivery[$this->Model->name]['creation_timestamp'] = time();
					$delivery[$this->Model->name]['modification_timestamp'] = time();
					$delivery[$this->Model->name]['created_by'] = $username;
					$delivery_imported = $this->Model->saveData($delivery);
					if($delivery_imported['error']){
						$this->num_import_failed += 1;
						$errors['error'] = true;
						$errors['msg'] = 'Ha ocurrido un error al intentar guardar la información del pedido en la base de datos, por favor comunicarse con el administrador para obtener más información';
						$this->saveLogError($this->Model->name, $delivery_imported['msg'], $username, $type_operation);
					}
					else{
						// en el caso de que sea un pedido entonces deberemos crear el componente de pago con el id obtenido de la importación
						// cargamos el controlador del pcomponente de pago
						$PaymentComponents = new PaymentComponentsController();
						// cargamos el modelo del componente de pago
						$Model_PaymentComponent = ClassRegistry::init('PaymentComponent');
						// creamos el componente de pago
                        $payment_component = $PaymentComponents->createPaymentComponent($delivery);
                        //guardamos el componente de pago
                        $save_payment_component = $Model_PaymentComponent->saveData($payment_component, $delivery_imported['id']);
                        // verificamos que el componente de pago haya sido guardado
                        if($save_payment_component['error']){
                     		$this->num_import_failed += 1;
                     		// si ha ocurrido un error al intentar guardar el componente de pago entonces hacemos un rollback del pedido
                     		$this->Model->delete($delivery_imported['id']);
                     		$errors['error'] = true;
							$errors['msg'] = 'Ha ocurrido un error al intentar guardar la información del componente de pago en la base de datos, por favor comunicarse con el administrador para obtener más información';		   	
                        		$this->saveLogError('PaymentComponent', $save_payment_component['msg'], $username, $type_operation);
                        }
                        else{
                        	$this->num_import_correct += 1;
                        }	
					}
				}
				else{
					// Aqui realizamos la importación de la información
					$register_to_import = array($this->Model->name => $fields);
					$imported = $this->Model->saveData($register_to_import);
					if($imported['error']){
						$this->num_import_failed += 1;
						$errors['error'] = true;
						$errors['msg'] = 'Ha ocurrido un error al intentar guardar la información en la base de datos, por favor comunicarse con el administrador para obtener más información';
						$this->saveLogError($this->Model->name, $imported['msg'], $username, $type_operation);
					}
					else{
						$this->num_import_correct += 1;
					}
					
				}
			}
			if($type_operation == 2){
				// 1. debemos construir una condición que la enviaremos a una función para obtener el id
				$key_unique = $this->Model->getFieldsKeysUpdate();
				$conditions = array();
				foreach ($variable as $key => $value) {
					$conditions[$value] = $fields[$value];
					// ahora eliminamos lo campos que son obligatorios, los que deben estar si o si para poder realizar la actualización y son campo que no deben de cambiar para nada, asi que los eliminamos del arreglo de actualización
					unset($fields[$value]);
				}
				$register_id = $this->Model->getIdByField($conditions);
				if($this->Model->name == 'Deliverie'){
					// 3. Si estamos en esta parte quiere decir que existe la transportadora
					$CourierModel = ClassRegistry::init('Courier');
					$courier = $CourierModel->find('first', array('conditions' => array('name' => $fields['courier_name'])));
					$fields['courier_id'] = $courier[$Courier->name]['id'];
					unset($fields['courier_name']);
				}
				// ahora lo que haremos es adicionar el id del registro encontrado anteriormente al arreglo
				$fields['id'] = $register_id;
				// ahora construimos el arreglo adecuado para realizar la actualización
				$register_update = array($this->Model->name => $fields);
				// ahora realizamos la actualización
				$updated = $this->Model->updateData($register_update);
				if($updated['error']){
					$this->num_import_failed += 1;
					$errors['error'] = true;
					$errors['msg'] = 'Ha ocurrido un error al intentar guardar la información en la base de datos, por favor comunicarse con el administrador para obtener más información';
					$this->saveLogError($this->Model->name, $updated['msg'], $username, $type_operation);
				}
				else{
					$this->num_import_correct += 1;
				}
			}
			return $errors;
		}


		private function saveLogError($module, $error_msg, $username, $operation){
			$LogError = ClassRegistry::init('LogError'); // cargamos el modelo LogError
			if($operation == 1){
				$operation_text = 'Importación de :: '.$module;
			}
			else{
				$operation_text = 'Actualización de :: '.$module;	
			}
			$logerror = array(
				'LogError' => array(
					'module' => 'Importación - '.$module,
					'description' => $error_msg,
					'user' => $username,
					'event_timestamp' => time(),
					'operation' => $operation_text,
				)
			);
			$LogError->create();
			$LogError->save($logerror);
		}

	}
?>
