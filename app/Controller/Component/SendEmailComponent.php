<?php
	App::uses('Component', 'Controller');
	App::uses('CakeEmail', 'Network/Email');
	
	class SendEmailComponent extends Component{
		public $component = array('EscapeHtml');

        public function SendEmail($subject, $message, $to){
			$ConfigEmail = ClassRegistry::init('ConfigEmail'); // aqui cargamos el modelo de la configuración del email
			$config_email = $ConfigEmail->find('all');
			// primero decodificaremos el password
            $password_encode = $config_email[0]['ConfigEmail']['password'];
            $config_email[0]['ConfigEmail']['password'] = base64_decode($password_encode);
            // aqui hacemos un escape a todos los campos 
            $config_email = $this->EscapeHtml->escapeHtml('ConfigEmail', $config_email);
            if(!empty($config_email)){
                if($config_email[0]['ConfigEmail']['tls'] == 1){
                    $tls = true;
                }
                else{
                    $tls = false;   
                }
                $email = new CakeEmail();
                $password = $config_email[0]['ConfigEmail']['password'];
                //$password = Security::decrypt(, Configure::read('Security.key'));
                $configuracion = array(
                        'host' => $config_email[0]['ConfigEmail']['host'],
                        'port' => $config_email[0]['ConfigEmail']['port'],
                        'username' => $config_email[0]['ConfigEmail']['username'],
                        'password' => $password,
                        'transport' => $config_email[0]['ConfigEmail']['transport'],
                    );
                $email->config($configuracion);
                $email->from(array('hfortega99@gmail.com' => 'Harold Ortega'));
                $email->to(implode(',', $to));
                $email->subject($subject);
                $email->emailFormat('html');
                if($email->send($message)){
                	return true;
                }
                else{
                	return false;
                }
            }
            return false;
		} 
	}
?>