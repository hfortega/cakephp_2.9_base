<?php
	App::uses('Component', 'Controller');
	
	class CustomValidationFieldComponent extends Component{
		private $Model;
        private $field;
        public function Validates($model,$fields, $fields_validation){
			$errors = array();
            $this->Model = ClassRegistry::init($model);
            foreach ($fields_validation as $key => $value) {
                $this->field = $key;
                $rules_validation = $value['rules'];
                $messages_error = $value['messages'];
                foreach ($rules_validation as $key_rule => $value_rule) {
                    $validate = call_user_func_array(array($this, $key_rule), array(addslashes($fields[$key]), $value_rule));
                    if($validate){
                        $errors[$key] = array($messages_error[$key_rule]);
                        break;
                    }
                }
            }
            return $errors;
		}

        private function regExpresion($check, $regular_expresion){
            if(preg_match($regular_expresion, $check)) {
                return false; // false es que el valor del campo esta correcto
            }
            else{
                return true; // true es que fallo la validación 
            }
            
        }

        private function betweenLength($check, $betweenLength){
            $length_array = explode(',', $betweenLength);
            $minLength = $length_array[0];
            $maxLength = $length_array[1];

            if( strlen($check) >= $minLength && strlen($check) <= $maxLength){
                return false; // false es que la longitud de la cadena esta entre el rango designado
            }
            else{
                return true; // true es que la longitud de la cadena no esta en el rango designado
            }
        }

        private function maxLength($check, $maxLength){
            if(strlen($check) <= $maxLength){
                return false; // false es que la longitud de la cadena es menor que el rango designado
            }
            else{
                return true; // true es que la longitud de la cadena no es menor que el rango designado
            }
        }

        private function minLength($check, $minLength){
            if(strlen($check) >= $minLength){
                return false; // false es que la longitud de la cadena es mayor que el rango designado
            }
            else{
                return true; // true es que la longitud de la cadena no es mayor que el rango designado
            }
        }

        private function allowEmpty($check, $allowEmpty){
            
            if( $allowEmpty === false && $check === ''){
              return true; 
            }
            else{
              return false;
            }
        }

        /**
        * Nombre: unique
        * Descripción: Función que me permitirá saber si el campo que voy a chequear ya se encuentra en la base de datos
        * Entrada:
        *   - $check: String, Cadena con el valor que se desea validar
        * Salida:
        *   - bool: true si existe y false si no existe
        */

        private function unique($check, $unique){
            if($unique){
              $register = $this->Model->find('first', array('conditions' => array($this->field => trim($check))));
              if(!empty($register)){
                return true;
              }
              else{
                return false;
              }
            }
            else{
              return false;
            }
        }


        /**
         * Nombre: ValidateUnique
         * Descripción: Función que me permitirá evaluar los campos únicos de cualquier modelo y devolver si son unicos o no
         * Entrada:
         *  - $model, String, Nombre del modelo que se utilizará para validar la informaicón
         *  - $fields_modify, Array, Arreglo con los campos que se modificaron
         *  - $validate, Array, Arreglo con los campos que se han validado anteriormente
         *  - $operation, String, Cadena que me dirá que tipo de operación se estaba realizando
         * Salida:
         *  - $fields_validate, Array, Arreglo con los errores si los hay sino un arreglo vacio
         */

        public function validateUnique($model, $fields_modify, &$validate, $operation = 'new'){
            // cargamos el modelos que vamos a utilizar para realizar las validaciones
            $this->Model = ClassRegistry::init($model);
            $fields_unique = $this->Model->getFieldsUnique();
            for ($i = 0; $i < count($fields_unique); $i++){
                $field_unique = array_keys($fields_unique)[$i];
                // aqui verificamos si lo estamos validando para cuando se ha editado el registro o no
                if($operation == 'edit'){
                    if(array_key_exists($field_unique, $fields_modify)){
                        // si entre los campos que se modificaron existe el campo unique entonces consultamos en la base de datos si este valor existe
                        $register = $this->Model->find('first', array('conditions' => array($field_unique => $fields_modify[$field_unique]['current_value'])));
                        if(!empty($register)){
                            // si existe un registro con esa información entonces adicionamos al arreglo de validaciones
                            $validate[$field_unique] = array($fields_unique[$field_unique]);
                        }
                    }
                }
                elseif ($operation == 'new'){
                    // aqui el recorrido es bastante diferentes
                    if(array_key_exists($field_unique, $fields_modify[$model])){
                        $register = $this->Model->find('first', array('conditions' => array($field_unique => $fields_modify[$model][$field_unique])));
                        if(!empty($register)){
                            $validate[$field_unique] = array($fields_unique[$field_unique]);
                        }
                    }
                }
            }
        }
	}
?>