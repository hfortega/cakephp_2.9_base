<?php

class LogErrorsController extends AppController {

    public $name = 'LogErrors';
    public $layout = 'admin';
    public $uses = array('Categorie','User','Module','Role', 'RoleModule', 'LogError', 'Courier');
    public $components = array('Paginator', 'Flash', 'CustomValidationField', 'EscapeHtml');
    // Variables utilizadas para llamar funciones o mostrar mensajes
    private $module_name = 'LogErrors';
    
    private $controller_name = 'LogErrors';
    private $model_name = 'LogError';
    private $action_list_in_row = array('Editar' => 'edit','Ver' => 'view','Cambiar Estado' => 'change_state','Borrar' => 'delete');
    private $actions_list_icon = array('Editar' => 'Data-Edit-24.png','Ver' => 'View-24.png','Cambiar Estado' => 'Command-Refresh-24.png','Borrar' => 'Garbage-Closed-24.png');

    public function beforeFilter() {
        if (empty($this->params[Configure::read('Routing.admin')]) || !$this->params[Configure::read('Routing.admin')]) {
            $this->Auth->allow($this->params['action']);
        }

        if($this->Session->check('Auth.User.id')){
            // sacamos el nombre de usuario con el que se creo
            $module = $this->Module->find('first', array('conditions' => array('Module.name_machine' => $this->controller_name)));
            $this->module_name_user = $module['Module']['name'];
            $this->Session->delete('menu');
            if($this->User->user_getForceLogout($this->Session->read('Auth.User.id'))){
                $this->User->user_forceLogout(array($this->Session->read('Auth.User.id')));
                $this->Flash->error('Lo sentimos, pero el rol de su cuenta se encuentra desactivado, por favor comuniquese con el administrador');
                $this->requestAction(array('controller' => 'Users', 'action' => 'logout', 'admin' => true));
            }
            else{
                $categorias = $this->Categorie->get_categorieActive();
                $menu = array();
                foreach ($categorias as $categoria) {
                    $modulos = $this->User->get_allmodules($this->Session->read('Auth.User.id'), $categoria['Categorie']['id']);
                    $menu[][$categoria['Categorie']['name']] = $modulos;
                }
                $this->Session->write('menu', $menu);
            }
        }

        parent::beforefilter();
    }


    /**
    * BLOQUE DE FUNCIONES PRIVADAS
    */

    /**
    * BLOQUE DE FUNCIONES PROTEGIDAS
    */

    /**
    * Implementa la función de exportacion según al criterio de busqueda
    */

    /* ************************ */

    public function admin_export($busqueda = array()){
            // validamos que tenga acceso al modulo, si lo tiene no pasa nada pero si no lo tiene lo regresamos al inicio de todo
        $access_operation = $this->Module->is_access_operation($this->Session->read('Auth.User.id'), $this->controller_name);
        $this->set('nombre_module', $this->module_name_user);
        if(!$access_operation[0]['RoleModule']['import']){
            //pr("prueba");
            $this->Flash->error('Este Usuario no tiene permisos para importar la información de este módulo ['.$this->module_name_user.']');
            return $this->redirect(
                array('controller' => 'Mains', 'action' => 'index', 'admin' => true)
            );
        }
        else{
            $this->layout = 'admin';
            $this->Flash->error('Lo sentimos pero la funcionalidad de exportar '.$this->module_name_user.' no esta disponible');
            return $this->redirect(
                array('controller' => $this->controller_name, 'action' => 'index', 'admin' => true)
            );
        }   
    }
    

    public function admin_index() {
        // validamos que tenga acceso al modulo, si lo tiene no pasa nada pero si no lo tiene lo regresamos al inicio de todo
        if(!$this->Module->is_access_module($this->Session->read('Auth.User.id'), $this->controller_name)){
            //pr("prueba");
            $this->Flash->error('Este Usuario no tiene permisos para acceder a este Modulo ['.$this->module_name_user.']');
            return $this->redirect(
                array('controller' => 'Mains', 'action' => 'index', 'admin' => true)
            );
        }
        else{
            $this->layout = 'admin';
            $limit = 20;
            $busqueda = array();
            if(!empty($this->request->data)){
                $this->request->params['named'] = $this->request->data;
            }


            if(isset($this->request->params['named'][$this->model_name])){
                $busqueda = array($this->model_name => $this->request->params['named'][$this->model_name]);
            }
            if(isset($this->request->params['named']['page'])){
                $page = $this->request->params['named']['page'];
            }
            else{
                $page = 1;
            }
            $this->Paginator->settings = $this->LogError->get_allLogError($busqueda, $page, $limit);
            $registros = $this->Paginator->paginate($this->model_name);
            //Creamos la varriable de session para mantener la busqueda
            $this->Session->write($this->model_name, $busqueda);
            // Sacamos los accesos a las operaciones
            $access_operation = $this->Module->is_access_operation($this->Session->read('Auth.User.id'), $this->controller_name);

            // Pasamos por el modulo de limpieza de javascript y html
            $registros = $this->EscapeHtml->escapeHtml($registros);

            $this->set('access_operation',$access_operation[0]);
            $this->set('registros', $registros);
            $this->set('nombre_module', $this->module_name_user);
            $this->set('busqueda', $this->request->data);
            // Paso de la información a las vista
            $this->set('controller_name', $this->controller_name);
            $this->set('model_name', $this->model_name);
            $this->set('action_rows', $this->action_list_in_row);
            $this->set('action_icons', $this->actions_list_icon);
        }
    }


    public function admin_add() {
        // validamos que tenga acceso al modulo, si lo tiene no pasa nada pero si no lo tiene lo regresamos al inicio de todo
        $access_operation = $this->Module->is_access_operation($this->Session->read('Auth.User.id'), $this->controller_name);
        $this->set('nombre_module', $this->module_name_user);
        if(!$access_operation[0]['RoleModule']['add']){
            //pr("prueba");
            $this->Flash->error('Este Usuario no tiene permisos para crear un nuevo ['.$this->module_name_user.']');
            return $this->redirect(
                array('controller' => 'Mains', 'action' => 'index', 'admin' => true)
            );
        }
        else{
            $this->layout = 'admin';
            $this->Flash->error('Lo sentimos pero la funcionalidad de agregar '.$this->module_name_user.' no esta disponible');
            return $this->redirect(
                array('controller' => $this->controller_name, 'action' => 'index', 'admin' => true)
            );
            
        }
    }

    public function admin_edit($id = null) {
        // validamos que tenga acceso al modulo, si lo tiene no pasa nada pero si no lo tiene lo regresamos al inicio de todo
        $access_operation = $this->Module->is_access_operation($this->Session->read('Auth.User.id'), $this->controller_name);
        $this->set('nombre_module', $this->module_name_user);
        if(!$access_operation[0]['RoleModule']['edit']){
            $this->Flash->error('Este Usuario no tiene permisos para editar un ['.$this->module_name_user.']');
            return $this->redirect(
                array('controller' => 'Mains', 'action' => 'index', 'admin' => true)
            );
        }
        else{
            $this->layout = 'admin';
            $this->Flash->error('Lo sentimos pero la funcionalidad de editar '.$this->module_name_user.' no esta disponible');
            return $this->redirect(
                array('controller' => $this->controller_name, 'action' => 'index', 'admin' => true)
            );
        }
            
    }

    public function admin_view($id = null) {
        // validamos que tenga acceso al modulo, si lo tiene no pasa nada pero si no lo tiene lo regresamos al inicio de todo
        $access_operation = $this->Module->is_access_operation($this->Session->read('Auth.User.id'), $this->controller_name);
        $this->set('nombre_module', $this->module_name_user);
        if(!$access_operation[0]['RoleModule']['edit']){
            //pr("prueba");
            $this->Flash->error('Este Usuario no tiene permisos para ver un ['.$this->module_name_user.']');
            return $this->redirect(
                array('controller' => 'Mains', 'action' => 'index', 'admin' => true)
            );
        }
        else{

            $this->layout = 'admin';
            // Aqui saquemos la informacion del usuario que se esta modificando
            $registros = $this->LogError->find('all', array('conditions' => array('id' => $id)));
            // Pasamos por el modulo de limpieza de javascript y html
            $registros = $this->EscapeHtml->escapeHtml($registros); 
            $registro = array_shift($registros);
            $this->set('registro', $registro);
            $this->set('model_name', $this->model_name);
            $this->set('controller_name', $this->controller_name);
            $this->set('nombre_modulo', $this->module_name_user);
        }
    }

    public function admin_delete($id = null) {
        // validamos que tenga acceso al modulo, si lo tiene no pasa nada pero si no lo tiene lo regresamos al inicio de todo
        $access_operation = $this->Module->is_access_operation($this->Session->read('Auth.User.id'), $this->controller_name);
        if(!$access_operation[0]['RoleModule']['delete']){
            //pr("prueba");
            $this->Flash->error('Este Usuario no tiene permisos para borrar un ['.$this->module_name_user.']');
            return $this->redirect(
                array('controller' => 'Mains', 'action' => 'index', 'admin' => true)
            );
        }
        else{

            if (!$id) {
                $this->Flash->error('ID no válido');
                $this->redirect(array('action' => 'index'));
            }
            if ($this->LogError->delete($id)) {
                $this->Flash->success('El registro ha sido borrado exitosamente.');
                $this->redirect(array('action' => 'index'));
            }
        }
    }

    public function admin_change_state($id = null){
        // validamos que tenga acceso al modulo, si lo tiene no pasa nada pero si no lo tiene lo regresamos al inicio de todo
        $access_operation = $this->Module->is_access_operation($this->Session->read('Auth.User.id'), $this->controller_name);
        if(!$access_operation[0]['RoleModule']['change_state']){
            //pr("prueba");
            $this->Flash->error('Este Usuario no tiene permisos para cambiar el estado de un ['.$this->module_name_user.']');
            return $this->redirect(
                array('controller' => 'Mains', 'action' => 'index', 'admin' => true)
            );
        }
        else{ 
            $this->layout = 'admin';
            $this->Flash->error('Lo sentimos pero la funcionalidad de cambiar de estado '.$this->module_name_user.' no esta disponible');
            return $this->redirect(
                array('controller' => $this->controller_name, 'action' => 'index', 'admin' => true)
            ); 
        }
    }

    public function admin_import_index(){
        // validamos que tenga acceso al modulo, si lo tiene no pasa nada pero si no lo tiene lo regresamos al inicio de todo
        $access_operation = $this->Module->is_access_operation($this->Session->read('Auth.User.id'), $this->controller_name);
        $this->set('nombre_module', $this->module_name_user);
        if(!$access_operation[0]['RoleModule']['import']){
            //pr("prueba");
            $this->Flash->error('Este Usuario no tiene permisos para importar la información de este módulo ['.$this->module_name_user.']');
            return $this->redirect(
                array('controller' => 'Mains', 'action' => 'index', 'admin' => true)
            );
        }
        else{
            $this->layout = 'admin';
            $this->Flash->error('Lo sentimos pero la funcionalidad de importar '.$this->module_name_user.' no esta disponible');
            return $this->redirect(
                array('controller' => $this->controller_name, 'action' => 'index', 'admin' => true)
            );
        }   
    }
}

?>