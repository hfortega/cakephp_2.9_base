<?php
	class MainsController extends AppController {
		public $name = 'Mains';
	    public $layout = 'admin';
	    public $uses = array('User','Categorie','Module');
	    public $components = array('Flash');
        public function beforeFilter() {
	        if (empty($this->params[Configure::read('Routing.admin')]) || !$this->params[Configure::read('Routing.admin')]) {
	            $this->Auth->allow($this->params['action']);
	        }

	        if($this->Session->check('Auth.User.id')){
                $this->Session->delete('menu');
            if($this->User->user_getForceLogout($this->Session->read('Auth.User.id'))){
                $this->User->user_forceLogout(array($this->Session->read('Auth.User.id')));
                $this->Flash->error('Lo sentimos, pero el rol de su cuenta se encuentra desactivado, por favor comuniquese con el administrador');
                $this->requestAction(array('controller' => 'Users', 'action' => 'logout', 'admin' => true));
            }
            else{
                $categorias = $this->Categorie->get_categorieActive();
                foreach ($categorias as $categoria) {
                    $modulos = $this->User->get_allmodules($this->Session->read('Auth.User.id'), $categoria['Categorie']['id']);
                    $menu[][$categoria['Categorie']['name']] = $modulos;
                }
                $this->Session->write('menu', $menu);
            }
        }

	        parent::beforefilter();
	    }

	    public function admin_index(){
    		$this->layout = 'admin';
        }

    	public function admin_error($error = null){
    		$this->layout = false;
    		$this->autoRender = false;
    		$this->Flash->error('El acceso a la url :: [ <b>'.str_replace('_', '/', $error).' </b>] no es posible, por favor comunicarse con el administrador');
    		return $this->redirect(
                array('controller' => 'Mains', 'action' => 'index', 'admin' => true)
            );
    	}
	}
?>