<?php

class ModulesController extends AppController {

    public $name = 'Modules';
    public $layout = 'admin';
    public $uses = array('User','Module','Role', 'RoleModule','Categorie');
    public $components = array('Paginator', 'Flash', 'CustomValidationField', 'EscapeHtml');
    

    private $controller_name = 'Modules';
    private $model_name = 'Module';
    private $module_name_user = 'Módulo';
    private $action_list_in_row = array('Editar' => 'edit','Ver' => 'view','Cambiar Estado' => 'change_state','Borrar' => 'delete');
    private $actions_list_icon = array('Editar' => 'Data-Edit-24.png','Ver' => 'View-24.png','Cambiar Estado' => 'Command-Refresh-24.png','Borrar' => 'Garbage-Closed-24.png');
    private $states = [];

    public function beforeFilter() {
        if (empty($this->params[Configure::read('Routing.admin')]) || !$this->params[Configure::read('Routing.admin')]) {
            $this->Auth->allow($this->params['action']);
        }

        if($this->Session->check('Auth.User.id')){
            // sacamos el nombre de usuario con el que se creo
            $module = $this->Module->find('first', array('conditions' => array('Module.name_machine' => $this->controller_name)));
            $this->module_name_user = $module[$this->model_name]['name'];
            $this->Session->delete('menu');
            if($this->User->user_getForceLogout($this->Session->read('Auth.User.id'))){
                $this->User->user_forceLogout(array($this->Session->read('Auth.User.id')));
                $this->Flash->error('Lo sentimos, pero el rol de su cuenta se encuentra desactivado, por favor comuniquese con el administrador');
                $this->requestAction(array('controller' => 'Users', 'action' => 'logout', 'admin' => true));
            }
            else{
                $categorias = $this->Categorie->get_categorieActive();
                $menu = array();
                foreach ($categorias as $categoria) {
                    $modulos = $this->User->get_allmodules($this->Session->read('Auth.User.id'), $categoria['Categorie']['id']);
                    $menu[][$categoria['Categorie']['name']] = $modulos;
                }
                $this->Session->write('menu', $menu);
            }
        }

        parent::beforefilter();
    }

    /**
    * Implementa la función de importación de los módulos con su respectiva vinculación a categorias
    */

    public function admin_export($busqueda = array()){
        // validamos que tenga acceso al modulo, si lo tiene no pasa nada pero si no lo tiene lo regresamos al inicio de todo
        $access_operation = $this->Module->is_access_operation($this->Session->read('Auth.User.id'), $this->controller_name);
        $this->set('nombre_module', $this->module_name_user);
        if(!$access_operation[0]['RoleModule']['export']){
            //pr("prueba");
            $this->Flash->error('Este Usuario no tiene permisos para crear un nuevo ['.$this->module_name_user.']');
            return $this->redirect(
                array('controller' => 'Mains', 'action' => 'index', 'admin' => true)
            );
        }
        else{
            $this->layout = 'ajax';
            $busqueda = $this->Session->read($this->model_name);
            $this->response->download($this->module_name_user."_".date('Ymd').".csv");
            $header = array(
                array(
                    $this->model_name => array(
                        'module_id' => 'codigo_menu', 
                        'module_name' => 'nombre_modulo', 
                        'module_name_machine' => 'nombre_maquina_modulo', 
                        'categorie_id' => 'codigo_menu', 
                        'categorie_name' => 'nombre_menu', 
                        'module_state' => 'estado_modulo'
                    )
                )
            );
            $registros = $this->Module->find('all', $this->Module->get_allModule($busqueda));
            $registro_info = array();
            foreach ($registros as $registro) {
                $registro_info[][$this->model_name] = array(
                    'module_id' => $registro[$this->model_name]['id'], 
                    'module_name' => $registro[$this->model_name]['name'], 
                    'module_name_machine' => $registro[$this->model_name]['name_machine'], 
                    'categorie_id' => $registro['Categorie']['id'],
                    'categorie_name' => $registro['Categorie']['name'], 
                    'module_state' => $registro[$this->model_name]['state']
                );
            }
            unset($registros);
            $registros = array_merge($header, $registro_info);
            $this->set(compact('registros'));
            $this->set('model_name', $this->model_name);
            return;
            
        }
    }

    public function admin_index() {
        // validamos que tenga acceso al modulo, si lo tiene no pasa nada pero si no lo tiene lo regresamos al inicio de todo
        if(!$this->Module->is_access_module($this->Session->read('Auth.User.id'), $this->controller_name)){
            //pr("prueba");
            $this->Flash->error('Este Usuario no tiene permisos para acceder a este Modulo ['.$this->module_name_user.']');
            return $this->redirect(
                array('controller' => 'Mains', 'action' => 'index', 'admin' => true)
            );
        }
        else{
            $this->layout = 'admin';
            $limit = 20;
            $busqueda = array();
            if(!empty($this->request->data)){
                $this->request->params['named'] = $this->request->data;
            }


            if(isset($this->request->params['named'][$this->model_name])){
                $busqueda = array($this->model_name => $this->request->params['named'][$this->model_name]);
            }
            if(isset($this->request->params['named']['page'])){
                $page = $this->request->params['named']['page'];
            }
            else{
                $page = 1;
            }
            $this->Paginator->settings = $this->Module->get_allModule($busqueda, $page, $limit);
            $registros = $this->Paginator->paginate($this->model_name);
            // Pasamos los registros por una limpieza de html
            $registros = $this->EscapeHtml->escapeHtml($registros);
            //Creamos la varriable de session para mantener la busqueda
            $this->Session->write($this->model_name, $busqueda);
            // Sacamos los accesos a las operaciones
            $access_operation = $this->Module->is_access_operation($this->Session->read('Auth.User.id'), $this->controller_name);
            $this->set('access_operation',$access_operation[0]);
            // sacamos todas las categorias
            $categories = $this->Categorie->find('all');
            $this->setState();
            unset($this->states[2]);
            // cargamos el breadcrumb del modelo Module
            $breadcrumb = $this->Module->getBreadCrumb($this->controller_name);
            $this->set('categories', $categories);
            $this->set('registros', $registros);
            $this->set('nombre_module', $this->module_name_user);
            $this->set('busqueda', $this->request->data);
            // Paso de la información a las vista
            $this->set('controller_name', $this->controller_name);
            $this->set('model_name', $this->model_name);
            $this->set('action_rows', $this->action_list_in_row);
            $this->set('action_icons', $this->actions_list_icon);
            $this->set('breadCrumb', $breadcrumb);
            $this->set('states', $this->states);
            $this->set('title_index', __('Listado de módulos'));
        }
    }

    public function admin_add() {
        // validamos que tenga acceso al modulo, si lo tiene no pasa nada pero si no lo tiene lo regresamos al inicio de todo
        $access_operation = $this->Module->is_access_operation($this->Session->read('Auth.User.id'), $this->controller_name);
        $this->set('nombre_module', $this->module_name_user);
        if(!$access_operation[0]['RoleModule']['add']){
            //pr("prueba");
            $this->Flash->error('Este Usuario no tiene permisos para crear un nuevo ['.$this->module_name_user.']');
            return $this->redirect(
                array('controller' => 'Mains', 'action' => 'index', 'admin' => true)
            );
        }
        else{
            if (!empty($this->request->data)) {
                $rules_validation = $this->Module->getRulesValidation();
                $validation = $this->CustomValidationField->Validates($this->model_name, $this->request->data[$this->model_name], $rules_validation);
                if(empty($validation)){
                    $this->Module->create();
                    if ($this->Module->save($this->request->data)) {
                        // sacamos el id del ultimo modulo insertado
                        $module_id = $this->Module->getLastInsertID();
                        // aqui se deben sacar todos los roles y asociar este nuevo modulo con cada uno de los roles
                        $roles = $this->Role->find('all');
                        foreach ($roles as $role) {
                            $role_modules = array();
                            $role_modules['RoleModule']['module_id'] = $module_id;
                            $role_modules['RoleModule']['role_id'] = $role['Role']['id'];
                            $role_modules['RoleModule']['add'] = 0;
                            $role_modules['RoleModule']['edit'] = 0;
                            $role_modules['RoleModule']['delete'] = 0;
                            $role_modules['RoleModule']['active'] = 0;
                            // Aqui guardamos la información de la relación
                            $this->RoleModule->create();
                            if(!$this->RoleModule->save($role_modules)){
                                $this->Module->delete($module_id);
                                $this->Flash->error('Error al intentar crear el registro. Por favor, intente nuevamente.');    
                                $this->redirect(array('action' => 'index'));
                            }
                            unset($role_modules);
                        }
                        $this->Flash->success('El registro se ha creado exitosamente.');
                        $this->redirect(array('action' => 'index'));
                    } else {
                        $this->Flash->error('Error al intentar crear el registro. Por favor, intente nuevamente.');
                    }
                }
                else{
                    $this->set('fields_validation', $validation);
                }
            }
            $this->layout = 'admin';
            // sacamos todas las categorias
            $categories = $this->Categorie->find('all');
            // cargamos el breadcrumb del modelo Module
            $breadcrumb = $this->Module->getBreadCrumb($this->controller_name);
            //adicionamos la acción nuevo al breadcrumb (miga de pan)
            $breadcrumb[] = 'Nuevo';
            $this->set('categories', $categories);
            // Paso de la información a las vista
            $this->set('controller_name', $this->controller_name);
            $this->set('model_name', $this->model_name);
            $this->set('nombre_modulo', $this->module_name_user);
            $this->set('breadCrumb', $breadcrumb);
        }
    }

    public function admin_edit($id = null) {
        // validamos que tenga acceso al modulo, si lo tiene no pasa nada pero si no lo tiene lo regresamos al inicio de todo
        $access_operation = $this->Module->is_access_operation($this->Session->read('Auth.User.id'), $this->controller_name);
        $this->set('nombre_module', $this->module_name_user);
        if(!$access_operation[0]['RoleModule']['edit']){
            //pr("prueba");
            $this->Flash->error('Este Usuario no tiene permisos para editar un ['.$this->module_name_user.']');
            return $this->redirect(
                array('controller' => 'Mains', 'action' => 'index', 'admin' => true)
            );
        }
        else{

            if (!$id && empty($this->request->data))
                $this->redirect(array('action' => 'index'));
            if (!empty($this->request->data)) {
                $rules_validation = $this->Module->getRulesValidation();
                $validation = $this->CustomValidationField->Validates($this->model_name, $this->request->data[$this->model_name], $rules_validation);
                if(empty($validation)){
                    if ($this->Module->save($this->request->data)) {
                        $this->Flash->success('Actualización exitosa');
                        $this->redirect(array('action' => 'index'));
                    } else {
                        $this->Flash->error('Error en la actualización');
                    }
                }
                else{
                    $this->set('fields_validation', $validation);
                }

            }
            if (empty($this->request->data)) {
                $this->request->data = $this->Module->read(null, $id);
            }
            $this->layout = 'admin';
            // Sacamos todos los modulos
            $modulos = $this->Module->get_ModulesActive();
            // cargamos el breadcrumb del modelo Module
            $breadcrumb = $this->Module->getBreadCrumb($this->controller_name);
            //adicionamos la acción nuevo al breadcrumb (miga de pan)
            $breadcrumb[] = 'Editar';
            $this->set('modules', $modulos);
            // sacamos todas las categorias
            $categories = $this->Categorie->find('all');
            $this->set('categories', $categories);
            // Paso de la información a las vista
            $this->set('controller_name', $this->controller_name);
            $this->set('model_name', $this->model_name);
            $this->set('nombre_modulo', $this->module_name_user);
            $this->set('breadCrumb', $breadcrumb);

        }
    }

    public function admin_view($id = null) {
        // validamos que tenga acceso al modulo, si lo tiene no pasa nada pero si no lo tiene lo regresamos al inicio de todo
        $access_operation = $this->Module->is_access_operation($this->Session->read('Auth.User.id'), $this->controller_name);
        $this->set('nombre_module', $this->module_name_user);
        if(!$access_operation[0]['RoleModule']['edit']){
            //pr("prueba");
            $this->Flash->error('Este Usuario no tiene permisos para ver un ['.$this->module_name_user.']');
            return $this->redirect(
                array('controller' => 'Mains', 'action' => 'index', 'admin' => true)
            );
        }
        else{

            $this->layout = 'admin';
            // Aqui saquemos la informacion del usuario que se esta modificando
            $registros = array();
            $registros[] = $this->Module->get_ModuleCategorie($id);
            $registros = $this->EscapeHtml->escapeHtml($registros);
            $registro = array_shift($registros);
            // cargamos el breadcrumb del modelo Module
            $breadcrumb = $this->Module->getBreadCrumb($this->controller_name);
            //adicionamos la acción nuevo al breadcrumb (miga de pan)
            $breadcrumb[] = 'Ver';
            $this->set('registro', $registro);
            $this->set('model_name', $this->model_name);
            $this->set('controller_name', $this->controller_name);
            $this->set('breadCrumb', $breadcrumb);
        }
    }

    public function admin_delete($id = null) {
        // validamos que tenga acceso al modulo, si lo tiene no pasa nada pero si no lo tiene lo regresamos al inicio de todo
        $access_operation = $this->Module->is_access_operation($this->Session->read('Auth.User.id'), $this->controller_name);
        if(!$access_operation[0]['RoleModule']['delete']){
            //pr("prueba");
            $this->Flash->error('Este Usuario no tiene permisos para borrar un ['.$this->module_name_user.']');
            $this->redirect(
                array('controller' => 'Mains', 'action' => 'index', 'admin' => true)
            );
        }
        else{
            if (!$id) {
                $this->Flash->error('ID no válido');
                $this->redirect(array('action' => 'index'));
            }
            /*
            if ($this->User->delete($id)) {
                $this->Flash->success('El registro ha sido borrado exitosamente.');
                $this->redirect(array('action' => 'index'));
            }
            */
            //Sacamos la la información para saber que estado tiene
            else{
                $registro = $this->Module->find('first', array('conditions' => array('id' => $id)));
                $registro_upd = array();
                $registro_upd[$this->model_name]['id'] = $id;
                $registro_upd[$this->model_name]['state'] = 2;
                if($this->User->save($registro_upd)){
                    $registro[$this->model_name]['state'] = $registro_upd[$this->model_name]['state'];
                    $this->saveLogActivity($registro, 'editar');
                    $this->Flash->success('Actualización Exitosa');
                    $this->redirect(array('action' => 'index'));
                }
                else{
                    $this->Flash->error('No se pudo realizar la actualización');
                }
            }
        }

    }

    public function admin_change_state($id = null){
        // validamos que tenga acceso al modulo, si lo tiene no pasa nada pero si no lo tiene lo regresamos al inicio de todo
        $access_operation = $this->Module->is_access_operation($this->Session->read('Auth.User.id'), $this->controller_name);
        if(!$access_operation[0]['RoleModule']['change_state']){
            //pr("prueba");
            $this->Flash->error('Este Usuario no tiene permisos para cambiar el estado de un ['.$this->module_name_user.']');
            return $this->redirect(
                array('controller' => 'Mains', 'action' => 'index', 'admin' => true)
            );
        }
        else{ 
            $this->layout = false;
            $this->autoRender = false;
            if (!$id) {
                $this->Flash->error('ID no válido');
                $this->redirect(array('action' => 'index'));
            }
            else {
                //Sacamos la la información para saber que estado tiene
                $registro = $this->Module->find('first', array('conditions' => array('id' => $id)));
                $registro_upd = array();
                $registro_upd[$this->model_name]['id'] = $id;
                if($registro[$this->model_name]['state'] == 0){
                    $registro_upd[$this->model_name]['state'] = 1;
                }
                else{
                    $registro_upd[$this->model_name]['state'] = 0;
                }
                if($this->Module->save($registro_upd)){
                    $this->Flash->success('Actualización Exitosa');
                    $this->redirect(array('action' => 'index'));
                }
                else{
                    $this->Flash->error('No se pudo realizar la actualización');
                }

            } 
        }
    }

    public function admin_import_index(){
        // validamos que tenga acceso al modulo, si lo tiene no pasa nada pero si no lo tiene lo regresamos al inicio de todo
        $access_operation = $this->Module->is_access_operation($this->Session->read('Auth.User.id'), $this->controller_name);
        $this->set('nombre_module', $this->module_name_user);
        if(!$access_operation[0]['RoleModule']['import']){
            //pr("prueba");
            $this->Flash->error(__('Este Usuario no tiene permisos para importar la información de este módulo [').$this->module_name_user.']');
            return $this->redirect(
                array('controller' => 'Mains', 'action' => 'index', 'admin' => true)
            );
        }
        else{
            $this->layout = 'admin';
            $this->Flash->error(__('Lo sentimos pero no existe la funcionalidad de importar Modulos.'));
            return $this->redirect(
                array('controller' => $this->controller_name, 'action' => 'index', 'admin' => true)
            );
        }
    }

    public function admin_add_import(){
        // validamos que tenga acceso al modulo, si lo tiene no pasa nada pero si no lo tiene lo regresamos al inicio de todo
        $access_operation = $this->Module->is_access_operation($this->Session->read('Auth.User.id'), $this->controller_name);
        $this->set('nombre_module', $this->module_name_user);
        if(!$access_operation[0]['RoleModule']['import']){
            //pr("prueba");
            $this->Flash->error(__('Este Usuario no tiene permisos para importar la información de este módulo [').$this->module_name_user.']');
            return $this->redirect(
                array('controller' => 'Mains', 'action' => 'index', 'admin' => true)
            );
        }
        else{
            $this->layout = 'admin';
            $this->Flash->error(__('Lo sentimos pero no existe la funcionalidad de importar Modulos.'));
            return $this->redirect(
                array('controller' => $this->controller_name, 'action' => 'index', 'admin' => true)
            );
        }
    }

    public function admin_delete_import($id = null) {
        // validamos que tenga acceso al modulo, si lo tiene no pasa nada pero si no lo tiene lo regresamos al inicio de todo
        $access_operation = $this->Module->is_access_operation($this->Session->read('Auth.User.id'), $this->controller_name);
        $this->set('nombre_module', $this->module_name_user);
        if(!$access_operation[0]['RoleModule']['import']){
            //pr("prueba");
            $this->Flash->error(__('Este Usuario no tiene permisos para borrar los archivos de importación de este módulo [').$this->module_name_user.']');
            return $this->redirect(
                array('controller' => 'Mains', 'action' => 'index', 'admin' => true)
            );
        }
        else{
            $this->layout = 'admin';
            $this->Flash->error(__('Lo sentimos pero no existe la funcionalidad de borrar importaciones Modulos.'));
            return $this->redirect(
                array('controller' => $this->controller_name, 'action' => 'index', 'admin' => true)
            );
        }
    }

    public function admin_download_file($id = null, $type = 'normal'){
        // validamos que tenga acceso al modulo, si lo tiene no pasa nada pero si no lo tiene lo regresamos al inicio de todo
        $access_operation = $this->Module->is_access_operation($this->Session->read('Auth.User.id'), $this->controller_name);
        $this->set('nombre_module', $this->module_name_user);
        if(!$access_operation[0]['RoleModule']['import']){
            //pr("prueba");
            $this->Flash->error(__('Este Usuario no tiene permisos para descargar los archivos de importación de este módulo [').$this->module_name_user.']');
            return $this->redirect(
                array('controller' => 'Mains', 'action' => 'index', 'admin' => true)
            );
        }
        else{
            $this->layout = 'admin';
            $this->Flash->error(__('Lo sentimos pero no existe la funcionalidad descargar archivos de importación de Modulos.'));
            return $this->redirect(
                array('controller' => $this->controller_name, 'action' => 'index', 'admin' => true)
            );
        }
    }

    private function setState(){
        /**
         * Estados
         *  0 - Inactivo
         *  1 - Activo
         *  2 - Borrado, cuando un registro esta en este estado no se muestra
         */

        $this->states[] = __('Inactivo');
        $this->states[] = __('Activo');
        $this->states[] = __('Borrado');
    }

    private function saveLogActivity($data, $operation){
        $log_activity = array(
            'LogActivitie' => array(
                'date' => time(),
                'object_id' => $data[$this->model_name]['id'],
                'controller' => $this->controller_name,
                'module_name_user' => $this->module_name_user,
                'user' => $this->Session->read('Auth.User.name'),
                'operation' => __($operation),
                'description' => json_encode($data),
            )
        );
        $this->LogActivitie->save($log_activity);
    }
}

?>