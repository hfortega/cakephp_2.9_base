<?php

class UsersController extends AppController {

    public $name = 'Users';
    public $uses = array('User', 'Module', 'Role', 'RolModule', 'Categorie', 'FileImport', 'LogImport', 'LogActivitie','CakeEmail', 'Network/Email', 'ConfigEmail');
    public $components = array('Paginator', 'Flash', 'SendEmail', 'CustomValidationField','EscapeHtml','Import', 'CompareObject');
    
    /* Variables */
    private $controller_name = 'Users';
    private $model_name = 'User';
    private $module_name_user = '';
    private $action_list_in_row = array('Editar' => 'edit','Ver' => 'view','Activar / Inactivar' => 'change_state','Borrar' => 'delete');
    private $actions_list_icon = array('Editar' => 'Data-Edit-24.png','Ver' => 'View-24.png','Activar / Inactivar' => 'Command-Refresh-24.png','Borrar' => 'Garbage-Closed-24.png');
    private $states = [];
    /* fin bloque de variables */

    public function beforeFilter() {
        if (empty($this->params[Configure::read('Routing.admin')]) || !$this->params[Configure::read('Routing.admin')]) {
            $this->Auth->allow($this->params['action']);
        }

        if($this->Session->check('Auth.User.id')){
            

            // sacamos el nombre de usuario con el que se creo
            $module = $this->Module->find('first', array('conditions' => array('Module.name_machine' => $this->controller_name)));
            $this->module_name_user = $module['Module']['name'];
            $this->Session->delete('menu');
            if($this->User->user_getForceLogout($this->Session->read('Auth.User.id'))){
                $this->User->user_forceLogout(array($this->Session->read('Auth.User.id')));
                $this->Flash->error(__('Lo sentimos, pero el rol de su cuenta se encuentra desactivado, por favor comuniquese con el administrador'));
                $this->requestAction(array('controller' => 'Users', 'action' => 'logout', 'admin' => true));
            }
            else{
                $categorias = $this->Categorie->get_categorieActive();
                $menu = array();
                foreach ($categorias as $categoria) {
                    $modulos = $this->User->get_allmodules($this->Session->read('Auth.User.id'), $categoria['Categorie']['id']);
                    $menu[][$categoria['Categorie']['name']] = $modulos;
                }
                $this->Session->write('menu', $menu);
            }
        }

        parent::beforefilter();
    }

    /**
     * Funcion privadas
     * Me permite eliminar información innecesaria para guardar en la session
    */

    private function cleanUser(){
        $this->layout = false;
        $this->autoRender = false;
        if(count($this->Session->read('Auth.User')) > 1){
            $data = array('email', 'nit', 'phone', 'cellphone', 'state', 'access_level');
            for($i = 0; $i < count($data); $i++){
                $this->Session->delete('Auth.User.'.$data[$i]);
            }    
        }
    }

    /**
     * Fin de las funciones privada
    */
    public function admin_login() {
        $this->layout = 'login';
        if (!empty($this->request->data)) {
            if ($this->Auth->login()) {
                $this->cleanUser();
                $user = $this->User->find('first', array(
                    'fields' => array('User.state'),
                    'conditions' => array('User.id' => $this->Session->read('Auth.User.id')),
                ));
                if($user[$this->model_name]['state']){
                    // Revisamos que el rol no este inactivo
                    if($this->Role->user_stateRole($this->Session->read('Auth.User.id'))){
                        // Aqui sacamos las categorias que se tienen activas
                        $categorias = $this->Categorie->get_categorieActive();
                        // aqui deberia sacar los modulos que tiene activos esto con el fin de poder organizar el menu
                        foreach ($categorias as $categoria) {
                            $modulos = $this->User->get_allmodules($this->Session->read('Auth.User.id'), $categoria['Categorie']['id']);
                            $menu[][$categoria['Categorie']['name']] = $modulos;
                        }
                        $this->Session->write('menu', $menu);
                        $this->redirect(array('controller' => 'Users', 'action' => 'index', 'admin' => true));
                    }
                    else{
                        $this->Flash->error(__('Lo sentimos, pero el rol de su cuenta se encuentra desactivado, por favor comuniquese con el administrador'));
                        $this->requestAction(array('controller' => 'Users', 'action' => 'logout', 'admin' => true));
                    }
                }
                else{
                    $this->Flash->error(__('Lo sentimos, pero su cuenta se encuentra desactivada, por favor comuniquese con el administrador'));
                    $this->requestAction(array('controller' => 'Users', 'action' => 'logout', 'admin' => true));
                }
            } else {
                $this->Flash->error(__('Usuario / Contraseña invalidos'));
                $this->requestAction(array('controller' => 'Users', 'action' => 'logout', 'admin' => true));
            }
        }
    }

    public function admin_logout() {
        // cuando un usuario se deslogue o le de al boton salir se debe colocar el estado conectado de ese usuario en desconectado
        $this->redirect($this->Auth->logout());
    }

    public function admin_export(){
        // validamos que tenga acceso al modulo, si lo tiene no pasa nada pero si no lo tiene lo regresamos al inicio de todo
        $access_operation = $this->Module->is_access_operation($this->Session->read('Auth.User.id'), $this->controller_name);
        $this->set('nombre_module', $this->module_name_user);
        if(!$access_operation[0]['RoleModule']['export']){
            //pr("prueba");
            $this->Flash->error(__('Este Usuario no tiene permisos para crear un nuevo ['.$this->module_name_user.']'));
            return $this->redirect(
                array('controller' => 'Mains', 'action' => 'index', 'admin' => true)
            );
        }
        else{
            $this->layout = 'ajax';
            $user_id = $this->Session->read('Auth.User.id');
            $busqueda = $this->Session->read($this->model_name);
            $this->setState();
            $this->response->download($this->module_name_user."_".date('Ymd').".csv");
            $header = array(
                array(
                    $this->model_name => array(
                        'nit' => __('Nro_Documento'),
                        'name' => __('Nombre'),
                        'phone' => __('Telefonos(s)'),
                        'cellphone' => __('Celular(es)'),
                        'email' => __('Correo_Electronico'),
                        'role_name' => __('Role'),
                        'username' => __('Nombre_Usuario'),
                        'state' => __('Estado')
                        )
                    )
                );
            $users = $this->User->find('all', $this->User->get_allUser_Export($busqueda, $user_id));
            $user_info = array();
            foreach ($users as $user) {
                $user_info[][$this->model_name] = array(
                    'nit' => $user[$this->model_name]['nit'], 
                    'name' => $user[$this->model_name]['name'], 
                    'phone' => $user[$this->model_name]['phone'], 
                    'cellphone' => $user[$this->model_name]['cellphone'], 
                    'email' => $user[$this->model_name]['email'], 
                    'role_name' => $user['Role']['name'], 
                    'username' => $user[$this->model_name]['username'], 
                    'state' => $this->states[$user[$this->model_name]['state']],
                    );
            }
            unset($users);
            $users = array_merge($header, $user_info);
            $this->set(compact('users'));
            $this->set('model_name', $this->model_name);
            return;
        }
    }

    /*
        Nombre: admin_index
    */
    public function admin_index() {
        // validamos que tenga acceso al modulo, si lo tiene no pasa nada pero si no lo tiene lo regresamos al inicio de todo
        if(!$this->Module->is_access_module($this->Session->read('Auth.User.id'), $this->controller_name)){
            //pr("prueba");
            $this->Flash->error(__('Este Usuario no tiene permisos para acceder a este Modulo ['.$this->module_name_user.']'));
            $this->redirect(
                array('controller' => 'Mains', 'action' => 'index', 'admin' => true)
            );
        }
        else{
            $limit = 20;
            $busqueda = array();
            if(!empty($this->request->data)){
                $this->request->params['named'] = $this->request->data;
            }

            if(isset($this->request->params['named'][$this->model_name])){
                $busqueda = array($this->model_name => $this->request->params['named'][$this->model_name]);
            }
            if(isset($this->request->params['named']['page'])){
                $page = $this->request->params['named']['page'];
            }
            else{
                $page = 1;
            }
            $user_id = $this->Session->read('Auth.User.id');
            $this->Paginator->settings = $this->User->get_allUser($busqueda, $user_id, $page, $limit);
            $registros = $this->Paginator->paginate($this->model_name);
            //Creamos la varriable de session para mantener la busqueda
            $this->Session->write($this->model_name, $busqueda);
            // Sacamos los accesos a las operaciones
            $access_operation = $this->Module->is_access_operation($user_id, $this->controller_name);
            // Pasamos los registros por una limpieza de html
            $registros = $this->EscapeHtml->escapeHtml($registros);
            // Cargamos los estado
            $this->setState();
            unset($this->states[2]);
            // cargamos el breadcrumb del modelo Module
            $breadcrumb = $this->Module->getBreadCrumb($this->controller_name);
            // Paso de la información a las vista
            $this->layout = 'admin';
            $this->set('controller_name', $this->controller_name);
            $this->set('model_name', $this->model_name);
            $this->set('registros', $registros);
            $this->set('action_rows', $this->action_list_in_row);
            $this->set('action_icons', $this->actions_list_icon);
            $this->set('roles', $this->Role->getListRoles());
            $this->set('access_operation',$access_operation[0]);
            $this->set('states', $this->states);
            $this->set('title_index', __('Listado de usuarios'));
            $this->set('busqueda', $this->request->data);
            $this->set('breadCrumb', $breadcrumb);
        }
    }

    public function admin_add() {
        // validamos que tenga acceso al modulo, si lo tiene no pasa nada pero si no lo tiene lo regresamos al inicio de todo
        $access_operation = $this->Module->is_access_operation($this->Session->read('Auth.User.id'), $this->controller_name);
        $this->set('nombre_module', $this->module_name_user);
        if(!$access_operation[0]['RoleModule']['add']){
            //pr("prueba");
            $this->Flash->error(__('Este Usuario no tiene permisos para crear un nuevo ['.$this->module_name_user.']'));
            $this->redirect(
                array('controller' => 'Mains', 'action' => 'index', 'admin' => true)
            );
        }
        else{
            $this->layout = 'admin';
            if (!empty($this->request->data)) {
                $rules_validation = $this->User->delRulesValidation(array('id', 'state'));
                $validation = $this->CustomValidationField->Validates($this->model_name, $this->request->data[$this->model_name], $rules_validation);
                // ahora lo que haremos es verificar si los cambios se hicieron en los campos unicos del modelo entonces los evaluamos que sean unicos
                $this->CustomValidationField->validateUnique($this->model_name, $this->request->data, $validation, 'new');
                if(empty($validation)){
                    $this->User->create();
                    $this->request->data[$this->model_name]['password'] = $this->Auth->password($this->request->data[$this->model_name]['password']);
                    if ($this->User->save($this->request->data)) {
                        $this->request->data[$this->model_name]['id'] = $this->User->getLastInsertId();
                        // guardamos este primer registro en las actividades ya que de aqui se iniciará todo el historial
                        $this->saveLogActivity($this->request->data, __('nuevo'));
                        $this->Flash->success(__('El registro se ha creado exitosamente.'));
                        $this->redirect(array('action' => 'index'));
                    } else {
                        $this->Flash->error(__('Error al intentar crear el registro. Por favor, intente nuevamente.'));
                    }
                }
                else{
                    $this->set('fields_validation', $validation);
                    unset($this->request->data[$this->model_name]['password']);
                }
            }
            
            // cargamos el breadcrumb del modelo Module
            $breadcrumb = $this->Module->getBreadCrumb($this->controller_name);
            //adicionamos la acción nuevo al breadcrumb (miga de pan)
            $breadcrumb[] = __('Nuevo');
            // Paso de la información a las vista
            $this->set('controller_name', $this->controller_name);
            $this->set('model_name', $this->model_name);
            $this->set('roles', $this->Role->getListRoles());
            $this->set('breadCrumb', $breadcrumb);
        }
        
    }

    public function admin_edit($id = null) {
        // validamos que tenga acceso al modulo, si lo tiene no pasa nada pero si no lo tiene lo regresamos al inicio de todo
        $access_operation = $this->Module->is_access_operation($this->Session->read('Auth.User.id'), $this->controller_name);
        $this->set('nombre_module', $this->module_name_user);
        if(!$access_operation[0]['RoleModule']['edit']){
            //pr("prueba");
            $this->Flash->error(__('Este Usuario no tiene permisos para editar un ['.$this->module_name_user.']'));
            $this->redirect(
                array('controller' => 'Mains', 'action' => 'index', 'admin' => true)
            );
        }
        else{
            if (!$id && empty($this->request->data))
                //$this->Session->setFlash('Error al sacar la información del usuario', 'flash_error', array('class' => 'flash-edit-error alert'));
                $this->redirect(array('action' => 'index'));
            if (!empty($this->request->data)) {
							  if (!empty($this->request->data[$this->model_name]['password1'])) {
										$this->request->data[$this->model_name]['password'] = $this->request->data[$this->model_name]['password1'];
                    $rules_validation = $this->User->getRulesValidation();
                    $rules_validation = $this->User->delRulesValidation(array('state'), $rules_validation);
                }
                else{
                    $rules_validation = $this->User->delRulesValidation(array('password','state'));
                }
                unset($this->request->data[$this->model_name]['password1']);
                // aqui hacemos las validaciones de formato y longitudes de campos
                $validation = $this->CustomValidationField->Validates($this->model_name, $this->request->data[$this->model_name], $rules_validation); 
                // ahora aqui lo que haremos es sacar si han habido modificaciones.
                $register_old = $this->User->find('first', array('conditions' => array('id' => $this->request->data[$this->model_name]['id'])));
								if(isset($this->request->data[$this->model_name]['password'])){
									$this->request->data[$this->model_name]['password'] = $this->Auth->password($this->request->data[$this->model_name]['password']);
								}
								$difference = $this->CompareObject->CompareObject($register_old[$this->model_name], $this->request->data[$this->model_name]);
                // ahora lo que haremos es verificar si los cambios se hicieron en los campos unicos del modelo entonces los evaluamos que sean unicos
                $this->CustomValidationField->validateUnique($this->model_name, $difference, $validation, 'edit');
                if(empty($validation)){
                    if ($this->User->save($this->request->data)) {
                        // validamos que hayan diferencias si las hay entonces registramos en el log de actividades
                        if(!empty($difference)){
                            // aqui debemos guardar las diferencias en el log de actividades
                            // 1. preparamos el arreglo que me permitirá guardar esas diferencias
                            $this->saveLogActivity($difference, __('editar'));
                        }
                        $this->Flash->success(__('Actualización exitosa'));
                        $this->redirect(array('action' => 'index'));    
                        
                    } else {
                        $this->Flash->error(__('Error en la actualización'));
                    }
                }
                else{
                    if(array_key_exists('password', $validation)){
                        $validation['password1'] = $validation['password'];
                        unset($validation['password']);
                    }
                    $this->set('fields_validation', $validation);
                    unset($this->request->data[$this->model_name]['password']);
                }
            }
            if (empty($this->request->data)) {
                $registros = $this->User->find('all', array('conditions' => array('id' => $id)));
                $registros = $this->EscapeHtml->escapeHtml($registros);
                $registro = array_shift($registros);
                $this->request->data = $registro;
            }
            $this->layout = 'admin';
            // cargamos el breadcrumb del modelo Module
            $breadcrumb = $this->Module->getBreadCrumb($this->controller_name);
            //adicionamos la acción nuevo al breadcrumb (miga de pan)
            $breadcrumb[] = 'Editar';
            // Paso de la información a las vista
            $this->set('controller_name', $this->controller_name);
            $this->set('model_name', $this->model_name);
            $this->set('roles', $this->Role->getListRoles());
            $this->set('breadCrumb', $breadcrumb);
        }
    }

    public function admin_view($id = null){
        // validamos que tenga acceso al modulo, si lo tiene no pasa nada pero si no lo tiene lo regresamos al inicio de todo
        $access_operation = $this->Module->is_access_operation($this->Session->read('Auth.User.id'), $this->controller_name);
        $this->set('nombre_module', $this->module_name_user);
        if(!$access_operation[0]['RoleModule']['edit']){
            //pr("prueba");
            $this->Flash->error(__('Este Usuario no tiene permisos para editar un ['.$this->module_name_user.']'));
            $this->redirect(
                array('controller' => 'Mains', 'action' => 'index', 'admin' => true)
            );
        }
        else{
            if (!$id){
                $this->Flash->error(__('No se ha podido extraer la información del Usuario'));
                $this->redirect(array('action' => 'index'));
            }
            else{
                $this->layout = 'admin';
                // sacamos el usuario para mostrarlo en una vista sin edición
                $registros = $this->User->find('all', array('conditions' => array('id' => $id)));
                $registros = $this->EscapeHtml->escapeHtml($registros);
                $registro = array_shift($registros);

                // cargamos el breadcrumb del modelo Module
                $breadcrumb = $this->Module->getBreadCrumb($this->controller_name);
                //adicionamos la acción nuevo al breadcrumb (miga de pan)
                $breadcrumb[] = 'Ver';
                // Paso de la información a las vista
                $this->set('controller_name', $this->controller_name);
                $this->set('model_name', $this->model_name);
                $this->set('roles', $this->Role->getListRoles());
                $this->set('breadCrumb', $breadcrumb);
                $this->set('registro', $registro);

            }
        }
    }

		private function admin_revisions($id = null, $action){
			// validamos que tenga acceso al modulo, si lo tiene no pasa nada pero si no lo tiene lo regresamos al inicio de todo
			$access_operation = $this->Module->is_access_operation($this->Session->read('Auth.User.id'), $this->controller_name);
			if(!$access_operation[0]['RoleModule']['view_revision']){
				//pr("prueba");
				$this->Flash->error(__('Este Usuario no tiene permisos para ver las revisiones ['.$this->module_name_user.']'));
				$this->redirect(
					array('controller' => 'Mains', 'action' => 'index', 'admin' => true)
				);
			}
			else{
				$this->layout = false;
				$this->autoRender = false;
				if(!$id){
					$this->Flash->error(__('ID no válido'));
					$this->redirect(array('action' => explode('_', $action)[1], 'admin' => true));
				}
				else{
					// variables de traducción
					$traductions = [
						'username' => 'Nombre Usuario',
						'password' => 'Contraseña',
						'name' => 'Nombre Completo',
						'email' => 'Correo Electronico',
						'state' => 'Estado',
						'nit' => 'Nro Documento',
						'phone' => 'Telefono',
						'cellphone' => 'Celular',
						'role_id' => 'Rol',
						'forceLogout' => 'Salida Forzada'
					];
					$revisions = $this->LogActivie->find('all', $this->LogActivitie->getLogActivity($id, $this->request->controller));
					for($i=0; $i < count($revisions); $i++){
						$description_keys = array_keys($revisions[$i]['LogActivitie']['description']);
						$description = $revisions[$i]['LogActivitie']['description'];
						for ($j=0; $j < count($description_keys); $j++){
							//$description
						}
					}
				}
			}
		}

    public function admin_delete($id = null) {
        // validamos que tenga acceso al modulo, si lo tiene no pasa nada pero si no lo tiene lo regresamos al inicio de todo
        $access_operation = $this->Module->is_access_operation($this->Session->read('Auth.User.id'), $this->controller_name);
        if(!$access_operation[0]['RoleModule']['delete']){
            //pr("prueba");
            $this->Flash->error(__('Este Usuario no tiene permisos para borrar un ['.$this->module_name_user.']'));
            $this->redirect(
                array('controller' => 'Mains', 'action' => 'index', 'admin' => true)
            );
        }
        else{
						// organizamos para que no intenten cargar ningún layout
						$this->layout = false;
						$this->autoRender = false;
            if (!$id) {
                $this->Flash->error(__('ID no válido'));
                $this->redirect(array('action' => 'index'));
            }
            /*
            if ($this->User->delete($id)) {
                $this->Flash->success('El registro ha sido borrado exitosamente.');
                $this->redirect(array('action' => 'index'));
            }
            */
            //Sacamos la la información para saber que estado tiene
            else{
                $registro = $this->User->find('first', array('conditions' => array('id' => $id)));
                $registro_upd = array();
                $registro_upd[$this->model_name]['id'] = $id;
                $registro_upd[$this->model_name]['state'] = 2;
                if($this->User->save($registro_upd)){
                    $registro[$this->model_name]['state'] = $registro_upd[$this->model_name]['state'];
                    $this->saveLogActivity($registro, __('eliminar'));
                    $this->Flash->success(__('Actualización Exitosa'));
                    $this->redirect(array('action' => 'index'));
                }
                else{
                    $this->Flash->error(__('No se pudo realizar la actualización'));
                }
            }
        }
        
    }

    public function admin_change_state($id = null){
        // validamos que tenga acceso al modulo, si lo tiene no pasa nada pero si no lo tiene lo regresamos al inicio de todo
        $access_operation = $this->Module->is_access_operation($this->Session->read('Auth.User.id'), $this->controller_name);
        if(!$access_operation[0]['RoleModule']['change_state']){
            //pr("prueba");
            $this->Flash->error(__('Este Usuario no tiene permisos para cambiar el estado de un ['.$this->module_name_user.']'));
            return $this->redirect(
                array('controller' => 'Mains', 'action' => 'index', 'admin' => true)
            );
        }
        else{ 
            $this->layout = false;
            $this->autoRender = false;
            if (!$id) {
                $this->Flash->error(__('ID no válido'));
                $this->redirect(array('action' => 'index'));
            }
            else {
                //Sacamos la la información para saber que estado tiene
                $registro = $this->User->find('first', array('conditions' => array('id' => $id)));
                $registro_upd = array();
                $registro_upd[$this->model_name]['id'] = $id;
                if($registro[$this->model_name]['state'] == 0){
                    $registro_upd[$this->model_name]['state'] = 1;

                }
                else{
                    $registro_upd[$this->model_name]['state'] = 0;
                }
                if($this->User->save($registro_upd)){
                    $registro[$this->model_name]['state'] = $registro_upd[$this->model_name]['state'];
                    $this->saveLogActivity($registro, __('editar'));
                    $this->Flash->success(__('Actualización Exitosa'));
                    $this->redirect(array('action' => 'index'));
                }
                else{
                    $this->Flash->error(__('No se pudo realizar la actualización'));
                }
            }
        }
    }

    /**
    * Funciones para la importación de la información 
    */

    public function admin_import_index(){
         // validamos que tenga acceso al modulo, si lo tiene no pasa nada pero si no lo tiene lo regresamos al inicio de todo
        $access_operation = $this->Module->is_access_operation($this->Session->read('Auth.User.id'), $this->controller_name);
        $this->set('nombre_module', $this->module_name_user);
        if(!$access_operation[0]['RoleModule']['import']){
            //pr("prueba");
            $this->Flash->error(__('Este Usuario no tiene permisos para importar la información de este módulo ['.$this->module_name_user.']'));
            return $this->redirect(
                array('controller' => 'Mains', 'action' => 'index', 'admin' => true)
            );
        }
        else{
            
            $limit = 20;
            $busqueda = array();
            if(!empty($this->request->data)){
                $this->request->params['named'] = $this->request->data;
            }
            if(isset($this->request->params['named'][$this->model_name])){
                $busqueda = array('FileImport' => $this->request->params['named'][$this->model_name]);
            }
            if(isset($this->request->params['named']['page'])){
                $page = $this->request->params['named']['page'];
            }
            else{
                $page = 1;
            }
            $busqueda['FileImport']['model'] = $this->model_name;
            $this->Paginator->settings = $this->FileImport->get_allFileImport($busqueda, $page, $limit);
            $registros = $this->Paginator->paginate('FileImport');
            // cargamos el breadcrumb del modelo Module
            $breadcrumb = $this->Module->getBreadCrumb($this->controller_name);
            $type_import = array('1' => __('Importaciones'), '2' => __('Actualizaciones'));
            $this->layout = 'admin';
            $this->set('access_operation',$access_operation[0]);
            $this->set('registros', $registros);
            $this->set('nombre_module', $this->module_name_user);
            $this->set('users', $this->User->find('list', array('fields' => array('id', 'name'))));
            $this->set('controller_name', $this->controller_name);
            $this->set('model_name', $this->model_name);
            $this->set('breadCrumb', $breadcrumb);
            $this->set('title_index', __('Archivos Importados / Actualizados'));
            $this->set('type_import', $type_import);
            
        }
    }

    public function admin_add_import(){
         // validamos que tenga acceso al modulo, si lo tiene no pasa nada pero si no lo tiene lo regresamos al inicio de todo
        $access_operation = $this->Module->is_access_operation($this->Session->read('Auth.User.id'), $this->controller_name);
        $this->set('nombre_module', $this->module_name_user);
        if(!$access_operation[0]['RoleModule']['import']){
            //pr("prueba");
            $this->Flash->error(__('Este Usuario no tiene permisos para importar la información de este módulo ['.$this->module_name_user.']'));
            return $this->redirect(
                array('controller' => 'Mains', 'action' => 'index', 'admin' => true)
            );
        }
        else{
            $this->layout = 'admin';
            $this->set('nombre_module', $this->module_name_user);
            $this->set('header_import', $this->User->getFieldImportMap());
            $this->set('header_update', $this->User->getFieldUpdateMap());
            $this->set('controller', $this->controller_name);
            $this->set('model', $this->model_name);
            //sacamos todas las lineas que estan activas
            if (!empty($this->request->data)) {
                $this->Import->import($this->model_name, $this->request->data, $this->Session->read('Auth.User.id'));
                $this->redirect(array('action' => 'import_index'));
            }  
        }
    }

    public function admin_delete_import($id = null) {
        // validamos que tenga acceso al modulo, si lo tiene no pasa nada pero si no lo tiene lo regresamos al inicio de todo
        $access_operation = $this->Module->is_access_operation($this->Session->read('Auth.User.id'), $this->controller_name);
        if(!$access_operation[0]['RoleModule']['delete']){
            //pr("prueba");
            $this->Flash->error(__('Este Usuario no tiene permisos para borrar un ['.$this->module_name_user.']'));
            return $this->redirect(
                array('controller' => 'Mains', 'action' => 'index', 'admin' => true)
            );
        }
        else{

            if (!$id) {
                $this->Flash->error('ID no válido');
                $this->redirect(array('action' => 'import_index'));
            }
            else{
                // sacamos el nombre del archivo
                $file_import = $this->FileImport->find('first', array('id' => $id));
                // intentamos borrar el archivo
                $destino = WWW_ROOT.'files'.DS.'import'.DS;
                if(unlink($destino.$file_import['FileImport']['name'])){
                    if ($this->FileImport->delete($id)) {
                        $this->Flash->success(__('El registro ha sido borrado exitosamente.'));
                        $this->redirect(array('action' => 'import_index'));
                    }
                }
                else{
                   $this->Flash->error(__('Ocurrio un error al borrar el registro.'));
                        $this->redirect(array('action' => 'import_index')); 
                }
            }
        }
    }

    public function admin_download_file($id = null, $type = 'normal'){
        // validamos que tenga acceso al modulo, si lo tiene no pasa nada pero si no lo tiene lo regresamos al inicio de todo
        $access_operation = $this->Module->is_access_operation($this->Session->read('Auth.User.id'), $this->controller_name);
        $this->set('nombre_module', $this->module_name_user);
        if(!$access_operation[0]['RoleModule']['import']){
            //pr("prueba");
            $this->Flash->error(__('Este Usuario no tiene permisos para importar la información de este módulo ['.$this->module_name_user.']'));
            return $this->redirect(
                array('controller' => 'Mains', 'action' => 'index', 'admin' => true)
            );
        }
        else{
            $this->layout = false;
            $this->autoRender = false;
            // Sacamos la información del archivo 
            $file = $this->FileImport->find('first', array('conditions' => array('id' => $id)));
            if($type == 'normal'){
                $path = $this->Import->getPathDir($this->model_name).$file['FileImport']['name'];
            }
            else{
                $path = $this->Import->getPathDir($this->model_name).$file['FileImport']['file_error'];
            }
            $this->response->file($path, array(
                'download' => true,
                'name' => $this->Import->getFileName($path),
            ));
            return $this->response;
        }
    }

    public function admin_account(){
        $this->layout = 'admin';
        // cargamos el breadcrumb del modelo Module
        $breadcrumb = $this->Module->getBreadCrumb($this->controller_name);
        //adicionamos la acción nuevo al breadcrumb (miga de pan)
        $breadcrumb[] = 'Mi cuenta';
        // Paso de la información a las vista
        $this->set('controller_name', $this->controller_name);
        $this->set('model_name', $this->model_name);
        $this->set('roles', $this->Role->getListRoles());
        $this->set('breadCrumb', $breadcrumb);
        if (!empty($this->request->data)) {
            if (!empty($this->request->data[$this->model_name]['password1'])) {
                $this->request->data[$this->model_name]['password'] = $this->request->data[$this->model_name]['password1'];
                $rules_validation = $this->User->getRulesValidation();
                $rules_validation = $this->User->delRulesValidation(array('state'), $rules_validation);
            }
            else{
                $rules_validation = $this->User->delRulesValidation(array('password','state'));
            }
            unset($this->request->data[$this->model_name]['password1']);
            // aqui hacemos las validaciones de formato y longitudes de campos
            $validation = $this->CustomValidationField->Validates($this->model_name, $this->request->data[$this->model_name], $rules_validation);
            // ahora aqui lo que haremos es sacar si han habido modificaciones.
            $register_old = $this->User->find('first', array('conditions' => array('id' => $this->request->data[$this->model_name]['id'])));
						if(isset($this->request->data[$this->model_name]['password'])){
							$this->request->data[$this->model_name]['password'] = $this->Auth->password($this->request->data[$this->model_name]['password']);
						}
						$difference = $this->CompareObject->CompareObject($register_old[$this->model_name], $this->request->data[$this->model_name]);
            // ahora lo que haremos es verificar si los cambios se hicieron en los campos unicos del modelo entonces los evaluamos que sean unicos
            $this->CustomValidationField->validateUnique($this->model_name, $difference, $validation, 'edit');
            if(empty($validation)){
                if ($this->User->save($this->request->data)) {
                    // validamos que hayan diferencias si las hay entonces registramos en el log de actividades
                    if(!empty($difference)){
                        // aqui debemos guardar las diferencias en el log de actividades
                        // 1. preparamos el arreglo que me permitirá guardar esas diferencias
                        $this->saveLogActivity($difference, 'editar');
                    }
                    $this->Flash->success(__('Actualización exitosa'));
                } else {
                    $this->Flash->error(__('Error en la actualización'));
                }
            }
            else{
                if(array_key_exists('password', $validation)){
                    $validation['password1'] = $validation['password'];
                    unset($validation['password']);
                }
                $this->set('fields_validation', $validation);
                unset($this->request->data[$this->model_name]['password']);
            }
        }
        if (empty($this->request->data)) {
            $registros = $this->User->find('all', array('conditions' => array('id' => $this->Session->read('Auth.User.id'))));
            $registros = $this->EscapeHtml->escapeHtml($registros);
            $registro = array_shift($registros);
            $this->request->data = $registro;
            //$this->request->data = $this->User->read(null, $this->Session->read('Auth.User.id'));
        }
    }

    public function recovery($username){
        $this->layout = false;
        $this->autoRender = false;
        
        //$username = $_POST['username'];
        //buscamos este usuario
        $user = $this->User->find('first', array('conditions' => array('username' => $username)));
        if(!empty($user)){
            // generamos el password
            $password_new = $this->generatePass();
            // lo guardamos en la base de datos
            $this->User->updateAll(
                array(
                    'password' => "'".$this->Auth->password($password_new)."'"
                ),
                array(
                    'id' => $user['User']['id']
                )
            );

            // organizamos el mensaje
            $message = __('Cordial Saludos <br/><br/> '.$user['User']['name'].'<br/> Usted ha solicitado una recuperación de su contraseña, el sistema le ha enviado una contraseña aleatoria la cual le servirá para ingresar al sistema, una vez ingrese usted puede cambiar la contraseña desde el menu Mi Cuenta. 
                    <br/><br/> contraseña :: '.$password_new.'
                    <br/><br/>P.D Recuerde que este correo es meramente informativo y por ningún motivo responda a esta dirección ya que nunca será respondida su inquietud.');

            if($this->SendEmail->SendEmail('Recuperación de contraseña', $message, array($user['User']['email']))){
                echo __('Se ha enviado un correo electronico a ('.$user['User']['email'].') con información sobre la recuperación de la contraseña. Por Favor revisar su bandeja principal o su bandeja de correo no deseado (spam)');
            }
            else{
                echo __("Ha ocurrido un error al enviar el correo electronico,");
            }
        }
        else{
            echo __('El usuario ('.$username.') no se encuentra registrado.');
        }
    }

    private function generatePass(){
        $this->layout = false;
        $this->autoRender = false;
        //Se define una cadena de caractares. Te recomiendo que uses esta.
        $cadena = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
        //Obtenemos la longitud de la cadena de caracteres
        $longitudCadena=strlen($cadena);
         
        //Se define la variable que va a contener la contraseña
        $pass = "";
        //Se define la longitud de la contraseña, en mi caso 10, pero puedes poner la longitud que quieras
        $longitudPass=10;
         
        //Creamos la contraseña
        for($i=1 ; $i<=$longitudPass ; $i++){
            //Definimos numero aleatorio entre 0 y la longitud de la cadena de caracteres-1
            $pos=rand(0,$longitudCadena-1);
         
            //Vamos formando la contraseña en cada iteraccion del bucle, añadiendo a la cadena $pass la letra correspondiente a la posicion $pos en la cadena de caracteres definida.
            $pass .= substr($cadena,$pos,1);
        }
        return $pass;
    }

    private function setState(){
        /**
         * Estados
         *  0 - Inactivo
         *  1 - Activo
         *  2 - Borrado, cuando un registro esta en este estado no se muestra 
         */

        $this->states[] = __('Inactivo');
        $this->states[] = __('Activo');
        $this->states[] = __('Borrado');
    }

    private function saveLogActivity($data, $operation){
        $log_activity = array(
            'LogActivitie' => array(
                'date' => time(),
                'object_id' => $data[$this->model_name]['id'],
                'controller' => $this->controller_name,
                'module_name_user' => $this->module_name_user,
                'user' => $this->Session->read('Auth.User.name'),
                'operation' => __($operation),
                'description' => json_encode($data),
            )
        );
        $this->LogActivitie->save($log_activity);
    }
}

?>