<?php

class ConfigEmailsController extends AppController {

    public $name = 'ConfigEmails';
    public $layout = 'admin';
    public $uses = array('User','ConfigEmail','Module','RoleModule','Categorie');
    public $components = array('Flash', 'CustomValidationField', 'EscapeHtml');
    // Variables utilizadas para llamar funciones o mostrar mensajes
    private $controller_name = 'ConfigEmails';
    private $model_name = 'ConfigEmail';
    private $module_name_user = 'Configuración Correo';
    public function beforeFilter() {
        if (empty($this->params[Configure::read('Routing.admin')]) || !$this->params[Configure::read('Routing.admin')]) {
            $this->Auth->allow($this->params['action']);
        }

        if($this->Session->check('Auth.User.id')){
            $this->Session->delete('menu');
            if($this->User->user_getForceLogout($this->Session->read('Auth.User.id'))){
                $this->User->user_forceLogout(array($this->Session->read('Auth.User.id')));
                $this->Flash->error('Lo sentimos, pero el rol de su cuenta se encuentra desactivado, por favor comuniquese con el administrador');
                $this->requestAction(array('controller' => 'Users', 'action' => 'logout', 'admin' => true));
            }
            else{
                $categorias = $this->Categorie->get_categorieActive();
                foreach ($categorias as $categoria) {
                    $modulos = $this->User->get_allmodules($this->Session->read('Auth.User.id'), $categoria['Categorie']['id']);
                    $menu[][$categoria['Categorie']['name']] = $modulos;
                }
                $this->Session->write('menu', $menu);
            }
        }

        parent::beforefilter();
    }

    public function admin_index(){
        // validamos que tenga acceso al modulo, si lo tiene no pasa nada pero si no lo tiene lo regresamos al inicio de todo
        if(!$this->Module->is_access_module($this->Session->read('Auth.User.id'), $this->controller_name)){
            //pr("prueba");
            $this->Flash->error('Este Usuario no tiene permisos para acceder a este Modulo ['.$this->module_name_user.']');
            return $this->redirect(
                array('controller' => 'Mains', 'action' => 'index', 'admin' => true)
            );
        }
        else{
            // cargamos el breadcrumb del modelo Module
            $breadcrumb = $this->Module->getBreadCrumb($this->controller_name);
            //adicionamos la acción nuevo al breadcrumb (miga de pan)
            $breadcrumb[] = 'Editar';

            $this->layout = 'admin';
            $this->set('controller_name', $this->controller_name);
            $this->set('model_name', $this->model_name);
            $this->set('breadCrumb', $breadcrumb);
            if (!empty($this->request->data)) {
                $rules_validation = $this->ConfigEmail->getRulesValidation();
                $validation = $this->CustomValidationField->Validates($this->model_name, $this->request->data[$this->model_name], $rules_validation); 
                if(empty($validation)){
                    if(!empty($this->request->data['ConfigEmail']['password1'])){
                        $password = $this->request->data['ConfigEmail']['password1'];
                        unset($this->request->data['ConfigEmail']['password1']);    
                        $password_encrypt = base64_encode($password);
                        $this->request->data['ConfigEmail']['password'] = $password_encrypt;
                    }
                    if ($this->ConfigEmail->save($this->request->data)) {
                        $this->Flash->success('Actualización exitosa');
                    } else {
                        $this->Flash->error('Error en la actualización');
                    }
                }
                else{
                    $this->set('fields_validation', $validation);
                }
                
            }
            if (empty($this->request->data)) {
                $registros = $this->ConfigEmail->find('all');
                $registros = $this->EscapeHtml->escapeHtml($registros);
                
                if(!empty($registros)){
                    $registro = array_shift($registros);
                    $this->request->data = $registro;    
                }
                
            }
        }
    }

    public function admin_add(){
        // validamos que tenga acceso al modulo, si lo tiene no pasa nada pero si no lo tiene lo regresamos al inicio de todo
        $access_operation = $this->Module->is_access_operation($this->Session->read('Auth.User.id'), $this->controller_name);
        $this->set('nombre_module', $this->module_name_user);
        if(!$access_operation[0]['RoleModule']['import']){
            //pr("prueba");
            $this->Flash->error('Este Usuario no tiene permisos para agregar la información de este módulo ['.$this->module_name_user.']');
            return $this->redirect(
                array('controller' => 'Mains', 'action' => 'index', 'admin' => true)
            );
        }
        else{
            $this->layout = 'admin';
            $this->Flash->error('Lo sentimos pero no existe la funcionalidad de Agregar configuraciones de email.');
            return $this->redirect(
                array('controller' => $this->controller_name, 'action' => 'index', 'admin' => true)
            );
        }   
    }

    public function admin_edit(){
        // validamos que tenga acceso al modulo, si lo tiene no pasa nada pero si no lo tiene lo regresamos al inicio de todo
        $access_operation = $this->Module->is_access_operation($this->Session->read('Auth.User.id'), $this->controller_name);
        $this->set('nombre_module', $this->module_name_user);
        if(!$access_operation[0]['RoleModule']['import']){
            //pr("prueba");
            $this->Flash->error('Este Usuario no tiene permisos para editar la información de este módulo ['.$this->module_name_user.']');
            return $this->redirect(
                array('controller' => 'Mains', 'action' => 'index', 'admin' => true)
            );
        }
        else{
            $this->layout = 'admin';
            $this->Flash->error('Lo sentimos pero no existe la funcionalidad de Editar configuraciones de email.');
            return $this->redirect(
                array('controller' => $this->controller_name, 'action' => 'index', 'admin' => true)
            );
        }   
    }

    public function admin_view(){
        // validamos que tenga acceso al modulo, si lo tiene no pasa nada pero si no lo tiene lo regresamos al inicio de todo
        $access_operation = $this->Module->is_access_operation($this->Session->read('Auth.User.id'), $this->controller_name);
        $this->set('nombre_module', $this->module_name_user);
        if(!$access_operation[0]['RoleModule']['import']){
            //pr("prueba");
            $this->Flash->error('Este Usuario no tiene permisos para ver la información de este módulo ['.$this->module_name_user.']');
            return $this->redirect(
                array('controller' => 'Mains', 'action' => 'index', 'admin' => true)
            );
        }
        else{
            $this->layout = 'admin';
            $this->Flash->error('Lo sentimos pero no existe la funcionalidad de Ver configuraciones de email.');
            return $this->redirect(
                array('controller' => $this->controller_name, 'action' => 'index', 'admin' => true)
            );
        }   
    }

    public function admin_change_state(){
        // validamos que tenga acceso al modulo, si lo tiene no pasa nada pero si no lo tiene lo regresamos al inicio de todo
        $access_operation = $this->Module->is_access_operation($this->Session->read('Auth.User.id'), $this->controller_name);
        $this->set('nombre_module', $this->module_name_user);
        if(!$access_operation[0]['RoleModule']['import']){
            //pr("prueba");
            $this->Flash->error('Este Usuario no tiene permisos para cambiar el estado de la información de este módulo ['.$this->module_name_user.']');
            return $this->redirect(
                array('controller' => 'Mains', 'action' => 'index', 'admin' => true)
            );
        }
        else{
            $this->layout = 'admin';
            $this->Flash->error('Lo sentimos pero no existe la funcionalidad de Cambiar Estado configuraciones de email.');
            return $this->redirect(
                array('controller' => $this->controller_name, 'action' => 'index', 'admin' => true)
            );
        }   
    }

    public function admin_delete(){
        // validamos que tenga acceso al modulo, si lo tiene no pasa nada pero si no lo tiene lo regresamos al inicio de todo
        $access_operation = $this->Module->is_access_operation($this->Session->read('Auth.User.id'), $this->controller_name);
        $this->set('nombre_module', $this->module_name_user);
        if(!$access_operation[0]['RoleModule']['import']){
            //pr("prueba");
            $this->Flash->error('Este Usuario no tiene permisos para borrar la información de este módulo ['.$this->module_name_user.']');
            return $this->redirect(
                array('controller' => 'Mains', 'action' => 'index', 'admin' => true)
            );
        }
        else{
            $this->layout = 'admin';
            $this->Flash->error('Lo sentimos pero no existe la funcionalidad de Borrar configuraciones de email.');
            return $this->redirect(
                array('controller' => $this->controller_name, 'action' => 'index', 'admin' => true)
            );
        }   
    }

    public function admin_export(){
        // validamos que tenga acceso al modulo, si lo tiene no pasa nada pero si no lo tiene lo regresamos al inicio de todo
        $access_operation = $this->Module->is_access_operation($this->Session->read('Auth.User.id'), $this->controller_name);
        $this->set('nombre_module', $this->module_name_user);
        if(!$access_operation[0]['RoleModule']['import']){
            //pr("prueba");
            $this->Flash->error('Este Usuario no tiene permisos para exportar la información de este módulo ['.$this->module_name_user.']');
            return $this->redirect(
                array('controller' => 'Mains', 'action' => 'index', 'admin' => true)
            );
        }
        else{
            $this->layout = 'admin';
            $this->Flash->error('Lo sentimos pero no existe la funcionalidad de Exportar configuraciones de email.');
            return $this->redirect(
                array('controller' => $this->controller_name, 'action' => 'index', 'admin' => true)
            );
        }   
    }

    public function admin_import_index(){
        // validamos que tenga acceso al modulo, si lo tiene no pasa nada pero si no lo tiene lo regresamos al inicio de todo
        $access_operation = $this->Module->is_access_operation($this->Session->read('Auth.User.id'), $this->controller_name);
        $this->set('nombre_module', $this->module_name_user);
        if(!$access_operation[0]['RoleModule']['import']){
            //pr("prueba");
            $this->Flash->error('Este Usuario no tiene permisos para importar la información de este módulo ['.$this->module_name_user.']');
            return $this->redirect(
                array('controller' => 'Mains', 'action' => 'index', 'admin' => true)
            );
        }
        else{
            $this->layout = 'admin';
            $this->Flash->error('Lo sentimos pero no existe la funcionalidad de importar Módulos del Sistema.');
            return $this->redirect(
                array('controller' => $this->controller_name, 'action' => 'index', 'admin' => true)
            );
        }   
    }

    private function saveLogActivity($data, $operation){
        $log_activity = array(
            'LogActivitie' => array(
                'date' => time(),
                'object_id' => $data[$this->model_name]['id'],
                'controller' => $this->controller_name,
                'module_name_user' => $this->module_name_user,
                'user' => $this->Session->read('Auth.User.name'),
                'operation' => __($operation),
                'description' => json_encode($data),
            )
        );
        $this->LogActivitie->save($log_activity);
    }
}

?>