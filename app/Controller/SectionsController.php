<?php

class SectionsController extends AppController {

    public $name = 'Sections';
    public $layout = 'default';
    public $uses = array('User','ConfigEmail','CakeEmail', 'Network/Email','LogActivie');
    public $components = array('Paginator', 'Flash');
    // Variables utilizadas para llamar funciones o mostrar mensajes
    private $controller_name = 'Sections';
    private $model_name = 'Section';
    // Funciones
    public function beforeFilter() {
        if (empty($this->params[Configure::read('Routing.admin')]) || !$this->params[Configure::read('Routing.admin')]) {
            $this->Auth->allow($this->params['action']);
        }
        parent::beforefilter();
    }

    public function home(){
        $this->layout = 'default';
        $this->set('title_for_layout', __('Inicio'));
        $this->set('model_name', $this->model_name);
        $this->set('controller_name', $this->controller_name);
    }
}
?>