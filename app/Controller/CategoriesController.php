<?php

class CategoriesController extends AppController {

    public $name = 'Categories';
    public $layout = 'admin';
    public $uses = array('Categorie','User','Module','Role', 'RoleModule');
    public $components = array('Paginator', 'Flash', 'SendEmail', 'CustomValidationField','EscapeHtml','Import', 'CompareObject');
    // Variables utilizadas para llamar funciones o mostrar mensajes
    private $module_name = 'Categories';
    
    private $controller_name = 'Categories';
    private $model_name = 'Categorie';
    private $action_list_in_row = array('Editar' => 'edit','Ver' => 'view','Cambiar Estado' => 'change_state','Borrar' => 'delete');
    private $actions_list_icon = array('Editar' => 'Data-Edit-24.png','Ver' => 'View-24.png','Cambiar Estado' => 'Command-Refresh-24.png','Borrar' => 'Garbage-Closed-24.png');
    private $states = [];
    public function beforeFilter() {
        if (empty($this->params[Configure::read('Routing.admin')]) || !$this->params[Configure::read('Routing.admin')]) {
            $this->Auth->allow($this->params['action']);
        }

        if($this->Session->check('Auth.User.id')){
            // sacamos el nombre de usuario con el que se creo
            $module = $this->Module->find('first', array('conditions' => array('Module.name_machine' => $this->controller_name)));
            $this->module_name_user = $module['Module']['name'];
            $this->Session->delete('menu');
            if($this->User->user_getForceLogout($this->Session->read('Auth.User.id'))){
                $this->User->user_forceLogout(array($this->Session->read('Auth.User.id')));
                $this->Flash->error('Lo sentimos, pero el rol de su cuenta se encuentra desactivado, por favor comuniquese con el administrador');
                $this->requestAction(array('controller' => 'Users', 'action' => 'logout', 'admin' => true));
            }
            else{
                $categorias = $this->Categorie->get_categorieActive();
                $menu = array();
                foreach ($categorias as $categoria) {
                    $modulos = $this->User->get_allmodules($this->Session->read('Auth.User.id'), $categoria[$this->model_name]['id']);
                    $menu[][$categoria[$this->model_name]['name']] = $modulos;
                }
                $this->Session->write('menu', $menu);
            }
        }

        parent::beforefilter();
    }

    /**
    * Implementa la función de exportacion según al criterio de busqueda
    */

    /* ************************ */

    public function admin_export($busqueda = array()){
        // validamos que tenga acceso al modulo, si lo tiene no pasa nada pero si no lo tiene lo regresamos al inicio de todo
        $access_operation = $this->Module->is_access_operation($this->Session->read('Auth.User.id'), $this->controller_name);
        if(!$access_operation[0]['RoleModule']['export']){
            //pr("prueba");
            $this->Flash->error('Este Usuario no tiene permisos para exportar la infroación de ['.$this->module_name_user.']');
            return $this->redirect(
                array('controller' => 'Mains', 'action' => 'index', 'admin' => true)
            );
        }
        else{
            $this->layout = 'ajax';
            $busqueda = $this->Session->read($this->model_name);
            $this->response->download($this->module_name_user."_".date('Ymd').".csv");
            $header = array(
                array(
                    $this->model_name => array(
                        'id' => 'codigo', 
                        'name' => 'nombre', 
                        'position' => 'posicion', 
                        'state' => 'estado'
                    )
                )
            );
            $registros = $this->Categorie->find('all', $this->Categorie->get_allCategorie($busqueda));
            $registro_info = array();
            foreach ($registros as $registro) {
                $registro_info[][$this->model_name] = array(
                    'id' => $registro[$this->model_name]['id'], 
                    'name' => $registro[$this->model_name]['name'], 
                    'position' => $registro[$this->model_name]['position'], 
                    'state' => $registro[$this->model_name]['state']
                );
            }
            unset($registros);
            $registros = array_merge($header, $registro_info);
            $this->set(compact('registros'));
            $this->set('model_name', $this->model_name);
            return;
        }
    }

    public function admin_index() {
        // validamos que tenga acceso al modulo, si lo tiene no pasa nada pero si no lo tiene lo regresamos al inicio de todo
        if(!$this->Module->is_access_module($this->Session->read('Auth.User.id'), $this->controller_name)){
            //pr("prueba");
            $this->Flash->error('Este Usuario no tiene permisos para acceder a este Modulo ['.$this->module_name_user.']');
            return $this->redirect(
                array('controller' => 'Mains', 'action' => 'index', 'admin' => true)
            );
        }
        else{
            $this->layout = 'admin';
            $limit = 20;
            $busqueda = array();
            if(!empty($this->request->data)){
                $this->request->params['named'] = $this->request->data;
            }


            if(isset($this->request->params['named'][$this->model_name])){
                $busqueda = array($this->model_name => $this->request->params['named'][$this->model_name]);
            }
            if(isset($this->request->params['named']['page'])){
                $page = $this->request->params['named']['page'];
            }
            else{
                $page = 1;
            }
            $this->Paginator->settings = $this->Categorie->get_allCategorie($busqueda, $page, $limit);
            $registros = $this->Paginator->paginate($this->model_name);
            // Pasamos los registros por una limpieza de html
            $registros = $this->EscapeHtml->escapeHtml($registros);
            //Creamos la varriable de session para mantener la busqueda
            $this->Session->write($this->model_name, $busqueda);
            // Sacamos los accesos a las operaciones
            $access_operation = $this->Module->is_access_operation($this->Session->read('Auth.User.id'), $this->controller_name);
            // Cargamos los estado
            $this->setState();
            unset($this->states[2]);
            // cargamos el breadcrumb del modelo Module
            $breadcrumb = $this->Module->getBreadCrumb($this->controller_name);
            $this->set('access_operation',$access_operation[0]);
            $this->set('registros', $registros);
            $this->set('nombre_module', $this->module_name_user);
            $this->set('busqueda', $this->request->data);
            // Paso de la información a las vista
            $this->set('controller_name', $this->controller_name);
            $this->set('model_name', $this->model_name);
            $this->set('action_rows', $this->action_list_in_row);
            $this->set('action_icons', $this->actions_list_icon);
            $this->set('states', $this->states);
            $this->set('title_index', __('Listado de menus administrativos'));
            $this->set('breadCrumb', $breadcrumb);
        }
    }

    public function admin_add() {
        // validamos que tenga acceso al modulo, si lo tiene no pasa nada pero si no lo tiene lo regresamos al inicio de todo
        $access_operation = $this->Module->is_access_operation($this->Session->read('Auth.User.id'), $this->controller_name);
        $this->set('nombre_module', $this->module_name_user);
        if(!$access_operation[0]['RoleModule']['add']){
            //pr("prueba");
            $this->Flash->error('Este Usuario no tiene permisos para crear un nuevo ['.$this->module_name_user.']');
            return $this->redirect(
                array('controller' => 'Mains', 'action' => 'index', 'admin' => true)
            );
        }
        else{


            if (!empty($this->request->data)) {
                $rules_validation = $this->Categorie->getRulesValidation();
                $validation = $this->CustomValidationField->Validates($this->model_name, $this->request->data[$this->model_name], $rules_validation);
                if(empty($validation)){
                    $this->Categorie->create();
                    if ($this->Categorie->save($this->request->data)) {
                        $this->request->data[$this->model_name]['id'] = $this->User->getLastInsertId();
                        // guardamos este primer registro en las actividades ya que de aqui se iniciará todo el historial
                        $this->saveLogActivity($this->request->data, 'nuevo');
                        $this->Flash->success('El registro se ha creado exitosamente.');
                        $this->redirect(array('action' => 'index'));
                    } else {
                        $this->Flash->error('Error al intentar crear el registro. Por favor, intente nuevamente.');
                    }
                }
                else{
                    $this->set('fields_validation', $validation);
                }
            }
            // cargamos el breadcrumb del modelo Module
            $breadcrumb = $this->Module->getBreadCrumb($this->controller_name);
            //adicionamos la acción nuevo al breadcrumb (miga de pan)
            $breadcrumb[] = 'Nuevo';
            $this->layout = 'admin';
            $this->set('controller_name', $this->controller_name);
            $this->set('model_name', $this->model_name);
            $this->set('nombre_modulo', $this->module_name_user);
            $this->set('breadCrumb', $breadcrumb);
        }
    }

    public function admin_edit($id = null) {
        // validamos que tenga acceso al modulo, si lo tiene no pasa nada pero si no lo tiene lo regresamos al inicio de todo
        $access_operation = $this->Module->is_access_operation($this->Session->read('Auth.User.id'), $this->controller_name);
        $this->set('nombre_module', $this->module_name_user);
        if(!$access_operation[0]['RoleModule']['edit']){
            //pr("prueba");
            $this->Flash->error('Este Usuario no tiene permisos para editar un ['.$this->module_name_user.']');
            return $this->redirect(
                array('controller' => 'Mains', 'action' => 'index', 'admin' => true)
            );
        }
        else{


            if (!$id && empty($this->request->data))
                $this->redirect(array('action' => 'index'));
            if (!empty($this->request->data)) {
                $rules_validation = $this->Categorie->getRulesValidation();
                $validation = $this->CustomValidationField->Validates($this->model_name, $this->request->data[$this->model_name], $rules_validation);
                // ahora aqui lo que haremos es sacar si han habido modificaciones.
                $register_old = $this->Categorie->find('first', array('conditions' => array('id' => $this->request->data[$this->model_name]['id'])));
                $difference = $this->CompareObject->CompareObject($register_old[$this->model_name], $this->request->data[$this->model_name]);
                if(empty($validation)){
                    if ($this->Categorie->save($this->request->data)) {
                        // validamos que hayan diferencias si las hay entonces registramos en el log de actividades
                        if(!empty($difference)){
                            // aqui debemos guardar las diferencias en el log de actividades
                            // 1. preparamos el arreglo que me permitirá guardar esas diferencias
                            $this->saveLogActivity($this->request->data, 'editar');
                        }
                        $this->Flash->success('Actualización exitosa');
                        $this->redirect(array('action' => 'index'));
                    } else {
                        $this->Flash->error('Error en la actualización');
                    }
                }
                else{
                    $this->set('fields_validation', $validation);
                }
            }
            if (empty($this->request->data)) {
                $this->request->data = $this->Categorie->read(null, $id);
            }
            // cargamos el breadcrumb del modelo Module
            $breadcrumb = $this->Module->getBreadCrumb($this->controller_name);
            //adicionamos la acción nuevo al breadcrumb (miga de pan)
            $breadcrumb[] = 'Editar';

            $this->layout = 'admin';
            $this->set('controller_name', $this->controller_name);
            $this->set('model_name', $this->model_name);
            $this->set('nombre_modulo', $this->module_name_user);
            $this->set('breadCrumb', $breadcrumb);
        }
    }

    public function admin_view($id = null) {
        // validamos que tenga acceso al modulo, si lo tiene no pasa nada pero si no lo tiene lo regresamos al inicio de todo
        $access_operation = $this->Module->is_access_operation($this->Session->read('Auth.User.id'), $this->controller_name);
        $this->set('nombre_module', $this->module_name_user);
        if(!$access_operation[0]['RoleModule']['edit']){
            //pr("prueba");
            $this->Flash->error('Este Usuario no tiene permisos para ver un ['.$this->module_name_user.']');
            return $this->redirect(
                array('controller' => 'Mains', 'action' => 'index', 'admin' => true)
            );
        }
        else{

            $this->layout = 'admin';
            $this->set('controller_name', $this->controller_name);
            $this->set('model_name', $this->model_name);
            // Aqui saquemos la informacion del usuario que se esta modificando
            $registros = $this->Categorie->find('all', array('conditions' => array('id' => $id)));
            $registros = $this->EscapeHtml->escapeHtml($registros);
            $registro = array_shift($registros);
            // cargamos el breadcrumb del modelo Module
            $breadcrumb = $this->Module->getBreadCrumb($this->controller_name);
            //adicionamos la acción nuevo al breadcrumb (miga de pan)
            $breadcrumb[] = 'Ver';
            $this->set('registro', $registro);
            $this->set('model_name', $this->model_name);
            $this->set('controller_name', $this->controller_name);
            $this->set('breadCrumb', $breadcrumb);
        }
    }

    public function admin_delete($id = null) {
        // validamos que tenga acceso al modulo, si lo tiene no pasa nada pero si no lo tiene lo regresamos al inicio de todo
        $access_operation = $this->Module->is_access_operation($this->Session->read('Auth.User.id'), $this->controller_name);
        if(!$access_operation[0]['RoleModule']['delete']){
            //pr("prueba");
            $this->Flash->error('Este Usuario no tiene permisos para borrar un ['.$this->module_name_user.']');
            $this->redirect(
                array('controller' => 'Mains', 'action' => 'index', 'admin' => true)
            );
        }
        else{
            if (!$id) {
                $this->Flash->error('ID no válido');
                $this->redirect(array('action' => 'index'));
            }
            /*
            if ($this->User->delete($id)) {
                $this->Flash->success('El registro ha sido borrado exitosamente.');
                $this->redirect(array('action' => 'index'));
            }
            */
            //Sacamos la la información para saber que estado tiene
            else{
                $registro = $this->Categorie->find('first', array('conditions' => array('id' => $id)));
                $registro_upd = array();
                $registro_upd[$this->model_name]['id'] = $id;
                $registro_upd[$this->model_name]['state'] = 2;
                if($this->User->save($registro_upd)){
                    $registro[$this->model_name]['state'] = $registro_upd[$this->model_name]['state'];
                    $this->saveLogActivity($registro, 'editar');
                    $this->Flash->success('Actualización Exitosa');
                    $this->redirect(array('action' => 'index'));
                }
                else{
                    $this->Flash->error('No se pudo realizar la actualización');
                }
            }
        }

    }

    public function admin_change_state($id = null){
        // validamos que tenga acceso al modulo, si lo tiene no pasa nada pero si no lo tiene lo regresamos al inicio de todo
        $access_operation = $this->Module->is_access_operation($this->Session->read('Auth.User.id'), $this->controller_name);
        if(!$access_operation[0]['RoleModule']['change_state']){
            //pr("prueba");
            $this->Flash->error('Este Usuario no tiene permisos para cambiar el estado de un ['.$this->module_name_user.']');
            return $this->redirect(
                array('controller' => 'Mains', 'action' => 'index', 'admin' => true)
            );
        }
        else{ 
            $this->layout = false;
            $this->autoRender = false;
            if (!$id) {
                $this->Flash->error('ID no válido');
                $this->redirect(array('action' => 'index'));
            }
            else {
                //Sacamos la la información para saber que estado tiene
                $registro = $this->Categorie->find('first', array('conditions' => array('id' => $id)));
                $registro_upd = array();
                $registro_upd[$this->model_name]['id'] = $id;
                if($registro[$this->model_name]['state'] == 0){
                    $registro_upd[$this->model_name]['state'] = 1;
                }
                else{
                    $registro_upd[$this->model_name]['state'] = 0;
                }
                if($this->Categorie->save($registro_upd)){
                    $this->Flash->success('Actualización Exitosa');
                    $this->redirect(array('action' => 'index'));
                }
                else{
                    $this->Flash->error('No se pudo realizar la actualización');
                }

            } 
        }
    }

    public function admin_import_index(){
        // validamos que tenga acceso al modulo, si lo tiene no pasa nada pero si no lo tiene lo regresamos al inicio de todo
        $access_operation = $this->Module->is_access_operation($this->Session->read('Auth.User.id'), $this->controller_name);
        $this->set('nombre_module', $this->module_name_user);
        if(!$access_operation[0]['RoleModule']['import']){
            //pr("prueba");
            $this->Flash->error(__('Este Usuario no tiene permisos para importar la información de este módulo [').$this->module_name_user.']');
            return $this->redirect(
                array('controller' => 'Mains', 'action' => 'index', 'admin' => true)
            );
        }
        else{
            $this->layout = 'admin';
            $this->Flash->error(__('Lo sentimos pero no existe la funcionalidad de importar Menus Administrativos.'));
            return $this->redirect(
                array('controller' => $this->controller_name, 'action' => 'index', 'admin' => true)
            );
        }
    }

    public function admin_add_import(){
        // validamos que tenga acceso al modulo, si lo tiene no pasa nada pero si no lo tiene lo regresamos al inicio de todo
        $access_operation = $this->Module->is_access_operation($this->Session->read('Auth.User.id'), $this->controller_name);
        $this->set('nombre_module', $this->module_name_user);
        if(!$access_operation[0]['RoleModule']['import']){
            //pr("prueba");
            $this->Flash->error(__('Este Usuario no tiene permisos para importar la información de este módulo [').$this->module_name_user.']');
            return $this->redirect(
                array('controller' => 'Mains', 'action' => 'index', 'admin' => true)
            );
        }
        else{
            $this->layout = 'admin';
            $this->Flash->error(__('Lo sentimos pero no existe la funcionalidad de importar Menus Administrativos.'));
            return $this->redirect(
                array('controller' => $this->controller_name, 'action' => 'index', 'admin' => true)
            );
        }
    }

    public function admin_delete_import($id = null) {
        // validamos que tenga acceso al modulo, si lo tiene no pasa nada pero si no lo tiene lo regresamos al inicio de todo
        $access_operation = $this->Module->is_access_operation($this->Session->read('Auth.User.id'), $this->controller_name);
        $this->set('nombre_module', $this->module_name_user);
        if(!$access_operation[0]['RoleModule']['import']){
            //pr("prueba");
            $this->Flash->error(__('Este Usuario no tiene permisos para borrar los archivos de importación de este módulo [').$this->module_name_user.']');
            return $this->redirect(
                array('controller' => 'Mains', 'action' => 'index', 'admin' => true)
            );
        }
        else{
            $this->layout = 'admin';
            $this->Flash->error(__('Lo sentimos pero no existe la funcionalidad de borrar importaciones Menus Administrativos.'));
            return $this->redirect(
                array('controller' => $this->controller_name, 'action' => 'index', 'admin' => true)
            );
        }
    }

    public function admin_download_file($id = null, $type = 'normal'){
        // validamos que tenga acceso al modulo, si lo tiene no pasa nada pero si no lo tiene lo regresamos al inicio de todo
        $access_operation = $this->Module->is_access_operation($this->Session->read('Auth.User.id'), $this->controller_name);
        $this->set('nombre_module', $this->module_name_user);
        if(!$access_operation[0]['RoleModule']['import']){
            //pr("prueba");
            $this->Flash->error(__('Este Usuario no tiene permisos para descargar los archivos de importación de este módulo [').$this->module_name_user.']');
            return $this->redirect(
                array('controller' => 'Mains', 'action' => 'index', 'admin' => true)
            );
        }
        else{
            $this->layout = 'admin';
            $this->Flash->error(__('Lo sentimos pero no existe la funcionalidad descargar archivos de importación de Menus Administrativos.'));
            return $this->redirect(
                array('controller' => $this->controller_name, 'action' => 'index', 'admin' => true)
            );
        }
    }

    private function setState(){
        /**
         * Estados
         *  0 - Inactivo
         *  1 - Activo
         *  2 - Borrado, cuando un registro esta en este estado no se muestra
         */

        $this->states[] = __('Inactivo');
        $this->states[] = __('Activo');
        $this->states[] = __('Borrado');
    }

    private function saveLogActivity($data, $operation){
        $log_activity = array(
            'LogActivitie' => array(
                'date' => time(),
                'object_id' => $data[$this->model_name]['id'],
                'controller' => $this->controller_name,
                'module_name_user' => $this->module_name_user,
                'user' => $this->Session->read('Auth.User.name'),
                'operation' => __($operation),
                'description' => json_encode($data),
            )
        );
        $this->LogActivitie->save($log_activity);
    }
}

?>