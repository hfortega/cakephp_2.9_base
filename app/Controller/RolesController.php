<?php

class RolesController extends AppController {

    public $name = 'Roles';
    public $layout = 'admin';
    public $uses = array('User','Role','Module','RoleModule','Categorie');
    public $components = array('Paginator', 'Flash','CustomValidationField', 'EscapeHtml');
    // Variables utilizadas para llamar funciones o mostrar mensajes
    private $controller_name = 'Roles';
    private $model_name = 'Role';
    private $module_name_user = '';
    private $states = [];
    private $action_list_in_row = array('Editar' => 'edit','Ver' => 'view','Cambiar Estado' => 'change_state','Borrar' => 'delete');
    private $actions_list_icon = array('Editar' => 'Data-Edit-24.png','Ver' => 'View-24.png','Cambiar Estado' => 'Command-Refresh-24.png','Borrar' => 'Garbage-Closed-24.png');

    public function beforeFilter() {
        if (empty($this->params[Configure::read('Routing.admin')]) || !$this->params[Configure::read('Routing.admin')]) {
            $this->Auth->allow($this->params['action']);
        }

        if($this->Session->check('Auth.User.id')){
            // sacamos el nombre de usuario con el que se creo
            $module = $this->Module->find('first', array('conditions' => array('Module.name_machine' => $this->controller_name)));
            $this->module_name_user = $module['Module']['name'];
            $this->Session->delete('menu');
            if($this->User->user_getForceLogout($this->Session->read('Auth.User.id'))){
                $this->User->user_forceLogout(array($this->Session->read('Auth.User.id')));
                $this->Flash->error('Lo sentimos, pero el rol de su cuenta se encuentra desactivado, por favor comuniquese con el administrador');
                $this->requestAction(array('controller' => 'Users', 'action' => 'logout', 'admin' => true));
            }
            else{
                $categorias = $this->Categorie->get_categorieActive();
                $menu = array();
                foreach ($categorias as $categoria) {
                    $modulos = $this->User->get_allmodules($this->Session->read('Auth.User.id'), $categoria['Categorie']['id']);
                    $menu[][$categoria['Categorie']['name']] = $modulos;
                }
                $this->Session->write('menu', $menu);
            }
        }

        parent::beforefilter();
    }

    /**
    * Implementa la función de importación de los módulos con su respectiva vinculación a categorias
    */

    public function admin_export($busqueda = array()){
        // validamos que tenga acceso al modulo, si lo tiene no pasa nada pero si no lo tiene lo regresamos al inicio de todo
        $access_operation = $this->Module->is_access_operation($this->Session->read('Auth.User.id'), $this->controller_name);
        $this->set('nombre_module', $this->module_name_user);
        if(!$access_operation[0]['RoleModule']['export']){
            //pr("prueba");
            $this->Flash->error('Este Usuario no tiene permisos para crear un nuevo ['.$this->module_name_user.']');
            return $this->redirect(
                array('controller' => 'Mains', 'action' => 'index', 'admin' => true)
            );
        }
        else{
            $this->layout = 'ajax';
            $busqueda = $this->Session->read($this->model_name);
            $this->response->download($this->module_name_user."_".date('Ymd').".csv");
            $header = array(array($this->model_name => array(
                'role_id' => 'codigo_rol', 
                'role_name' => 'nombre_rol', 
                'module_id' => 'codigo_module', 
                'module_name' => 'nombre_module', 
                'role_module_active' => 'Activo', 
                'role_module_add' => 'Agregar', 
                'role_module_edit' => 'Editar', 
                'role_module_view' => 'Ver', 
                'role_module_delete' => 'Borrar',
                'role_module_view_revision' => 'Ver_Revisiones',
                'role_module_change_state' => 'Cambiar_Estado',
                'role_module_export' => 'Exportar', 
                'role_module_import' => 'Importar' , 
                'role_state' => 'estado_rol'
                )));
            $registros = $this->Role->find('all', $this->Role->get_allRole_export($busqueda));
            $registro_info = array();
            foreach ($registros as $registro) {
                $registro_info[][$this->model_name] = array(
                    'role_id' => $registro[$this->model_name]['id'], 
                    'role_name' => $registro[$this->model_name]['name'], 
                    'module_id' => $registro['Module']['id'], 
                    'module_name' => $registro['Module']['name'], 
                    'role_module_active' => $registro['RoleModule']['active'], 
                    'role_module_add' => $registro['RoleModule']['add'], 
                    'role_module_edit' => $registro['RoleModule']['edit'], 
                    'role_module_view' => $registro['RoleModule']['view'], 
                    'role_module_delete' => $registro['RoleModule']['delete'], 
                    'role_module_view_revision' => $registro['RoleModule']['view_revision'],
                    'role_module_change_state' => $registro['RoleModule']['change_state'],
                    'role_module_export' => $registro['RoleModule']['export'], 
                    'role_module_import' => $registro['RoleModule']['import'] , 
                    'role_state' => $registro[$this->model_name]['state']);
            }
            unset($registros);
            $registros = array_merge($header, $registro_info);
            $this->set('model_name', $this->model_name);
            $this->set(compact('registros'));
            return;
            
        }
    }

    public function admin_index() {
        // validamos que tenga acceso al modulo, si lo tiene no pasa nada pero si no lo tiene lo regresamos al inicio de todo
        if(!$this->Module->is_access_module($this->Session->read('Auth.User.id'), $this->controller_name)){
            //pr("prueba");
            $this->Flash->error('Este Usuario no tiene permisos para acceder a este Modulo ['.$this->module_name_user.']');
            $this->redirect(
                array('controller' => 'Mains', 'action' => 'index', 'admin' => true)
            );
        }
        else{
            
            $limit = 20;
            $busqueda = array();
            if(!empty($this->request->data)){
                $this->request->params['named'] = $this->request->data;
            }


            if(isset($this->request->params['named'][$this->model_name])){
                $busqueda = array($this->model_name => $this->request->params['named'][$this->model_name]);
            }
            if(isset($this->request->params['named']['page'])){
                $page = $this->request->params['named']['page'];
            }
            else{
                $page = 1;
            }
            $this->Paginator->settings = $this->Role->get_allRole($busqueda,$page, $limit);
            $registros = $this->Paginator->paginate($this->model_name);
            // Pasamos por el modulo de limpieza de javascript y html
            $registros = $this->EscapeHtml->escapeHtml($registros);
            //Creamos la varriable de session para mantener la busqueda
            $this->Session->write($this->model_name, $busqueda);
            // Sacamos los accesos a las operaciones
            $access_operation = $this->Module->is_access_operation($this->Session->read('Auth.User.id'), $this->controller_name);
           // Cargamos los estado
            $this->setState();
            unset($this->states[2]);
            // cargamos el breadcrumb del modelo Module
            $breadcrumb = $this->Module->getBreadCrumb($this->controller_name);
            $this->layout = 'admin';
            $this->set('access_operation',$access_operation[0]);
            $this->set('registros', $registros);
            $this->set('nombre_module', $this->module_name_user);
            $this->set('busqueda', $this->request->data);
            // Paso de la información a las vista
            $this->set('controller_name', $this->controller_name);
            $this->set('model_name', $this->model_name);
            $this->set('action_rows', $this->action_list_in_row);
            $this->set('action_icons', $this->actions_list_icon);
            $this->set('title_index', __('Listado de roles'));
            $this->set('breadCrumb', $breadcrumb);
            $this->set('states', $this->states);
        }
    }

    public function admin_add() {
        // validamos que tenga acceso al modulo, si lo tiene no pasa nada pero si no lo tiene lo regresamos al inicio de todo
        $access_operation = $this->Module->is_access_operation($this->Session->read('Auth.User.id'), $this->controller_name);
        $this->set('nombre_module', $this->module_name_user);
        if(!$access_operation[0]['RoleModule']['add']){
            //pr("prueba");
            $this->Flash->error('Este Usuario no tiene permisos para crear un nuevo ['.$this->module_name_user.']', 'flash_custom');
            $this->redirect(
                array('controller' => 'Mains', 'action' => 'index', 'admin' => true)
            );
        }
        else{
            if (!empty($this->request->data)) {
                $rules_validation = $this->Role->getRulesValidation();
                $validation = $this->CustomValidationField->Validates($this->model_name, $this->request->data[$this->model_name], $rules_validation);
                if(empty($validation)) {
                    $this->Role->create();
                    // lo primero que haremos será insertar el rol
                    $role = array();
                    $role[$this->model_name] = array('name' => $this->request->data[$this->model_name]['name']);
                    $error = false;
                    if ($this->Role->save($role)) {
                        // una vez salvado el rol extraemo el id de este rol
                        $id_rol = $this->Role->getLastInsertID();
                        // ahora insertamos la relación de modulo con rol
                        $count = $this->request->data[$this->model_name]['count'];
                        for ($i=1; $i < $count; $i++) { 
                            $rol_module = array();
                            $rol_module['RoleModule'] = array(
                                'module_id' => $this->request->data[$this->model_name]['module_id_'.$i],
                                'role_id' => $id_rol,
                                'add' => $this->request->data[$this->model_name]['add_'.$i],
                                'edit' => $this->request->data[$this->model_name]['edit_'.$i],
                                'view' => $this->request->data[$this->model_name]['view_'.$i],
                                'delete' => $this->request->data[$this->model_name]['delete_'.$i],
                                'view_revision' => $this->request->data[$this->model_name]['view_revision_'.$i],
                                'active' => $this->request->data[$this->model_name]['active_'.$i],
                                'change_state' => $this->request->data[$this->model_name]['change_state_'.$i],
                                'export' => $this->request->data[$this->model_name]['export_'.$i],
                                'import' => $this->request->data[$this->model_name]['import_'.$i],
                            );
                            // validamos que no haya problemas con la insercion de la relación, si hay problemas rompemos el ciclo,generamos el error y eliminamos todo lo que hayamos insertado
                            $this->RoleModule->create();    
                            if (!$this->RoleModule->save($rol_module)){
                                $this->Role->query("DELETE FROM role_modules WHERE role_id = ".$id_rol);
                                $this->Role->delete($id_rol);
                                $error = true;
                                break;
                            }
                            unset($rol_module);
                        }
                        // si la variable error = true generamos el error
                        if($error){
                            $this->Flash->error('Error al intentar crear el registro. Por favor, intente nuevamente.');
                        }
                        else{
                            // si la variable error = false quiere decir que todo salio perfectamente
                            $this->Flash->success('El registro se ha creado exitosamente.');
                            $this->redirect(array('action' => 'index'));
                                    
                        }
                    } else {
                        $this->Flash->error('Error al intentar crear el registro. Por favor, intente nuevamente.');
                    }
                }
                else{
                    $this->set('fields_validation', $validation);
                }
            }

            $this->layout = 'admin';
            // sacamos todos los modulos
            $modules = $this->Module->get_allModuleCategorie();
            // cargamos el breadcrumb del modelo Module
            $breadcrumb = $this->Module->getBreadCrumb($this->controller_name);
            //adicionamos la acción nuevo al breadcrumb (miga de pan)
            $breadcrumb[] = 'Nuevo';
            $this->set('modulos', $modules);
            // Paso de la información a las vista
            $this->set('controller_name', $this->controller_name);
            $this->set('model_name', $this->model_name);
            $this->set('nombre_modulo', $this->module_name_user);
            $this->set('breadCrumb', $breadcrumb);
        }
    }

    public function admin_edit($id = null) {
        // validamos que tenga acceso al modulo, si lo tiene no pasa nada pero si no lo tiene lo regresamos al inicio de todo
        $access_operation = $this->Module->is_access_operation($this->Session->read('Auth.User.id'), $this->controller_name);
        $this->set('nombre_module', $this->module_name_user);
        if(!$access_operation[0]['RoleModule']['edit']){
            //pr("prueba");
            $this->Flash->error('Este Usuario no tiene permisos para editar un ['.$this->module_name_user.']');
            return $this->redirect(
                array('controller' => 'Mains', 'action' => 'index', 'admin' => true)
            );
        }
        else{
            if (!$id && empty($this->request->data))
                $this->redirect(array('action' => 'index'));
            if (!empty($this->request->data)) {
                $rules_validation = $this->Role->getRulesValidation();
                $validation = $this->CustomValidationField->Validates($this->model_name, $this->request->data[$this->model_name], $rules_validation);
                if(empty($validation)) {
                    // lo primero que haremos será insertar el rol
                    $role = array();
                    $role[$this->model_name] = array(
                        'id' => $this->request->data[$this->model_name]['id'],
                        'name' => $this->request->data[$this->model_name]['name'],
                    );
                    $error = false;
                    if ($this->Role->save($role)) {
                        // ahora insertamos la relación de modulo con rol
                        $count = $this->request->data[$this->model_name]['count'];
                        for ($i=1; $i < $count; $i++) { 
                            $rol_module = array();
                            $rol_module['RoleModule'] = array(
                                'id' => $this->request->data[$this->model_name]['id_'.$i],
                                'add' => $this->request->data[$this->model_name]['add_'.$i],
                                'edit' => $this->request->data[$this->model_name]['edit_'.$i],
                                'view' => $this->request->data[$this->model_name]['view_'.$i],
                                'delete' => $this->request->data[$this->model_name]['delete_'.$i],
                                'view_revision' => $this->request->data[$this->model_name]['view_revision_'.$i],
                                'active' => $this->request->data[$this->model_name]['active_'.$i],
                                'change_state' => $this->request->data[$this->model_name]['change_state_'.$i],
                                'export' => $this->request->data[$this->model_name]['export_'.$i],
                                'import' => $this->request->data[$this->model_name]['import_'.$i],
                            );
                            // validamos que no haya problemas con la insercion de la relación, si hay problemas rompemos el ciclo,generamos el error y eliminamos todo lo que hayamos insertado
                            if (!$this->RoleModule->save($rol_module)){
                                $error = true;
                                break;
                            }
                            unset($rol_module);
                        }
                        // si la variable error = true generamos el error
                        if($error){
                            $this->Flash->error('Error al intentar actualizar el registro. Por favor, intente nuevamente.');
                        }
                        else{
                            // si la variable error = false quiere decir que todo salio perfectamente
                            $this->Flash->success('El registro se ha actualizado exitosamente.');
                            $this->redirect(array('action' => 'index'));
                                    
                        }
                    }
                }
                else{
                    $this->set('fields_validation', $validation);
                }

            }
            if (empty($this->request->data)) {
                $this->request->data = $this->Role->read(null, $id);
            }


            $this->layout = 'admin';
            // Sacamos todos los modulos asociados a este rol
            $role_modules = $this->Module->get_allModuleCategorieOperation($id);
            // cargamos el breadcrumb del modelo Module
            $breadcrumb = $this->Module->getBreadCrumb($this->controller_name);
            //adicionamos la acción nuevo al breadcrumb (miga de pan)
            $breadcrumb[] = 'Editar';
            $this->set('modules', $role_modules);
            // Paso de la información a las vista
            $this->set('controller_name', $this->controller_name);
            $this->set('model_name', $this->model_name);
            $this->set('nombre_modulo', $this->module_name_user);
            $this->set('breadCrumb', $breadcrumb);
        }
    }

    public function admin_delete($id = null) {
        // validamos que tenga acceso al modulo, si lo tiene no pasa nada pero si no lo tiene lo regresamos al inicio de todo
        $access_operation = $this->Module->is_access_operation($this->Session->read('Auth.User.id'), $this->controller_name);
        if(!$access_operation[0]['RoleModule']['delete']){
            //pr("prueba");
            $this->Flash->error('Este Usuario no tiene permisos para borrar un ['.$this->module_name_user.']');
            $this->redirect(
                array('controller' => 'Mains', 'action' => 'index', 'admin' => true)
            );
        }
        else{
            if (!$id) {
                $this->Flash->error('ID no válido');
                $this->redirect(array('action' => 'index'));
            }
            /*
            if ($this->User->delete($id)) {
                $this->Flash->success('El registro ha sido borrado exitosamente.');
                $this->redirect(array('action' => 'index'));
            }
            */
            //Sacamos la la información para saber que estado tiene
            else{
                $registro = $this->User->find('first', array('conditions' => array('id' => $id)));
                $registro_upd = array();
                $registro_upd[$this->model_name]['id'] = $id;
                $registro_upd[$this->model_name]['state'] = 2;
                if($this->User->save($registro_upd)){
                    $registro[$this->model_name]['state'] = $registro_upd[$this->model_name]['state'];
                    $this->saveLogActivity($registro, 'editar');
                    $this->Flash->success('Actualización Exitosa');
                    $this->redirect(array('action' => 'index'));
                }
                else{
                    $this->Flash->error('No se pudo realizar la actualización');
                }
            }
        }
        
    }


    public function admin_change_state($id = null){
        // validamos que tenga acceso al modulo, si lo tiene no pasa nada pero si no lo tiene lo regresamos al inicio de todo
        $access_operation = $this->Module->is_access_operation($this->Session->read('Auth.User.id'), $this->controller_name);
        if(!$access_operation[0]['RoleModule']['change_state']){
            //pr("prueba");
            $this->Flash->error(__('Este Usuario no tiene permisos para cambiar el estado de un [').$this->module_name_user.']');
            return $this->redirect(
                array('controller' => 'Mains', 'action' => 'index', 'admin' => true)
            );
        }
        else{ 
            $this->layout = false;
            $this->autoRender = false;
            if (!$id) {
                $this->Flash->error(__('ID no válido'));
                $this->redirect(array('action' => 'index'));
            }
            else {
                //Sacamos la la información para saber que estado tiene
                $registro = $this->Role->find('first', array('conditions' => array('id' => $id)));
                $registro_upd = array();
                $registro_upd[$this->model_name]['id'] = $id;
                if($registro[$this->model_name]['state'] == 0){
                    $registro_upd[$this->model_name]['state'] = 1;
                }
                else{
                    $registro_upd[$this->model_name]['state'] = 0;
                }
                if($this->Role->save($registro_upd)){
                    $this->Flash->success(__('Actualización Exitosa'));
                    $this->redirect(array('action' => 'index'));
                }
                else{
                    $this->Flash->error(__('No se pudo realizar la actualización'));
                }

            } 
        }
    }

    public function admin_import_index(){
        // validamos que tenga acceso al modulo, si lo tiene no pasa nada pero si no lo tiene lo regresamos al inicio de todo
        $access_operation = $this->Module->is_access_operation($this->Session->read('Auth.User.id'), $this->controller_name);
        $this->set('nombre_module', $this->module_name_user);
        if(!$access_operation[0]['RoleModule']['import']){
            //pr("prueba");
            $this->Flash->error(__('Este Usuario no tiene permisos para importar la información de este módulo [').$this->module_name_user.']');
            return $this->redirect(
                array('controller' => 'Mains', 'action' => 'index', 'admin' => true)
            );
        }
        else{
            $this->layout = 'admin';
            $this->Flash->error(__('Lo sentimos pero no existe la funcionalidad de importar Roles.'));
            return $this->redirect(
                array('controller' => $this->controller_name, 'action' => 'index', 'admin' => true)
            );
        }   
    }

    public function admin_add_import(){
        // validamos que tenga acceso al modulo, si lo tiene no pasa nada pero si no lo tiene lo regresamos al inicio de todo
        $access_operation = $this->Module->is_access_operation($this->Session->read('Auth.User.id'), $this->controller_name);
        $this->set('nombre_module', $this->module_name_user);
        if(!$access_operation[0]['RoleModule']['import']){
            //pr("prueba");
            $this->Flash->error(__('Este Usuario no tiene permisos para importar la información de este módulo [').$this->module_name_user.']');
            return $this->redirect(
                array('controller' => 'Mains', 'action' => 'index', 'admin' => true)
            );
        }
        else{
            $this->layout = 'admin';
            $this->Flash->error(__('Lo sentimos pero no existe la funcionalidad de importar Roles.'));
            return $this->redirect(
                array('controller' => $this->controller_name, 'action' => 'index', 'admin' => true)
            );
        }
    }

    public function admin_delete_import($id = null) {
        // validamos que tenga acceso al modulo, si lo tiene no pasa nada pero si no lo tiene lo regresamos al inicio de todo
        $access_operation = $this->Module->is_access_operation($this->Session->read('Auth.User.id'), $this->controller_name);
        $this->set('nombre_module', $this->module_name_user);
        if(!$access_operation[0]['RoleModule']['import']){
            //pr("prueba");
            $this->Flash->error(__('Este Usuario no tiene permisos para borrar los archivos de importación de este módulo [').$this->module_name_user.']');
            return $this->redirect(
                array('controller' => 'Mains', 'action' => 'index', 'admin' => true)
            );
        }
        else{
            $this->layout = 'admin';
            $this->Flash->error(__('Lo sentimos pero no existe la funcionalidad de borar importaciones Roles.'));
            return $this->redirect(
                array('controller' => $this->controller_name, 'action' => 'index', 'admin' => true)
            );
        }
    }

    public function admin_download_file($id = null, $type = 'normal'){
        // validamos que tenga acceso al modulo, si lo tiene no pasa nada pero si no lo tiene lo regresamos al inicio de todo
        $access_operation = $this->Module->is_access_operation($this->Session->read('Auth.User.id'), $this->controller_name);
        $this->set('nombre_module', $this->module_name_user);
        if(!$access_operation[0]['RoleModule']['import']){
            //pr("prueba");
            $this->Flash->error(__('Este Usuario no tiene permisos para descargar los archivos de importación de este módulo [').$this->module_name_user.']');
            return $this->redirect(
                array('controller' => 'Mains', 'action' => 'index', 'admin' => true)
            );
        }
        else{
            $this->layout = 'admin';
            $this->Flash->error(__('Lo sentimos pero no existe la funcionalidad descargar archivos de importación de Roles.'));
            return $this->redirect(
                array('controller' => $this->controller_name, 'action' => 'index', 'admin' => true)
            );
        }
    }

    private function setState(){
        /**
         * Estados
         *  0 - Inactivo
         *  1 - Activo
         *  2 - Borrado, cuando un registro esta en este estado no se muestra 
         */

        $this->states[] = __('Inactivo');
        $this->states[] = __('Activo');
        $this->states[] = __('Borrado');
    }

    private function saveLogActivity($data, $operation){
        $log_activity = array(
            'LogActivitie' => array(
                'date' => time(),
                'object_id' => $data[$this->model_name]['id'],
                'controller' => $this->controller_name,
                'module_name_user' => $this->module_name_user,
                'user' => $this->Session->read('Auth.User.name'),
                'operation' => __($operation),
                'description' => json_encode($data),
            )
        );
        $this->LogActivitie->save($log_activity);
    }

}

?>