/**
 * Funcion para inicializar el select
 */

function initSelect(div_ids){
    for (var i = 0; i < div_ids.length; i++) {
        $(div_ids[i]).select2({
            'placeholder' : 'Seleccione Opción',
            'allowClear' : true
        });
    };


}

/**
 * function para destruir el select
 */

function destroySelect(id_div){
    $('#'+id_div).select2('destroy');
}

/**
 * Funcion para realizar un reset a los select's
 */

function resetSelect(div_ids){
    for (var i = 0; i < div_ids.length; i++) {
        $("#"+div_ids[i]).empty().trigger('change')
    };
}