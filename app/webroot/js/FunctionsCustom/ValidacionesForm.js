/**
* Función para recuperar los accesos del login en el formulario de los usuarios finales
*/

function recovery_access(usuario, img, url){
	if(usuario == ''){
        $("#msj_popup").html('<div class="col-md-12">Para recuperar el acceso por favor ingrese el Email, este será enviado por correo electrónico</div>');
    }
    else{
        $("#msj_popup").html('<p style="text-align:center">'+img+'</p>');
        $("#cancel_button").css('display', 'none');
        $.ajax({
            type: 'POST',
            url: url,
            async: true,
            success: function(response){
                $("#msj_popup").html(response);
                $("#cancel_button").css('display', '');       
            }
        })
    }
}

/*
	Función : Validar formulario
	Datos Entrada :
		- Id del botón guardar
		- id del formulario
		- id del div para mostrar el error
	Datos Salida :
		- Texto del error en el div con id que entra a la función

	Descripción:
		Esta función evalua todos los campos que tengan un atributo rel = 'Requerido' y verifica que estos tengan un valor
		en caso contrario mostrará un texto introducido en el atributo data colocado en el div con el id pasado por parametro
*/
function validar_formulario(id_form, id_div){
	$("#helpBlock").remove();
	var check = true;
	$("#"+id_form).find(":input[rel='Requerido']").each(function(){
		var value = $("#"+this.id).val();
		var padre = $("#"+this.id).parent();
		if(padre.hasClass('has-error')){
			padre.removeClass('has-error');
		}
		/* Validamos que el valor no sea vacio */
		if(value == ""){
			var data = $("#"+this.id).attr('data-error');
			$("#"+this.id).attr('placeholder', data);
			$("#"+this.id).focus();
			padre.addClass('has-error');
			return check = false;
		}
		if($("#"+this.id).is('[data-type]')){
			var type = $("#"+this.id).attr('data-type');
			if(type == "Numerico"){
				var bool = validar_numeros(value);
				if(!bool){
					var data = $("#"+this.id).attr('data-type-error');
					$("#"+this.id).focus();
					padre.append('<span id="helpBlock" class="help-block">'+data+'</span>');
					padre.addClass('has-error');
					return check = false;		
				}	
			}

			if(type == "NumericoPositivo"){
				var bool = true
				if(value < 0){
					bool = false;
				}
				if(!bool){
					var data = $("#"+this.id).attr('data-type-error');
					$("#"+this.id).focus();
					padre.addClass('has-error');
					padre.append('<span id="helpBlock" class="help-block">'+data+'</span>');
					return check = false;		
				}	
			}

			if(type == "NroQuestions"){
				var cantidad_preguntas = parseInt($("#counter").val())-1;
				var bool = validar_nro_questions(parseInt(value), cantidad_preguntas);
				if(bool){
					var data = $("#"+this.id).attr('data-type-error');
					$("#"+this.id).focus();
					padre.addClass('has-error');
					padre.append('<span id="helpBlock" class="help-block">'+data+'</span>');
					return check = false;		
				}	
			}

			if(type == "PuntosUsuario"){
				var puntos_totales = parseInt($("#PuntosTotales").val());
				var bool = validar_puntos_usuario(parseInt(value), puntos_totales);
				if(bool){
					var data = $("#"+this.id).attr('data-type-error');
					$("#"+this.id).focus();
					padre.addClass('has-error');
					padre.append('<span id="helpBlock" class="help-block">'+data+'</span>');
					return check = false;		
				}	
			}

			if(type == "Email"){
				var bool = validar_email(value);
				if(!bool){
					var data = $("#"+this.id).attr('data-type-error');
					$("#"+this.id).focus();
					padre.addClass('has-error');
					padre.append('<span id="helpBlock" class="help-block">'+data+'</span>');
					return check = false;		
				}	
			}
			if(type == "EmailM"){
				// realizamos un split de la cadena que ingresa
				var emails = value.split(',');
				for(var i=0; i < emails.length; i++){
					var bool = validar_email(emails[i]);
					if(!bool){
						var data = $("#"+this.id).attr('data-type-error');
						$("#"+this.id).focus();
						padre.addClass('has-error');
						padre.append('<span id="helpBlock" class="help-block">'+data+'</span>');
						return check = false;	
					}	
				}
					
			}
			if(type == "SinEspacios"){
				var bool = validar_espacios(value);
				if(!bool){
					var data = $("#"+this.id).attr('data-type-error');
					$("#"+this.id).focus();
					padre.append('<span id="helpBlock" class="help-block">'+data+'</span>');
					padre.addClass('has-error');
					return check = false;		
				}	
			}
			if(type == "DosNroTelefonos"){
				var bool = validar_dos_nro_telefonos(value);
				if(!bool){
					var data = $("#"+this.id).attr('data-type-error');
					$("#"+this.id).focus();
					padre.addClass('has-error');
					padre.append('<span id="helpBlock" class="help-block">'+data+'</span>');
					return check = false;		
				}	
			}
			if(type == "DosNroTelefonosC"){
				var bool = validar_dos_nro_telefonosC(value);
				if(!bool){
					var data = $("#"+this.id).attr('data-type-error');
					$("#"+this.id).focus();
					padre.addClass('has-error');
					padre.append('<span id="helpBlock" class="help-block">'+data+'</span>');
					return check = false;		
				}	
			}
			if(type == "Archivo_CSV"){
				var bool = validar_archvio_csv(value);
				if(!bool){
					var data = $("#"+this.id).attr('data-type-error');
					$("#"+this.id).focus();
					padre.addClass('has-error');
					padre.append('<span id="helpBlock" class="help-block">'+data+'</span>');
					return check = false;		
				}	
			}

			 
		}
	})
	return check;
}

/*
	Función : validar_nro_questions
	Datos Entrada :
		- Valor (tipo: Numérico)
		- cantidad_preguntas (tipo: Numérico)
	Datos Salida
		- Booleano
	Descripción:
		Esta función me permite validar valor > a la cantidad de preguntas	
*/
function validar_nro_questions(value, cantidad_preguntas){
	if(value > cantidad_preguntas){
		return true;
	}
	else{
		return false;
	}
}

/*
	Función : validar_puntos_usuario
	Datos Entrada :
		- Valor (tipo: Numérico)
		- puntos_totales (tipo: Numérico)
	Datos Salida
		- Booleano
	Descripción:
		Esta función me permite validar valor > a la puntos totales	
*/
function validar_puntos_usuario(value, puntos_totales){
	if(value > puntos_totales){
		return true;
	}
	else{
		return false;
	}
}
/*
	Función : validar_email
	Datos Entrada :
		- Valor (tipo: Cadena)
	Datos Salida
		- Booleano
	Descripción:
		Esta función me permite validar si la cadena que ingresa tiene el formato de un correo electronico
*/
function validar_email(valor)
{
    // creamos nuestra regla con expresiones regulares.
    var filter = /[\w-\.]{3,}@([\w-]{2,}\.)*([\w-]{2,}\.)[\w-]{2,4}$/;
    // utilizamos test para comprobar si el parametro valor cumple la regla
    if(filter.test(valor))
        return true;
    else
        return false;
};

/*
	Función : validar_numeros
	Datos Entrada :
		- Valor (tipo: Cadena)
	Datos Salida
		- Booleano
	Descripción:
		Esta función me permite validar si la cadena que ingresa tiene el formato de un número sin decimales
*/
function validar_numeros(valor)
{
    var patron = /[0-9]+/;
    if(patron.test(valor))
        return true;
    else
        return false;
}

/*
	Función : validar_espacios
	Datos Entrada :
		- Valor (tipo: Cadena)
	Datos Salida
		- Booleano
	Descripción:
		Esta función me permite validar si la cadena que ingresa tiene espacios
*/
function validar_espacios(valor)
{
    var patron = /(.+\s+)+/;
    if(patron.test(valor))
        return false;
    else
        return true;
}

/*
	Función : validar_telefonos
	Datos Entrada :
		- Valor (tipo: Cadena)
	Datos Salida
		- Booleano
	Descripción:
		Esta función me permite validar que solamente se ingresen dos números de telefono seguidos por un - y que pueden o no tener el codigo de area
*/
function validar_dos_nro_telefonos(valor)
{
    var patron = /^(\(\+?(\d{2,})\))?\d{7,}(-\d{7,})?$/;
    if(patron.test(valor))
        return true;
    else
        return false;
}
/*
	Función : validar_telefonos_celulares
	Datos Entrada :
		- Valor (tipo: Cadena)
	Datos Salida
		- Booleano
	Descripción:
		Esta función me permite validar que solamente se ingresen dos números de telefono seguidos por un - y que pueden o no tener el codigo de area
*/
function validar_dos_nro_telefonosC(valor)
{
    var patron = /^(\(\+?(\d{2,})\))?\d{10,}(-\d{10,})?$/;
    if(patron.test(valor))
        return true;
    else
        return false;
}

/*
	Función : validar_archivo_csv
	Datos Entrada :
		- Valor (tipo: Cadena)
	Datos Salida
		- Booleano
	Descripción:
		Esta función me permite validar que la extension del archivo sea solo csv
*/
function validar_archvio_csv(valor)
{
   	var fileExtension = ['csv'];
    if ($.inArray(valor.split('.').pop().toLowerCase(), fileExtension) == -1) {
        return false;
    }
    else{
    	return true;
    }
}