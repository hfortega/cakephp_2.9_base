CREATE DATABASE  IF NOT EXISTS `base_cake` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `base_cake`;
-- MySQL dump 10.13  Distrib 5.6.13, for Win32 (x86)
--
-- Host: 127.0.0.1    Database: base_cake
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.21-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `state` int(11) DEFAULT '1',
  `position` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` (`id`, `name`, `state`, `position`) VALUES (1,'Configuraciones',1,2),(2,'Pedidos',1,1),(3,'Logs',1,3);
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `config_emails`
--

DROP TABLE IF EXISTS `config_emails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `config_emails` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `host` varchar(255) DEFAULT NULL,
  `port` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `transport` varchar(45) DEFAULT NULL,
  `tls` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `config_emails`
--

LOCK TABLES `config_emails` WRITE;
/*!40000 ALTER TABLE `config_emails` DISABLE KEYS */;
INSERT INTO `config_emails` (`id`, `host`, `port`, `username`, `password`, `transport`, `tls`) VALUES (1,'ssl://smtp.gmail.com','465','hfortega99@gmail.com','RnI0bmMzc2MwOTk=','Smtp','1'),(2,'ssl://smtp.gmail.com','465','hfortega99@gmail.com','RnI0bmMzc2MwOTk=','Smtp','1');
/*!40000 ALTER TABLE `config_emails` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `file_imports`
--

DROP TABLE IF EXISTS `file_imports`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `file_imports` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` datetime DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `model` varchar(255) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `type` varchar(50) DEFAULT NULL,
  `file_error` varchar(255) DEFAULT NULL,
  `failed` int(11) DEFAULT '0',
  `error` int(11) DEFAULT '0',
  `correct` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `file_imports`
--

LOCK TABLES `file_imports` WRITE;
/*!40000 ALTER TABLE `file_imports` DISABLE KEYS */;
INSERT INTO `file_imports` (`id`, `date`, `name`, `model`, `user_id`, `type`, `file_error`, `failed`, `error`, `correct`) VALUES (37,'2017-05-26 21:38:07','test_import_central_cake_26052017_213807.csv','Deliverie',1,'Importar Registros','Errores_Importacion_Pedidos_26052017_213807.csv',0,2,0);
/*!40000 ALTER TABLE `file_imports` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `log_activities`
--

DROP TABLE IF EXISTS `log_activities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log_activities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` int(11) DEFAULT NULL,
  `description` longtext,
  `user` varchar(255) DEFAULT NULL,
  `operation` text,
  `object_id` int(11) DEFAULT NULL,
  `controller` varchar(255) DEFAULT NULL,
  `module_name_user` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=225 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `log_activities`
--

LOCK TABLES `log_activities` WRITE;
/*!40000 ALTER TABLE `log_activities` DISABLE KEYS */;
INSERT INTO `log_activities` (`id`, `date`, `description`, `user`, `operation`, `object_id`, `controller`, `module_name_user`) VALUES (219,1504294972,'{\"User\":{\"id\":\"2\",\"nit\":\"1038105254\",\"name\":\"Alina Navarro Gil\",\"phone\":\"\",\"cellphone\":\"3008543519\",\"email\":\"prueba@prueba.com\",\"role_id\":\"1\",\"username\":\"anavarro\"}}','Harold Ortega','editar',2,'Users','Usuarios'),(220,1504296120,'{\"User\":{\"id\":\"2\",\"nit\":\"1038105254\",\"name\":\"Alina Navarro Gil\",\"phone\":\"\",\"cellphone\":\"3008543519\",\"email\":\"prueba@prueba.com\",\"role_id\":\"1\",\"username\":\"anavarro\",\"password\":\"67e44dbf2af62688a33c4038c9e8e793cb350551\"}}','Harold Ortega','editar',2,'Users','Usuarios'),(221,1504297453,'{\"User\":{\"nit\":\"1140108657\",\"name\":\"Oscar Ocampo\",\"phone\":\"\",\"cellphone\":\"3005675691\",\"email\":\"info@prueba.com\",\"role_id\":\"1\",\"username\":\"aocampo\",\"password\":\"2e3a242bbdfd71eed25731538113b56075b6a3c1\"}}','Harold Ortega','nuevo',3,'Users','Usuarios'),(222,1504302256,'{\"User\":{\"id\":\"3\",\"username\":\"aocampo\",\"password\":\"2e3a242bbdfd71eed25731538113b56075b6a3c1\",\"name\":\"Oscar Ocampo\",\"email\":\"info@prueba.com\",\"state\":3,\"nit\":\"1140108657\",\"phone\":\"\",\"cellphone\":\"3005675691\",\"forceLogout\":\"0\",\"role_id\":\"1\"}}','Harold Ortega','editar',3,'Users','Usuarios'),(223,1504368991,'{\"User\":{\"id\":\"2\",\"username\":\"anavarro\",\"password\":\"67e44dbf2af62688a33c4038c9e8e793cb350551\",\"name\":\"Alina Navarro Gil\",\"email\":\"prueba@prueba.com\",\"state\":0,\"nit\":\"1038105254\",\"phone\":\"\",\"cellphone\":\"3008543519\",\"forceLogout\":\"0\",\"role_id\":\"1\"}}','Harold Ortega','editar',2,'Users','Usuarios'),(224,1504368995,'{\"User\":{\"id\":\"2\",\"username\":\"anavarro\",\"password\":\"67e44dbf2af62688a33c4038c9e8e793cb350551\",\"name\":\"Alina Navarro Gil\",\"email\":\"prueba@prueba.com\",\"state\":1,\"nit\":\"1038105254\",\"phone\":\"\",\"cellphone\":\"3008543519\",\"forceLogout\":\"0\",\"role_id\":\"1\"}}','Harold Ortega','editar',2,'Users','Usuarios');
/*!40000 ALTER TABLE `log_activities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `log_errors`
--

DROP TABLE IF EXISTS `log_errors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log_errors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `controller` text,
  `description` longtext,
  `user` varchar(255) DEFAULT NULL,
  `event_timestamp` int(11) DEFAULT NULL,
  `operation` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `log_errors`
--

LOCK TABLES `log_errors` WRITE;
/*!40000 ALTER TABLE `log_errors` DISABLE KEYS */;
/*!40000 ALTER TABLE `log_errors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `log_imports`
--

DROP TABLE IF EXISTS `log_imports`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log_imports` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` datetime DEFAULT NULL,
  `description` longtext,
  `user_id` int(11) DEFAULT NULL,
  `model` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `log_imports`
--

LOCK TABLES `log_imports` WRITE;
/*!40000 ALTER TABLE `log_imports` DISABLE KEYS */;
/*!40000 ALTER TABLE `log_imports` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `modules`
--

DROP TABLE IF EXISTS `modules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `modules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `name_machine` varchar(255) DEFAULT NULL,
  `categorie_id` int(11) DEFAULT NULL,
  `state` int(11) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `modules`
--

LOCK TABLES `modules` WRITE;
/*!40000 ALTER TABLE `modules` DISABLE KEYS */;
INSERT INTO `modules` (`id`, `name`, `name_machine`, `categorie_id`, `state`) VALUES (1,'Usuarios','Users',1,1),(2,'Modulos Sistema','Modules',1,1),(3,'Roles','Roles',1,1),(4,'Menu Administrativo','Categories',1,1),(8,'Configuracion Servidor Correos','ConfigEmails',1,1),(12,'Log Errores','LogErrors',3,1);
/*!40000 ALTER TABLE `modules` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_modules`
--

DROP TABLE IF EXISTS `role_modules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_modules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `module_id` int(11) DEFAULT '0',
  `role_id` int(11) DEFAULT '0',
  `add` int(11) DEFAULT '0',
  `edit` int(11) DEFAULT '0',
  `delete` int(11) DEFAULT '0',
  `active` int(11) DEFAULT '0',
  `change_state` int(11) DEFAULT '0',
  `view` int(11) DEFAULT '0',
  `export` int(11) DEFAULT '0',
  `import` int(11) DEFAULT '0',
  `view_revision` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_modules`
--

LOCK TABLES `role_modules` WRITE;
/*!40000 ALTER TABLE `role_modules` DISABLE KEYS */;
INSERT INTO `role_modules` (`id`, `module_id`, `role_id`, `add`, `edit`, `delete`, `active`, `change_state`, `view`, `export`, `import`, `view_revision`) VALUES (1,1,1,1,1,1,1,1,1,1,1,0),(2,2,1,1,1,1,1,1,0,0,0,0),(3,3,1,1,1,1,1,1,0,0,0,0),(4,4,1,1,1,1,1,1,0,0,0,0),(9,5,1,1,1,1,1,1,0,1,1,0),(11,6,1,1,1,1,1,1,0,1,1,0),(13,7,1,1,1,1,1,1,0,1,1,0),(15,8,1,0,0,0,1,0,0,0,0,0),(19,9,1,1,1,0,1,0,1,1,1,0),(21,10,1,1,1,1,1,0,1,0,0,0),(23,11,1,0,0,0,1,0,1,0,0,0),(25,12,1,1,1,1,1,1,0,1,1,0);
/*!40000 ALTER TABLE `role_modules` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `state` varchar(45) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` (`id`, `name`, `state`) VALUES (1,'Super Administrador','1');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `state` int(11) DEFAULT '1',
  `nit` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `cellphone` varchar(255) DEFAULT NULL,
  `forceLogout` int(11) DEFAULT '0',
  `role_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `username`, `password`, `name`, `email`, `state`, `nit`, `phone`, `cellphone`, `forceLogout`, `role_id`) VALUES (1,'admin','6552a71a26c78253edcb7489c529027472e0cfb8','Harold Ortega','hfortega99@gmail.com',1,'1130587391','3008543519','3008543519',0,1),(2,'anavarro','67e44dbf2af62688a33c4038c9e8e793cb350551','Alina Navarro Gil','prueba@prueba.com',1,'1038105254','','3008543519',0,1),(3,'aocampo','2e3a242bbdfd71eed25731538113b56075b6a3c1','Oscar Ocampo','info@prueba.com',2,'1140108657','','3005675691',0,1);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-09-05 12:19:17
