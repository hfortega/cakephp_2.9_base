-- creación de la primera vista de los pedidos (Drupal), esto se hace ya que mysql solo tiene una capacidad de procesamiento de 61 tablas

create view `delivery_p1` AS

select 
    cd.central_delivery_id, 
    cd.delivery_globalid  as delivery_globalid,
    cd.create_ts  as creation_timestamp,
    cd.modify_ts  as modification_timestamp,
    cd.is_ppm as is_ppm,
    fet.field_event_timestamp_value as event_timestamp,
    cds.central_delivery_state_value as central_delivery_state,
    fcn.field_customer_name_value as customer_name,
    fci.field_customer_id_value as customer_id,
    fpn.field_program_name_value as program_name,
    fiu.field_instance_username_value as instance_username,
    fiul.field_instance_user_lastnames_value as instance_user_lastnames,
    fiun.field_instance_user_names_value as instance_user_names,
    fiuid.field_instance_user_iddoc_value as instance_user_iddoc,
    fiue.field_instance_user_email_email as instance_user_email_email,
    fius.field_instance_user_segment_value as instance_user_segment,
    fiud.field_instance_user_division_value as instance_user_division,
    fitm.field_is_third_managed_value as is_third_managed,
    fs.field_sku_value as sku,
    fe.field_ean_14_value as ean_14,
    fw.field_weight_value as weight,
    fv.field_volume_value as volume,
    fpu.field_packaging_units_value as packaging_units,
    fprn.field_product_name_value as product_name,
    fpb.field_product_brand_value as product_brand,
    fipc.field_instance_prod_category_value as instance_prod_category,
    fq.field_quantity_value as quantity,
    fc.field_cost_value as cost,
    fp.field_price_value as price,
    fsc.field_shipping_cost_value as shipping_cost,
    fvat.field_vat_value as vat,
    ftup.field_total_unit_price_value as total_unit_price,
    ftp.field_total_price_value as total_price,
    fpp.field_points_price_value as points_price,
    fpv.field_point_value_value as point,
    fpa.field_prepay_amount_value as prepay_amount,
    fpoints.field_prepay_points_value as prepay_points,
    fb.field_balance_value as balance,
    fpbalance.field_points_balance_value as points_balance,
    fpprovider.field_prepay_provider_value as prepay_provider,
    fptx.field_prepay_txnid_value as prepay_txnid,
    fpvat.field_prepay_vat_value as prepay_vat,
    fpcb.field_prepay_commision_base_value as prepay_commision_base,
    fpco.field_prepay_commision_value as prepay_commision,
    fpcovat.field_prepay_commision_vat_value as prepay_commision_vat,
    fpiddoc.field_prepay_iddoc_value as prepay_iddoc,
    fpnames.field_prepay_names_value as prepay_names,
    fpln.field_prepay_lastnames_value as prepay_lastnames,
    fpemail.field_prepay_email_value as prepay_email,
    fpfr.field_prepay_franchise_value as prepay_franchise,
    fpcus.field_prepay_cus_value as prepay_cus,
    fpauth.field_prepay_authorization_value as prepay_authorization,
    fpstate.field_ppm_state_value as ppm_state,
    fni.field_number_invoice_value as number_invoice,
    fis.field_is_shippable_value as is_shippable,
    fsl.field_shipping_lastnames_value as shipping_lastnames,
    fsn.field_shipping_names_value as shipping_names,
    fsr.field_shipping_region_value as shipping_region,
    fsm.field_shipping_municipality_value as shipping_municipality,
    fsmc.field_shipping_municipality_code_value as shipping_municipality_code,
    fsa.field_shipping_address_value as shipping_address,
    fsp.field_shipping_phone_value as shipping_phone,
    fsnotes.field_shipping_notes_value as shipping_notes,
    fsnews.field_shipping_news_value as shipping_news

    
    
    
    
from central_delivery as cd

left join field_data_field_event_timestamp as fet
	on fet.entity_id = cd.central_delivery_id and fet.entity_type = 'central_delivery'
    
left join field_data_central_delivery_state as cds
	on cds.entity_id = cd.central_delivery_id and cds.entity_type = 'central_delivery'

left join field_data_field_customer_name as fcn
	on fcn.entity_id = cd.central_delivery_id and fcn.entity_type = 'central_delivery'
    
left join field_data_field_customer_id as fci
	on fci.entity_id = cd.central_delivery_id and fci.entity_type = 'central_delivery'
    
left join field_data_field_program_name as fpn
	on fpn.entity_id = cd.central_delivery_id and fpn.entity_type = 'central_delivery'
    
left join field_data_field_instance_username as fiu
	on fiu.entity_id = cd.central_delivery_id and fiu.entity_type = 'central_delivery'

left join field_data_field_instance_user_lastnames as fiul
	on fiul.entity_id = cd.central_delivery_id and fiul.entity_type = 'central_delivery'

left join field_data_field_instance_user_names as fiun
	on fiun.entity_id = cd.central_delivery_id and fiun.entity_type = 'central_delivery'
    
left join field_data_field_instance_user_iddoc as fiuid
	on fiuid.entity_id = cd.central_delivery_id and fiuid.entity_type = 'central_delivery'

left join field_data_field_instance_user_email as fiue
	on fiue.entity_id = cd.central_delivery_id and fiue.entity_type = 'central_delivery'
    
left join field_data_field_instance_user_segment as fius
	on fius.entity_id = cd.central_delivery_id and fius.entity_type = 'central_delivery'
    
left join field_data_field_instance_user_division as fiud
	on fiud.entity_id = cd.central_delivery_id and fiud.entity_type = 'central_delivery'
    
left join field_data_field_is_third_managed as fitm
	on fitm.entity_id = cd.central_delivery_id and fitm.entity_type = 'central_delivery'
    
left join field_data_field_sku as fs
	on fs.entity_id = cd.central_delivery_id and fs.entity_type = 'central_delivery'
    
left join field_data_field_ean_14 as fe
	on fe.entity_id = cd.central_delivery_id and fe.entity_type = 'central_delivery'
    
left join field_data_field_weight as fw
	on fw.entity_id = cd.central_delivery_id and fw.entity_type = 'central_delivery'
    
left join field_data_field_volume as fv
	on fv.entity_id = cd.central_delivery_id and fv.entity_type = 'central_delivery'

left join field_data_field_packaging_units as fpu
	on fpu.entity_id = cd.central_delivery_id and fpu.entity_type = 'central_delivery'
    
left join field_data_field_product_name as fprn
	on fprn.entity_id = cd.central_delivery_id and fprn.entity_type = 'central_delivery'
    
left join field_data_field_product_brand as fpb
	on fpb.entity_id = cd.central_delivery_id and fpb.entity_type = 'central_delivery'
    
left join field_data_field_instance_prod_category as fipc
	on fipc.entity_id = cd.central_delivery_id and fipc.entity_type = 'central_delivery'
    
left join field_data_field_quantity as fq
	on fq.entity_id = cd.central_delivery_id and fq.entity_type = 'central_delivery'
    
left join field_data_field_cost as fc
	on fc.entity_id = cd.central_delivery_id and fc.entity_type = 'central_delivery'
    
left join field_data_field_price as fp
	on fp.entity_id = cd.central_delivery_id and fp.entity_type = 'central_delivery'
    
left join field_data_field_shipping_cost as fsc
	on fsc.entity_id = cd.central_delivery_id and fsc.entity_type = 'central_delivery'
    
left join field_data_field_vat as fvat
	on fvat.entity_id = cd.central_delivery_id and fvat.entity_type = 'central_delivery'
    
left join field_data_field_total_unit_price as ftup
	on ftup.entity_id = cd.central_delivery_id and ftup.entity_type = 'central_delivery'
    
left join field_data_field_total_price as ftp
	on ftp.entity_id = cd.central_delivery_id and ftp.entity_type = 'central_delivery'
    
left join field_data_field_points_price as fpp
	on fpp.entity_id = cd.central_delivery_id and fpp.entity_type = 'central_delivery'
    
left join field_data_field_point_value as fpv
	on fpv.entity_id = cd.central_delivery_id and fpv.entity_type = 'central_delivery'
    
left join field_data_field_prepay_amount as fpa
	on fpa.entity_id = cd.central_delivery_id and fpa.entity_type = 'central_delivery'
    
left join field_data_field_prepay_points as fpoints
	on fpoints.entity_id = cd.central_delivery_id and fpoints.entity_type = 'central_delivery'
    
left join field_data_field_balance as fb
	on fb.entity_id = cd.central_delivery_id and fb.entity_type = 'central_delivery'
    
left join field_data_field_points_balance as fpbalance
	on fpbalance.entity_id = cd.central_delivery_id and fpbalance.entity_type = 'central_delivery'
    
left join field_data_field_prepay_provider as fpprovider
	on fpprovider.entity_id = cd.central_delivery_id and fpprovider.entity_type = 'central_delivery'
    
left join field_data_field_prepay_txnid as fptx
	on fptx.entity_id = cd.central_delivery_id and fptx.entity_type = 'central_delivery'
    
left join field_data_field_prepay_vat as fpvat
	on fpvat.entity_id = cd.central_delivery_id and fpvat.entity_type = 'central_delivery'
    
left join field_data_field_prepay_commision_base as fpcb
	on fpcb.entity_id = cd.central_delivery_id and fpcb.entity_type = 'central_delivery'
    
left join field_data_field_prepay_commision as fpco
	on fpco.entity_id = cd.central_delivery_id and fpco.entity_type = 'central_delivery'
    
left join field_data_field_prepay_commision_vat as fpcovat
	on fpcovat.entity_id = cd.central_delivery_id and fpcovat.entity_type = 'central_delivery'
    
left join field_data_field_prepay_iddoc as fpid
	on fpid.entity_id = cd.central_delivery_id and fpid.entity_type = 'central_delivery'
    
left join field_data_field_prepay_iddoc as fpiddoc
	on fpiddoc.entity_id = cd.central_delivery_id and fpiddoc.entity_type = 'central_delivery'
    
left join field_data_field_prepay_names as fpnames
	on fpnames.entity_id = cd.central_delivery_id and fpnames.entity_type = 'central_delivery'
    
left join field_data_field_prepay_lastnames as fpln
	on fpln.entity_id = cd.central_delivery_id and fpln.entity_type = 'central_delivery'
    
left join field_data_field_prepay_email as fpemail
	on fpemail.entity_id = cd.central_delivery_id and fpemail.entity_type = 'central_delivery'
    
left join field_data_field_prepay_franchise as fpfr
	on fpfr.entity_id = cd.central_delivery_id and fpfr.entity_type = 'central_delivery'
    
left join field_data_field_prepay_cus as fpcus
	on fpcus.entity_id = cd.central_delivery_id and fpcus.entity_type = 'central_delivery'
    
left join field_data_field_prepay_authorization as fpauth
	on fpauth.entity_id = cd.central_delivery_id and fpauth.entity_type = 'central_delivery'
    
left join field_data_field_ppm_state as fpstate
	on fpstate.entity_id = cd.central_delivery_id and fpstate.entity_type = 'central_delivery'
    
left join field_data_field_number_invoice as fni
	on fni.entity_id = cd.central_delivery_id and fni.entity_type = 'central_delivery'
    
left join field_data_field_is_shippable as fis
	on fis.entity_id = cd.central_delivery_id and fis.entity_type = 'central_delivery'
    
left join field_data_field_shipping_lastnames as fsl
	on fsl.entity_id = cd.central_delivery_id and fsl.entity_type = 'central_delivery'
    
left join field_data_field_shipping_names as fsn
	on fsn.entity_id = cd.central_delivery_id and fsn.entity_type = 'central_delivery'
    
left join field_data_field_shipping_region as fsr
	on fsr.entity_id = cd.central_delivery_id and fsr.entity_type = 'central_delivery'
    
left join field_data_field_shipping_municipality as fsm
	on fsm.entity_id = cd.central_delivery_id and fsm.entity_type = 'central_delivery'
    
left join field_data_field_shipping_municipality_code as fsmc
	on fsmc.entity_id = cd.central_delivery_id and fsmc.entity_type = 'central_delivery'
    
left join field_data_field_shipping_address as fsa
	on fsa.entity_id = cd.central_delivery_id and fsa.entity_type = 'central_delivery'
    
left join field_data_field_shipping_phone as fsp
	on fsp.entity_id = cd.central_delivery_id and fsp.entity_type = 'central_delivery'
    
left join field_data_field_shipping_notes as fsnotes
	on fsnotes.entity_id = cd.central_delivery_id and fsnotes.entity_type = 'central_delivery'
    
left join field_data_field_shipping_news as fsnews
	on fsnews.entity_id = cd.central_delivery_id and fsnews.entity_type = 'central_delivery'

-- creación de la segunda vista de los pedidos (Drupal), esto se hace ya que mysql solo tiene una capacidad de procesamiento de 61 tablas

create view `delivery_p2` AS
select 
	cd.central_delivery_id as central_delivery_id,
    ftc.field_taxoref_courier_tid as taxoref_courier_tid,
    fcti.field_courier_tracking_id_value as courier_tracking_id,
    fii.field_instance_id_value as instance_id,
    foi.field_order_id_value as order_id,
    fpi.field_program_id_value as program_id,
    fiu.field_instance_uid_value as instance_uid,
    fes.field_erp_state_value as erp_state,
    fe.field_error_value as error,
    fem.field_error_msg_value as error_msg,
    feii.field_erp_impproc_id_value as erp_impproc_id


    
from central_delivery AS cd

left join field_data_field_taxoref_courier as ftc
	on ftc.entity_id = cd.central_delivery_id and ftc.entity_type = 'central_delivery'
    
left join field_data_field_courier_tracking_id as fcti
	on fcti.entity_id = cd.central_delivery_id and fcti.entity_type = 'central_delivery'
    
left join field_data_field_instance_id as fii
	on fii.entity_id = cd.central_delivery_id and fii.entity_type = 'central_delivery'
    
left join field_data_field_order_id as foi
	on foi.entity_id = cd.central_delivery_id and foi.entity_type = 'central_delivery'
    
left join field_data_field_program_id as fpi
	on fpi.entity_id = cd.central_delivery_id and fpi.entity_type = 'central_delivery'
    
left join field_data_field_instance_uid as fiu
	on fiu.entity_id = cd.central_delivery_id and fiu.entity_type = 'central_delivery'
    
left join field_data_field_erp_state as fes
	on fes.entity_id = cd.central_delivery_id and fes.entity_type = 'central_delivery'
    
left join field_data_field_error as fe
	on fe.entity_id = cd.central_delivery_id and fe.entity_type = 'central_delivery'
    
left join field_data_field_error_msg as fem
	on fem.entity_id = cd.central_delivery_id and fem.entity_type = 'central_delivery'
    
left join field_data_field_erp_impproc_id as feii
	on feii.entity_id = cd.central_delivery_id and feii.entity_type = 'central_delivery'
    


-- creación de la vista de los componentes de pago (drupal)

create view `payment_component` AS

select 
	cd.delivery_globalid AS delivery_globalid,
    cpp.central_payment_component_id, 
	fci.field_customer_id_value as customer_id,
    fcn.field_customer_name_value as customer_name,
	fii.field_instance_id_value as instance_id,
	fet.field_event_timestamp_value as event_timestamp,
	fuo.field_userpoints_operation_value as userpoints_operation,
	fld.field_long_description_value as long_description,
    fpp.field_points_price_value as points_price,
	fp.field_price_value as price,
	fpcs.field_payment_component_state_value as state,
	fvv.field_vat_value_value as vat_value

from central_delivery  as cd

inner join field_data_field_points_payment_components ppc ON ppc.entity_id = cd.central_delivery_id AND ppc.entity_type = 'central_delivery'

inner join central_payment_component cpp ON cpp.central_payment_component_id = ppc.field_points_payment_components_target_id

left join field_data_field_customer_id as fci
	on fci.entity_id = cpp.central_payment_component_id and fci.entity_type = 'central_payment_component'
    
left join field_data_field_customer_name as fcn
	on fcn.entity_id = cpp.central_payment_component_id and fcn.entity_type = 'central_payment_component'
    
left join field_data_field_instance_id as fii
	on fii.entity_id = cpp.central_payment_component_id and fii.entity_type = 'central_payment_component'
    
left join field_data_field_event_timestamp as fet
	on fet.entity_id = cpp.central_payment_component_id and fet.entity_type = 'central_payment_component'
    
left join field_data_field_userpoints_operation as fuo
	on fuo.entity_id = cpp.central_payment_component_id and fuo.entity_type = 'central_payment_component'
    
left join field_data_field_long_description as fld
	on fld.entity_id = cpp.central_payment_component_id and fld.entity_type = 'central_payment_component'
    
left join field_data_field_points_price as fpp
	on fpp.entity_id = cpp.central_payment_component_id and fpp.entity_type = 'central_payment_component'
    
left join field_data_field_price as fp
	on fp.entity_id = cpp.central_payment_component_id and fp.entity_type = 'central_payment_component'
    
left join field_data_field_vat_value as fvv
	on fvv.entity_id = cpp.central_payment_component_id and fvv.entity_type = 'central_payment_component'

left join field_data_field_payment_component_state as fpcs
	on fpcs.entity_id = cpp.central_payment_component_id and fpcs.entity_type = 'central_payment_component'




-- Pasar los pedidos del modelo de central (drupal) a el model de central (cake)

insert into deliveries (delivery_globalid,creation_timestamp,modification_timestamp,is_ppm,event_timestamp,central_delivery_state,customer_name,customer_id,program_name,instance_username,
instance_user_lastnames,instance_user_names,instance_user_iddoc,instance_user_email,instance_user_segment,instance_user_division,is_third_managed,sku,
ean_14,weight,volume,packaging_units,product_name,product_brand,instance_prod_category,quantity,cost,price,shipping_cost,vat,total_unit_price,total_price,
points_price,point_value,prepay_amount,prepay_points,balance,points_balance,prepay_provider,prepay_txnid,prepay_vat,prepay_commision_base,prepay_commision,
prepay_commision_vat,prepay_iddoc,prepay_names,prepay_lastnames,prepay_email,prepay_franchise,prepay_cus,prepay_authorization,ppm_state,number_invoice,
is_shippable,shipping_lastnames,shipping_names,shipping_region,shipping_municipality,shipping_municipality_code,shipping_address,shipping_phone,shipping_notes,
shipping_news,courier_id,courier_tracking_id,instance_id,order_id,program_code,instance_uid,erp_state,have_error,msg_error,erp_impproc_id) 
(
select delivery_globalid,creation_timestamp,modification_timestamp,is_ppm,UNIX_TIMESTAMP(event_timestamp),central_delivery_state,customer_name,customer_id,program_name,instance_username,instance_user_lastnames,instance_user_names,instance_user_iddoc,instance_user_email_email,instance_user_segment,instance_user_division,is_third_managed,sku,ean_14,weight,volume,packaging_units,product_name,product_brand,instance_prod_category,quantity,cost,price,shipping_cost,vat,total_unit_price,total_price,points_price,point,prepay_amount,prepay_points,balance,points_balance,prepay_provider,prepay_txnid,prepay_vat,prepay_commision_base,prepay_commision,prepay_commision_vat,prepay_iddoc,prepay_names,prepay_lastnames,prepay_email,prepay_franchise,prepay_cus,prepay_authorization,ppm_state,number_invoice,is_shippable,shipping_lastnames,shipping_names,shipping_region,shipping_municipality,shipping_municipality_code,shipping_address,shipping_phone,shipping_notes,shipping_news,taxoref_courier_tid,courier_tracking_id,instance_id,order_id,program_id,instance_uid,erp_state,error,error_msg,erp_impproc_id 
from central_nueva.delivery where event_timestamp >= '2016-08-01 00:00:00' limit 8000
)

-- Pasar los componentes de pago del modelo de central (drupal) a el model de central (cake)
insert into payment_components (delivery_globalid,customer_id, customer_name, instance_id, event_timestamp, userpoints_operation, long_description, points_price, price, state, 
    vat_value)
(
	select delivery_globalid,customer_id, customer_name, instance_id, UNIX_TIMESTAMP(event_timestamp), userpoints_operation, long_description, points_price, price, state, 
    vat_value from central_nueva.payment_component where event_timestamp >= '2016-08-01 00:00:00' limit 8000
)